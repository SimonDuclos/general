import maya.cmds as cmds

def orig(selection):
    for sel in selection:
        if "_" in sel:
            name_split = sel.split("_")
            name_split[-1] = "GRP"
            group_name = "_".join(name_split)
        else:
            group_name = "{}_GRP".format(sel)
        trans = cmds.xform(sel, q=True, ws=True, t=True)
        rot = cmds.xform(sel, q=True, ws=True, ro=True)
        orig = cmds.group(em=True, n=group_name)
        cmds.xform(orig, t=trans, ws=True)
        cmds.xform(orig, ro=rot, ws=True)
        par = cmds.listRelatives(sel, p=True, f=True)
        if par:
            cmds.parent(orig, par)
        cmds.parent(sel, orig)

def ctlsphere(name, ctlsize):
    curve_01 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_02 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_03 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    cmds.rotate(90, 0, 0, curve_02[0], relative=True, objectSpace=True)
    cmds.rotate(0, 0, 90, curve_03[0], relative=True, objectSpace=True)
    cmds.makeIdentity(curve_02[0], curve_03[0], apply=True, rotate=True)
    curveShape_01 = cmds.listRelatives(curve_01[0], shapes=True) or []
    curveShape_02 = cmds.listRelatives(curve_02[0], shapes=True) or []
    curveShape_03 = cmds.listRelatives(curve_03[0], shapes=True) or []
    cmds.parent(curveShape_02[0], curve_01[0], relative=True, shape=True)
    cmds.parent(curveShape_03[0], curve_01[0], relative=True, shape=True)
    cmds.delete(curve_02[0], curve_03[0])
    ctl = cmds.rename(curve_01[0], name)
    ctlshape= cmds.listRelatives(ctl, shapes=True) or []
    return ctl, ctlshape

def createJntCtls(*args):
    jnts=cmds.ls(sl=True)
    i=1
    ctlSize=cmds.floatField('CtlSize', q=True, v=True)
    name = cmds.textField('RigName', q=True, tx=True)
    namedjnts = []
    for jnt in jnts:
        if cmds.checkBox('orient', q=True, v=True):
            cmds.joint(jnt, e=True, oj='xyz', sao='yup', zso=True)
        par = cmds.listRelatives(jnt, p=True, f=True)
        if cmds.checkBox('UseCurrentNaming', q=True, v=True):
            name=name
        else:
            jnt = cmds.rename(jnt, name+'_'+str(i).zfill(3)+'_JNT')
        name_split=jnt.split('_')
        name_split[-1] = "CTL"
        ctl_name = "_".join(name_split)
        ctl = ctlsphere(ctl_name, ctlSize)
        ctlshape = ctl[1]
        ctl=ctl[0]
        cmds.delete(cmds.pointConstraint(jnt, ctl, maintainOffset = False))
        cmds.delete(cmds.orientConstraint(jnt, ctl, maintainOffset = False))
        cmds.parentConstraint(ctl, jnt)
        cmds.scaleConstraint(ctl, jnt)
    # find previous ctl and parent
        group_name = str(ctl.replace('CTL', 'GRP'))
        trans = cmds.xform(ctl, q=True, ws=True, t=True)
        rot = cmds.xform(ctl, q=True, ws=True, ro=True)
        orig = cmds.group(em=True, n=group_name)
        cmds.xform(orig, t=trans, ws=True)
        cmds.xform(orig, ro=rot, ws=True)
        par = cmds.listRelatives(ctl, p=True, f=True)
        if par:
            cmds.parent(orig, par)
        cmds.parent(ctl, orig)

        namedjnts.append(jnt)
        i+=1
        
    for jnt in namedjnts:
        if cmds.listRelatives(jnt, p=True):
            par = cmds.listRelatives(jnt, p=True, pa=True) or []
            par = par[0]

            if par in namedjnts:
                
                name_split=jnt.split('_')
                name_split[-1] = "GRP"
                grp = "_".join(name_split)
                
                
                par_split=par.split('_')
                par_split[-1] = "CTL"
                ctl = "_".join(par_split)
                cmds.parent(grp, ctl)

def closeWindow(*args):
    
    # Closes UI
    cmds.deleteUI('ForestCTLonJnt')

def createUI(*args):
    
    windowprop = 'ForestCTLonJnt'
    
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window(windowprop, t='Forest - Controls from Joints',w=300, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('Layout')
    cmds.rowColumnLayout( numberOfColumns=1, columnWidth=[ (1,300) ], parent='Layout')
    cmds.text('Controls from Joints', al='center', font="boldLabelFont", h=40, w=100)
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 150), (2, 150)], parent='Layout')
    cmds.text('  Naming:', al='left', font="obliqueLabelFont")
    cmds.textField('RigName', text='RigName', width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=10, style='none')
    cmds.checkBox('UseCurrentNaming', l='Use Current Naming') 
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')
    cmds.text('  Options:', al='left', font="obliqueLabelFont")
    cmds.text('Controls Size', al='left')
    cmds.separator(h=10, style='none')
    cmds.floatField('CtlSize', minValue=0.001, value=1)
    cmds.separator(h=10, style='none')
    cmds.separator(h=5, style='none')
    cmds.separator(h=5, style='none')
    cmds.checkBox('orient', l='Orient Joints')
    cmds.separator(h=5, style='none')
    cmds.separator(h=5, style='none')

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 300)], parent='Layout')
    cmds.button(label = 'Create Rig', h=30, backgroundColor= [0.1,.4,.25], command = createJntCtls)
    cmds.separator(h=10, style='none')
    cmds.button(label = 'Close Window', h=30, backgroundColor= [0.4,.1,.25], command = closeWindow)
    cmds.showWindow(windowprop)
    cmds.window(window, edit=True, w=305, h=235) 
