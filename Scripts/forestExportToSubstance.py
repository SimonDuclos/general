import maya.cmds as cmds 
import os
import os.path
from functools import partial
import pymel.core as pm

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def closeWindow(*args):
    # Closes UI
    cmds.deleteUI('ForestExpSBS')

def exportLowHigh(*args):
    sel = cmds.ls(sl=True)
    cmds.file(s=True)
    filePath = cmds.file(q=True, sn=True) 
    filename=cmds.textField('fileName', q=True, text=True)
    subdiv=cmds.intField('subdiv', q=True, v=True)
    exportPath =''
    if '01_MOD' in filePath:
        exportPath = filePath.split('01_MOD')[0]+'02_SHD/_Geos/'
    make_dir(exportPath)
    low= []
    high=[]
    for s in sel:
        if '_GRP' in s:
            geos = cmds.listRelatives(s, ad=True)
            for geo in geos:
                if 'Shape' in geo:
                    geos.remove(geo)
            #create a shader (material)
            shader=cmds.shadingNode('lambert', asShader=True, n=s[:-4]) #placeholder name
            #create a shading group
            shading_group=cmds.sets(renderable=True, noSurfaceShader=True, empty=True, n=s[:-4]+'_SHDG')
            #connect the shader to the shading group
            cmds.connectAttr('%s.outColor' %shader, '%s.surfaceShader'%shading_group)
            for geo in geos:
                if '_MOD' in geo:
                    cmds.sets(geo, e=True, forceElement=shading_group)
                    newname=cmds.rename(geo, geo[:-3]+'low')
                    dup = cmds.duplicate(newname, n=geo[:-3]+'high')

                    shape=cmds.listRelatives(dup, s=True)
                    cmds.polySmooth(shape, ksb=True, dv=subdiv)
                    low.append(newname)
                    high.append(dup[0])
    if exportPath:
        exportPath = cmds.fileDialog2(dialogStyle=2, fileMode =3, dir=exportPath) [0]
    else:
        exportPath = cmds.fileDialog2(dialogStyle=2, fileMode =3) [0]
    cmds.select(low)
    pm.mel.FBXExport(f=exportPath+'/'+filename+'_low', s=True)
    cmds.select(high)
    pm.mel.FBXExport(f=exportPath+'/'+filename+'_high', s=True)
    cmds.file(filePath, o=True, force=True)
    closeWindow()

def createUI(*args):
    #pm.loadPlugin("fbxmaya")
    windowprop = 'ForestExpSBS'
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window(windowprop, t='Forest Export To Substance',w=400, h=250, sizeable=True, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('Layout')
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 150), (2, 150)], parent='Layout')
    cmds.text('  File Name:', al='left', font="obliqueLabelFont")
    cmds.textField('fileName', text=filename[:-7], width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')
    cmds.text('  Subdivision Amount:', al='left', font="obliqueLabelFont")
    cmds.intField('subdiv', v=1, width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')
    cmds.setParent('..')

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 300)], parent='Layout')
    cmds.button(label = 'Export', h=30, backgroundColor= [0.1,.4,.25], command = exportLowHigh)
    cmds.separator(h=15, style='none')
    cmds.showWindow(windowprop)
    cmds.window(window, edit=True, w=405, h=220) 
