#Fixes: to make the ribbon more accurate, you need to snap rotate the ctls before parenting the ctlhs, so the ribbon won't be affected by their original orientation.  

from maya import cmds
import AutoBendyArms
import snapr
import snapt
import orig

def facialCrv(*args):

    edge = cmds.ls( sl= True)

    mesh = edge[0].split('.')[0]

    crvOrig, node = cmds.polyToCurve (form =2 , degree =1 )
    cmds.delete(node)
    if cmds.checkBox('reverse', q=True, v=True):
        cmds.reverseCurve(crvOrig, rpo=True)
    crv_shape = cmds.listRelatives(crvOrig, s=True)

    name=cmds.textField('name', q=True, tx=True)
        
    mCtlSize = cmds.floatField('mCtlSize', q=True, v=True)
    sCtlSize = mCtlSize/2
       
    rig = cmds.group(em=True, n='R_'+name+'_RIG_GRP')

    puppet= cmds.group(em=True, n='R_'+name+'_PUPPET_GRP', p=rig)

    nbr_cv=cmds.intField('nbr_cv', q=True, v=True)

    size =cmds.intField('size', q=True, v=True)

    lr = 1/size

    coef = size/(nbr_cv-1)

    cv_mid = (nbr_cv-1)/2

    cv_max = nbr_cv-1

    nbr_cv_min = [0, cv_mid ,cv_max ]

    ctls=[]

    for i in xrange(nbr_cv):
        ctls.append(i)

    secctl = [item for item in ctls if item not in nbr_cv_min]

    crvMain = cmds.rebuildCurve(crvOrig, rt=0, s=4, d=1)

    for n in xrange(5):
        h = cmds.spaceLocator (n= 'R_'+name+'_'+str(n)+'_HLP')
        place = cmds.pathAnimation (h,c= crvMain[0],f=True,fm =True)
        cmds.setAttr (str(place)+'.uValue' , n*(1.000/(nbr_cv-1)))
        cmds.delete(h, ch=True)

    crvSurf =cmds.duplicate(crvMain) 
    cmds.rebuildCurve(crvSurf[0], rt=0, s=1, d=1)

    skipLocA = cmds.spaceLocator (n= 'R_'+name+'_skipLocStart_HLP')
    placeA = cmds.pathAnimation (skipLocA,c= crvMain[0],f=True,fm =True)
    cmds.setAttr(str(placeA)+'.uValue' , 1.000/(nbr_cv+2))


    skipLocB = cmds.spaceLocator (n= 'R_'+name+'_skipLocEnd_HLP')
    placeB = cmds.pathAnimation (skipLocB,c= crvMain[0],f=True,fm =True)
    cmds.setAttr (str(placeB)+'.uValue' , 1.000-(1.000/(nbr_cv+2)))



    knotParameter = cmds.arclen(crvSurf[0])
    cmds.insertKnotCurve(crvSurf[0], parameter = knotParameter * .5, replaceOriginal = True)

    midloc='R_'+name+'_'+str(nbr_cv_min[1])+'_HLP'
    x=cmds.getAttr(midloc+'.translateX')
    y=cmds.getAttr(midloc+'.translateY')
    z=cmds.getAttr(midloc+'.translateZ')
    cmds.move(x,y,z,  crvSurf[0]+'.cv[1]', wd=True)


    crvSec = cmds.duplicate(crvSurf[0])
    y = cmds.getAttr(crvSec[0]+'.translateY')
    cmds.setAttr(crvSec[0]+'.translateY', y+0.1)

        
    surforig = cmds.loft(crvSurf[0], crvSec[0], n='R_'+name+'_SurfOrig_NSURF', d=1, u=True)
    cmds.delete(surforig, ch=True)

    cmds.delete(crvSec[0])




    surfsec = cmds.nurbsPlane(n=('R_'+name+'_SurfSeR_NSURF'), degree=3, w=10, lr=lr,  ax=[0,1,0], u=nbr_cv-1)


#Create Main Clusters (1)
    ctlgrp = cmds.group(em=True, n='R_'+name+'_CTL_GRP', p=rig)
    
    x= 0
    for i in nbr_cv_min:
        
        cls = cmds.cluster('R_'+name+'_SurfOrig_NSURF' + '.cv[*][' + str(x) + ']', n='R_'+name+'_MainCluster_{}_CLS'.format(str(i).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, 'R_'+name+'_MainClusterHandle_{}_CLH'.format(str(i).zfill(3)))
        x = x+1
        
        
    #Create Main CTLS (1)
        
        a = AutoBendyArms.ctlsphere('R_'+name+'_RibbonMain_{}_CTL'.format(str(i).zfill(3)), mCtlSize)
        ctl=a[0]
        ctlshape= a[1]      
        
        cmds.delete(cmds.pointConstraint(clh, ctl, maintainOffset = False))
        cmds.parent(clh,ctl)
        
        AutoBendyArms.orig(ctl)
        
        cmds.hide(clh)

        
    #Create Main Fol (1) 
    
    nbr_fol = nbr_cv-3
    fol_mid = (nbr_fol/2)+1
    U=0.5
    V=0
    coef = (1/(float(nbr_cv)-1))
    
    for i in xrange (nbr_fol):
        
        if i+1 == fol_mid:            
            V = V+(coef*2) 
        else :            
            V = V+coef 
                       
        AutoBendyArms.createFolicule('R_'+name+'_RibbonMain_{}_FCTL'.format(str(secctl[i]).zfill(3)), surforig[0], U, V)
           
    
    #SurfRIB
    
    #Create Clusters (2)
    
    skip = [1, nbr_cv]
    do =[]
    for i in xrange(nbr_cv+2):
        do.append(i)
    
    do = [item for item in do if item not in skip]
    for i in xrange(nbr_cv):
        cls = cmds.cluster(surfsec[0] + '.cv[' + str(do[i]) + '][*]', n='R_'+name+'_RibCluster_{}_CLS'.format(str(do[i]).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, 'R_'+name+'_RibClusterHandle_{}_CLH'.format(str(do[i]).zfill(3)))
                     
        
    #Create CTLS (2)
        
        b = AutoBendyArms.ctlsphere('R_'+name+'_RibbonSeR_{}_CTL'.format(str(i).zfill(3)), sCtlSize)
        
        ctl = b[0]
        ctlshape= b[1]
        
        cmds.delete(cmds.pointConstraint(clh, ctl, maintainOffset = False))
        cmds.parent(clh,ctl)
        
        AutoBendyArms.orig(ctl)
        cmds.hide(clh)

    for i in skip:
        cls = cmds.cluster(surfsec[0] + '.cv[' + str(i) + '][*]', n='R_'+name+'_RibCluster_{}_CLS'.format(str(i).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, 'R_'+name+'_RibClusterHandle_{}_CLH'.format(str(i).zfill(3)))
        orig.orig([clh])
        gclh='R_'+name+'_RibClusterHandle_{}_GRP'.format(str(i).zfill(3))
        if i == 1:
            par = 'R_'+name+'_RibbonSeR_000_CTL'
            cmds.setAttr(clh+'.translateX', (size/2)-(size/2)*(1.000/(nbr_cv+2)))
        else:
            par = 'R_'+name+'_RibbonSeR_{}_CTL'.format(str(cv_max).zfill(3))
            cmds.setAttr(clh+'.translateX', -((size/2)-(size/2)*(1.000/(nbr_cv+2))))
        cmds.parent(gclh,par)
        cmds.hide(clh)
        
    #Create Fol (2)
    
    folnum = (nbr_cv*2)+1
    V =0.5
    
     
    for i in xrange(folnum): 
        
        U = float(i)/(folnum-1)  
        
        a = AutoBendyArms.createFolicule('R_'+name+'_RibbonSeR_{}_FOL'.format(str(i).zfill(3)), surfsec[0], U, V)
        folicleTrans = a
        bone = cmds.joint(p=(0,0,0), n='R_'+name+'_ArmRibbon_{}_JNT'.format(str(i).zfill(3)), r=0.25)
        translate = cmds.xform(folicleTrans, t=True, q=True, ws=True)
        cmds.xform(bone, t=translate, ws=True)
        cmds.parent (bone, folicleTrans)
            

    #Parent CRVRIB to CRV
    
    for i in secctl:
        child = 'R_'+name+'_RibbonSeR_{}_GRP'.format(str(i).zfill(3))
        par = 'R_'+name+'_RibbonMain_{}_FCTL'.format(str(i).zfill(3))
        cmds.parent(child,par)
        
    for i in nbr_cv_min:
        child = 'R_'+name+'_RibbonSeR_{}_GRP'.format(str(i).zfill(3))       
        par = 'R_'+name+'_RibbonMain_{}_CTL'.format(str(i).zfill(3))
        cmds.parent(child,par)     
            
            
    
    surfaces  = cmds.ls('R_'+name+'*'+'NSURF')
    group_name = 'R_'+name+'_SurfaceR_GRP'
    surfgrp = cmds.group(surfaces, n=group_name, p=puppet)
    
    follicles = cmds.ls('R_'+name+'*'+'FCTL')
    group_name = 'R_'+name+'_SecCTL_GRP'
    cmds.group(follicles , n=group_name, p=ctlgrp)
    
    
    cmds.select('R_'+name+'*'+'FOL')
    follicles = cmds.ls(sl=True)
    group_name = 'R_'+name+'_FollicleR_GRP'
    cmds.group(follicles , n=group_name, p=puppet)

    for i in nbr_cv_min:
        snap=['R_'+name+'_RibbonMain_{}_GRP'.format(str(i).zfill(3)),'R_'+name+'_'+str(i)+'_HLP']
        snapt.snapt(snap)
        snapr.snapr(snap)
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateX'.format(str(i).zfill(3)), 0)
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateY'.format(str(i).zfill(3)), 0)
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateZ'.format(str(i).zfill(3)), 0)
        cmds.parent(snap[0], rig)
        
    for i in secctl:
        snapsec=['R_'+name+'_RibbonSeR_{}_GRP'.format(str(i).zfill(3)),'R_'+name+'_'+str(i)+'_HLP']
        snapr.snapr(snapsec) 
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateX'.format(str(i).zfill(3)), 0)
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateY'.format(str(i).zfill(3)), 0)
        cmds.setAttr('R_'+name+'_RibbonSeR_{}_GRP.translateZ'.format(str(i).zfill(3)), 0)

    snapStart= ['R_'+name+'_RibClusterHandle_001_GRP', skipLocA]
    snapt.snapt(snapStart)
    snapEnd= ['R_'+name+'_RibClusterHandle_{}_GRP'.format(str(nbr_cv).zfill(3)), skipLocB]
    snapt.snapt(snapEnd)


    cmds.delete(crvMain[0])
    cmds.delete(crvSurf)
    cmds.delete('*_HLP')


def facialCrvWin(*args):
    #Window Parameters

    if cmds.window('facialTool', exists = True):
        cmds.deleteUI('facialTool')

    define = cmds.window('facialTool', width=150, maximizeButton=True, minimizeButton=True, titleBar=True, backgroundColor= [0.1,.2,.2])

    cmds.columnLayout(adj=True)

    cmds.text('    ')

    cmds.button (label='  Create Ribbon from Crv  ', recomputeSize=True, command=facialCrv, backgroundColor=[0.1,.4,.4], height=50)

    cmds.text('    ')

    cmds.separator()

    cmds.rowColumnLayout('param', numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 150), (2, 150)])

    cmds.text( label='Rig Name  ')
    cmds.textField('name', text='Ribbon')

    cmds.text( label='Secondary Ctls Number  ')
    cmds.intField('nbr_cv', minValue=5, maxValue= 20, value=5)

    cmds.text( label='Ribbon Size  ')
    cmds.intField('size', minValue=1, value=10)

    cmds.text( label='Main CTLs Size  ')
    cmds.floatField('mCtlSize', minValue=0.001, maxValue= 20, value=1)

    cmds.checkBox('reverse', l='Reverse Curve')
    cmds.text('    ')
    cmds.text('    ')


    cmds.showWindow(define)  
    
    






