import maya.cmds as cmds
import maya.mel as mel

def controlMaker(*args):

    path = cmds.internalVar(upd=True)+ "ForestTools.txt"
    f=open(path, 'r')
    toolDir= f.readline()
    path = toolDir+"/General/Scripts"
    source='source "'+path+'/controlMaker.mel"'
    mel.eval(source)
    mel.eval('controlMaker()')