import maya.cmds as cmds

def showJntOrient(*args):
	jnts = cmds.ls(type='joint')
	cmds.select(jnts)

	for jnt in jnts :
	    cmds.setAttr(jnt+'.jox', k=True)
	    cmds.setAttr(jnt+'.joy', k=True)
	    cmds.setAttr(jnt+'.joz', k=True)