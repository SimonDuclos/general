import orig
import ctlSphere
import maya.cmds as cmds
from functools import partial


def createCrvClh(curve, *args):
	#Create Clusters
	clhs=[]
	crv = curve[0]
	name=cmds.textField('cName', q=True, tx=True)
	nbr_cv = cmds.getAttr(crv + '.spans') + cmds.getAttr(crv + '.degree')
	for i in xrange(nbr_cv):
		cls = cmds.cluster(crv + '.cv[' + str(i) + ']', n='C_'+name+'_Cluster_{}_CLS'.format(str(i).zfill(3)))
		handle = cls[1]
		clh = cmds.rename(handle, 'C_'+name+'_ClusterHandle_{}_CLH'.format(str(i).zfill(3)))
		clhs.append(clh)
	return clhs

def createLatClh(*args):
	clhs=[]
	name=cmds.textField('cName', q=True, tx=True)
	lattice = cmds.ls(sl=True, fl=True) 
	all=False
	if len(lattice[0].split('.'))==1:
		cmds.select(lattice[0]+'.pt[*][*][*]')
		lattice = cmds.ls(sl=True, fl=True)
		all=True
	x=1
	for latpt in lattice: 
		cls = cmds.cluster(latpt, n='C_'+name+'_Cluster_{}_CLS'.format(str(x).zfill(3)))
		handle = cls[1]
		clh = cmds.rename(handle, 'C_'+name+'_ClusterHandle_{}_CLH'.format(str(x).zfill(3)))
		clhs.append(clh)
		x=x+1
	return clhs

def createClhCtls(clhs, ctlSize, *args):
	name=cmds.textField('cName', q=True, tx=True)
	i=1
	print ('name='+name)
	for clh in clhs:
		if name!='':
			clh = cmds.rename(clh, 'C_'+name+'_ClusterHandle_'+str(i).zfill(2)+'_CLH')
		print ('clh name'+clh)
		name_split=clh.split('_')
		name_split[-1] = "CTL"
		ctl_name = "_".join(name_split)		
		ctl = ctlSphere.ctlsphere(ctl_name, ctlSize)
		ctlshape = ctl[1]
		ctl=ctl[0]
		cmds.delete(cmds.pointConstraint(clh, ctl, maintainOffset = False))
		cmds.parent(clh,ctl)

		group_name = str(ctl.replace('CTL', 'GRP'))
		trans = cmds.xform(ctl, q=True, ws=True, t=True)
		rot = cmds.xform(ctl, q=True, ws=True, ro=True)
		orig = cmds.group(em=True, n=group_name)
		cmds.xform(orig, t=trans, ws=True)
		cmds.xform(orig, ro=rot, ws=True)
		par = cmds.listRelatives(ctl, p=True, f=True)
		if par:
		    cmds.parent(orig, par)
		cmds.parent(ctl, orig)
		cmds.hide(clh)
		i+=1

def CtlsOnCrv(*args):
	clhs = createCrvClh(cmds.ls(sl=True))
	createClhCtls(clhs, cmds.floatField('cSize', q=True, v=True))

def CtlsOnLat(*args):
	clhs= createLatClh()
	createClhCtls(clhs, cmds.floatField('cSize', q=True, v=True))

def ClsFuncWin(*args):
	if cmds.window("clsFuncWin", exists=True):
	    cmds.deleteUI("clsFuncWin")
	window = cmds.window("clsFuncWin", title= "Cluster Function Tools", w=200, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])
	cmds.columnLayout('sepLayout', w=200, columnAlign='center')
	cmds.separator(h=10, style='none')
	cmds.text('Controller Parameters', w=200)
	cmds.rowColumnLayout('text', numberOfColumns=2, columnWidth=[(1, 70),(2,100)], columnSpacing=[(1,10),(2,12)])
	
	cmds.text('Name', align='left')
	cmds.textField('cName', w=50)
	cmds.text('Size', align='left')
	cmds.floatField('cSize', w=50, v=.5)
	cmds.setParent('..')
	cmds.rowColumnLayout('buttons', numberOfColumns=1, columnWidth=[(1, 180)], columnAlign=(1,'center'), columnSpacing=(1,10))
	cmds.separator(h=10, style='none')
	cmds.button('Curve Point Cluster', h=30, backgroundColor= [0.1,.4,.25], c=CtlsOnCrv)
	cmds.separator(h=25)
	cmds.button('Lattice Point Cluster', h=30, backgroundColor= [0.1,.4,.25], command=CtlsOnLat )
	cmds.separator(h=25)
	cmds.button('CTL on Cluster', h=30, backgroundColor= [0.1,.4,.25], command=partial(createClhCtls, cmds.ls(sl=True), cmds.floatField('cSize', q=True, v=True)))	
	cmds.showWindow(window)
	cmds.window(window, edit=True, w=205, h=235) 

