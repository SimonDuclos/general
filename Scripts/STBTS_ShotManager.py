import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import users

def exitWindow(*args):
    cmds.deleteUI("FShotCreate")

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def MakeFolder(path, *args):
    make_dir(path)
    make_dir(path+'/_OLD')
    make_dir(path+'/_Useful')
    make_dir(path+'/_PUB')

def MakeShotDir(filepath, *args):
    newShotName=cmds.textField('aName', q=1, tx=1)
    newShotPath=filepath+'/'+newShotName
    make_dir(newShotPath)
    MakeFolder(newShotPath)
    
    

def shotTypeCreate(filepath, *args):
    filepathA=filepath+'/03_Layout'
    filepathB=filepath+'/04_Animation'
    filepathC=filepath+'/05_Lighting'

    MakeShotDir(filepathA)
    MakeShotDir(filepathB)
    MakeShotDir(filepathC)
    pdir= cmds.workspace(q=True, rd=True)
    shotMenu(pdir)
    exitWindow()

def ShotCreateWindow(filepath, *args):
    if cmds.window("FShotCreate", exists=True):
        cmds.deleteUI("FShotCreate")
    window = cmds.window("FShotCreate", title= "Create Shot", w=200, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Shot Name')
    cmds.textField('aName', tx='')
    
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    
    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=2, columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.button('Create', backgroundColor= [0.1,.4,.25], command=partial(shotTypeCreate, filepath)) 
    cmds.button('Exit', backgroundColor= [0.4,.1,.25], command=exitWindow)

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=185)

def forestSave(filepath, project, Shot, job, saveas, *args):
    username= cmds.optionMenu('initials', q=True, v=True)
    pname= project
    vrs=1
    filename= pname+'_'+Shot+'_'+job+'_'+str(vrs).zfill(3)+'_'+username
            
    if not saveas:
        cmds.file(new=True, force=True)    
    cmds.file( rename=filepath+'/'+filename)
    cmds.file( save=True, type='mayaAscii' )
    cmds.deleteUI('FSaveWindow')



def CreateVRSWindow(filepath, *args):

    project=cmds.text('project', q=True, l=True)
    Shot=cmds.optionMenu('shotMenu', q=True, v=True)
    job=cmds.optionMenu('JobMenu', q=True, v=True).split('_')[1]
    name=''
    
    if cmds.window("FSaveWindow", exists=True):
        cmds.deleteUI("FSaveWindow")
    window = cmds.window("FSaveWindow", title= "Forest Saving Tool", w=300, h=150, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Project Name')
    cmds.textField('pname', tx=project)

    cmds.text('Shot Name')
    cmds.textField('Shot', tx=Shot) 
        
    cmds.text('Initials')
    cmds.optionMenu('initials')
    cmds.menuItem(name)
    for user in (users.users):
        if user[1] != name:
            cmds.menuItem(label = user[1])
    
    
    cmds.setParent("..")
    cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
        
    cmds.separator()
    
    cmds.text("  ")

    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=1, columnWidth=(1,300))

    cmds.button('Create', backgroundColor= [0.1,.4,.25], command=partial(forestSave, filepath, project, Shot, job, False))    
    cmds.button('SaveAs', l='Save As', backgroundColor= [0.1,.4,.25], command=partial(forestSave, filepath, project, Shot, job, True))  
    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=150) 


def clearMenu(menu, *args):
    menuItems = cmds.optionMenu(menu, q=True, itemListLong=True) # itemListLong returns the children
    if menuItems:
        cmds.deleteUI(menuItems)

def editProject(*args):
    pdir= cmds.workspace(q=True, rd=True)
    ppath = cmds.fileDialog2(fm=3, dir=pdir) 
    pname =pdir.split('/')[-2]
    cmds.text('project', e=True, l= pname)
    projectdirs = [f for f in os.listdir(ppath) if os.path.isdir(os.path.join(ppath, f))]
    for dirs in projectdirs:
        if '3D' in dirs:
            ShotsDir = ppath+dirs+'/01_Shots/'

    
    cmds.textField('SDir', e=True, tx=ShotsDir)

    types = [f for f in os.listdir(ShotsDir) if os.path.isdir(os.path.join(ShotsDir, f))]
    clearMenu('shotMenu')
    if path.isdir(ShotsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem( parent = ( 'shotMenu'), l=type)
    else :
        cmds.menuItem( parent = ( 'shotMenu'), l='Check Shot Directory')

def forestShotManager(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    pname =pdir.split('/')[-2]
    projectdirs = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]

    if path.isdir(pdir):
        episodes = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]

    if cmds.window("ShotManagerWindow", exists=True):
        cmds.deleteUI("ShotManagerWindow")
    window = cmds.window("ShotManagerWindow", title= "Storybots Shot Manager", w=500, h=200, sizeable=True, backgroundColor= [0.1,.2,.2])

    cmds.columnLayout('episodeMenu')
    cmds.rowColumnLayout("ProjectLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=30, style='none')
    cmds.text('project', al='center', font='fixedWidthFont', l=pname)
    cmds.setParent('..')
    cmds.rowColumnLayout("editLayout", numberOfColumns=3, columnWidth=[(1,230),(2,40),(3,230)])
    cmds.separator( height=20, style='none')
    cmds.button('Edit', backgroundColor= [0.1,.4,.25], h=12, command=editProject) 
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.setParent('..')

    cmds.rowColumnLayout("EpisodeLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.rowColumnLayout('EpisodeColumnLayout', nc=2, cw=[(1,150),(2,330)], cs=(2,10))
    cmds.text('Episode', backgroundColor= [0.1,.4,.25])

    cmds.optionMenu('EpisodeMenu',backgroundColor= [0.1,.4,.4], cc= partial(shotMenu, pdir))
    for episode in episodes:
        if episode!='.DS_Store':
            cmds.menuItem(episode) 
    
    cmds.setParent('..')
    cmds.rowColumnLayout("separatorLayout", numberOfColumns=1)

    cmds.separator( height=20, style='none')
    cmds.rowColumnLayout("windowLayout", numberOfColumns=1)
    shotMenu(pdir) 

    cmds.showWindow(window)
    #cmds.window(window, edit=True, w=200, h=170) 

def shotMenu(pdir, *args):
    
    if cmds.optionMenu('shotMenu', exists=True):
        cmds.deleteUI('shotMenu')
    if cmds.rowColumnLayout('typeLayout', exists=True):
        cmds.deleteUI('typeLayout')
    if cmds.rowColumnLayout('ShotDirLayout', exists=True):
        cmds.deleteUI('ShotDirLayout')
    if cmds.rowColumnLayout('NamesLayout', exists=True):
        cmds.deleteUI('NamesLayout')
    if cmds.tabLayout('tabLayout', exists=True):
        cmds.deleteUI('tabLayout')
    if cmds.optionMenu('JobMenu', exists=True):
        cmds.deleteUI('JobMenu','jobLayout','jobSep')
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.setParent('..')
    episodeName = cmds.optionMenu('EpisodeMenu', q=True, v=True)
    edir = pdir+'/'+episodeName
    episodedirs = [f for f in os.listdir(edir) if os.path.isdir(os.path.join(edir, f))]
    for dirs in episodedirs:
        if '3D' in dirs:
            ShotsDir = edir+'/'+dirs+'/05_Lighting/'

            if path.isdir(ShotsDir):
                types = [f for f in os.listdir(ShotsDir) if os.path.isdir(os.path.join(ShotsDir, f))]

    cmds.rowColumnLayout("ShotDirLayout", numberOfColumns=2, columnWidth=[(1,100),(2,400)], p="windowLayout")
    cmds.text('Shot Directory')
    cmds.textField('SDir', tx=ShotsDir, backgroundColor= [0.05,.15,.15])
    cmds.setParent('..')


    tabs= cmds.tabLayout('tabLayout', imw=5, imh=5, w=500, h=250, hlc=[0.1,.4,.25], cr=True)
    cmds.columnLayout('                                  VRS                                  ', h=180, parent=tabs, cal='center', adj=True)
    cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=10, style='in')
    cmds.rowColumnLayout('typeLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)])
    cmds.text('Shot N.', backgroundColor= [0.1,.4,.25])

    cmds.optionMenu('shotMenu',backgroundColor= [0.1,.4,.4], cc= partial(ShotJob, ShotsDir, False, "NamesLayout"))
    if path.isdir(ShotsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem(type) 
    else :
        cmds.menuItem( parent = ('shotMenu'), l='Check Shot Directory')
    cmds.setParent('..')
    cmds.separator(h=15, style='in' )       
    ShotJob(ShotsDir, False, "NamesLayout")

    cmds.button('createButton', l='Create',  backgroundColor= [0.4,.1,.25], c=partial(ShotCreateWindow, ShotsDir.rstrip('/05_Lighting/')))
    cmds.setParent('..')
    cmds.separator(h=15, style='in' )



    parentr=cmds.columnLayout('                         Reference PUB                         ', cal='center', h=180, parent=tabs, adj=True)
    cmds.rowColumnLayout("RefNamesLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=10, style='in')
    cmds.rowColumnLayout('ReftypeLayout', nc=2, cw=[(1,150),(2,330)], cs=(2,10))
    cmds.text('Shot Type', backgroundColor= [0.1,.4,.25])
    cmds.optionMenu('RefshotMenu',backgroundColor= [0.1,.4,.4], cc= partial(ShotJob, ShotsDir, True, "RefNamesLayout"))
    if path.isdir(ShotsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem(type) 
    else :
        cmds.menuItem( parent = ('shotMenu'), l='Check Shot Directory')
    cmds.setParent('..')
    cmds.separator(h=15, style='in' )           
    ShotJob(ShotsDir, True, "RefNamesLayout")


def ShotJob(ShotsDir, ref, parent, *args):
    if cmds.optionMenu('JobMenu', exists=True):
        cmds.deleteUI('JobMenu','jobLayout','jobSep')
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    ShotsDir=ShotsDir.rstrip('/05_Lighting/')
    
    cmds.rowColumnLayout('jobLayout', nc=2, cw=[(1,150),(2,230)], cs=(2,10), p=parent)
    cmds.text('jobText', l='Job', backgroundColor= [0.1,.4,.25])
    ShotNum = cmds.optionMenu('shotMenu', q=True, v=True)
    jobs = ['03_Layout', '04_Animation', '05_Lighting']
    if ref:
        cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(jobPub, ShotNum, ShotsDir, parent))
    else:
        cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(jobVRS, ShotNum, ShotsDir, parent))
    for job in jobs:
        if job!='.DS_Store':
            cmds.menuItem(job) 
    cmds.setParent('..')
    cmds.separator('jobSep', h=15, style='in' )


def jobVRS( ShotNum, ShotsDir, parent, *args):
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.rowColumnLayout('VRSLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    cmds.text('VRS', l='VRS', backgroundColor= [0.1,.4,.25])
    jobType = cmds.optionMenu('JobMenu', q=True, v=True)
    vrs = [f for f in os.listdir(ShotsDir+'/'+jobType+'/'+ShotNum) if os.path.isfile(os.path.join(ShotsDir+'/'+jobType+'/'+ShotNum, f))]
    listvrs=[]
    filepath=ShotsDir+'/'+jobType+'/'+ShotNum+'/'
    vrssep=False
    cmds.optionMenu('FileMenu', backgroundColor= [0.1,.4,.4], cc=partial(openButton, ShotNum, ShotsDir, jobType, parent))
    if vrs:
        for vr in vrs:
            vr=vr.split('.')[0]
            num=vr.split('_')[5]
            listvrs.append(int(num))    
        if listvrs:
            vrsmax=max(listvrs)
        
        for vr in vrs:
            if vr!='.DS_Store':
                cmds.menuItem(vr) 
                if str(vrsmax) in vr.split('_')[5]:
                    cmds.optionMenu('FileMenu', e=True, v=vr)
                    cmds.setParent('..')
                    vrssep=cmds.separator('vrsSep', h=15, style='in' )
                    openButton(ShotNum, ShotsDir, jobType, parent)

    
    if not vrssep:
        cmds.button('createVRSButton', l='Create',  backgroundColor= [0.4,.1,.25], c=partial(CreateVRSWindow, filepath))
        cmds.setParent('..')
        cmds.separator('vrsSep', h=15, style='in' )

def jobPub(ShotNum, ShotsDir, parent, *args):
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.rowColumnLayout('VRSLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    cmds.text('VRS', l='PUB', backgroundColor= [0.1,.4,.25])
    jobType = cmds.optionMenu('JobMenu', q=True, v=True)
    vrs = [f for f in os.listdir(ShotsDir+'/'+jobType+'/'+ShotNum+'/_PUB') if os.path.isfile(os.path.join(ShotsDir+'/'+jobType+'/'+ShotNum+'/_PUB', f))]
    listvrs=[]
    vrssep=False
    cmds.optionMenu('FileMenu', backgroundColor= [0.1,.4,.4], cc=partial(refButton, ShotNum, ShotsDir, jobType, parent))
    if vrs:
        for vr in vrs:
            vr=vr.split('.')[0]
            num=vr.split('_')[6]
            listvrs.append(int(num))    
        if listvrs:
            vrsmax=max(listvrs)
        
        for vr in vrs:
            if vr!='.DS_Store':
                cmds.menuItem(vr) 
                if str(vrsmax) in vr.split('_')[6]:
                    cmds.optionMenu('FileMenu', e=True, v=vr)
                    cmds.setParent('..')
                    vrssep=cmds.separator('vrsSep', h=15, style='in' )
                    cmds.rowColumnLayout('vrsbuttonLayout', nc=2, cw=[(1,250),(2,250)], p=parent)
                    refButton(ShotNum, ShotsDir, jobType, parent)
                    replaceRefButton(ShotNum, ShotsDir, jobType, parent)

    
    if not vrssep:
        cmds.setParent('..')
        cmds.separator('vrsSep', h=15, style='in' )

def openButton(ShotNum, ShotsDir, jobType, parent, *args):
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    filename = cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = ShotsDir+'/'+jobType+'/'+ShotNum+'/'+filename
    cmds.button('openButton',  l='Open', p=parent, w=500, h=35,  backgroundColor= [0.4,.1,.25], c=partial(openScene, filepath, ShotNum, ShotsDir, jobType))


def openScene(filepath, *args):
    cmds.file(filepath, o=True, force=True)

def refButton(ShotNum, ShotsDir, jobType, parent, *args):
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    pub= cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = ShotsDir+'/'+jobType+'/'+ShotNum+'/_PUB/'+pub
    cmds.button('refButton', l='Reference', h=35,  p='vrsbuttonLayout', backgroundColor= [0.4,.1,.25], c=partial(importRef, filepath, ShotNum, ShotsDir, jobType, pub))

def replaceRefButton(ShotNum, ShotsDir, jobType, parent, *args):
    if cmds.button('replacerefButton', exists=True):
        cmds.deleteUI('replacerefButton')
    pub= cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = ShotsDir+'/'+jobType+'/'+ShotNum+'/_PUB/'+pub
    cmds.button('replacerefButton', l='Replace Reference', p='vrsbuttonLayout', h=35,  backgroundColor= [0.4,.1,.25], c=partial(replaceRef, filepath, ShotNum, ShotsDir, jobType, pub))

def importRef(filepath, ShotNum, ShotsDir, jobType, pub, *args):
    if cmds.file(filepath, q=True, ex=True):
        cmds.file(filepath, r=True, ns=ShotNum+'_'+jobType.split('_')[1])

def replaceRef(filepath,ShotNum, ShotsDir, jobType, pub, *args):
    refnode=cmds.referenceQuery(cmds.ls(sl=True)[0], rfn=True)
    if cmds.file(filepath, q=True, ex=True):
        cmds.file(filepath, lr=refnode)