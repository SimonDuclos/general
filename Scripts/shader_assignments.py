import pprint
import re
import os
import copy
import json
import maya.OpenMaya as om
import maya.cmds as mc
import maya.mel as mel


ignore_attr_list = [
'rsEnableSubdivision',
'rsSubdivisionRule',
'rsScreenSpaceAdaptive',
'rsDoSmoothSubdivision',
'rsMinTessellationLength',
'rsMaxTessellationSubdivs',
'rsOutOfFrustumTessellationFactor',
'rsEnableDisplacement',
'rsMaxDisplacement',
'rsDisplacementScale',
'rsAutoBumpMap'
]

default_shading_nodes = [
'defaultColorMgtGlobals'
]

keep_shader_nodes = [
'groupId',
'skinCluster',
'cMuscleMultiCollide',
'blendShape',
'objectSet',
'ffd',
'groupParts',
'time',
'wrap',
'plusMinusAverage',
'multiplyDivide',
'nonLinear',
'RedshiftProxyMesh',
'place2dTexture',
'place3dTexture',
'file',
'ramp'
]

none_deletable = [
'particleCloud1',
'initialShadingGroup',
'initialParticleSE',
'lambert1'
]

renderStats = [
'castsShadows',
'receiveShadows',
'motionBlur',
'primaryVisibility',
'smoothShading',
'visibleInReflections',
'visibleInRefractions',
'doubleSided',
'castsShadows',
'holdOut'
]

redshift_render_attr = [
"rsEnableVisibilityOverrides",
"rsPrimaryRayVisible",
"rsSecondaryRayVisible",
"rsShadowCaster",
"rsShadowReceiver",
"rsSelfShadows",
"rsAOCaster",
"rsRefractionVisible",
"rsRefractionCaster",
"rsFgVisible",
"rsGiVisible",
"rsCausticVisible",
"rsFgCaster",
"rsCausticReceiver",
"rsGiReceiver",
"rsGiCaster"
]


write_rs_attr = [
"rsEnableSubdivision",
"rsEnableDisplacement"
]




def get_uv_links():
    """ Get the uv links dictionary.

        Return:
           uv_links (dictonary): file_node to links .
    """
    uv_links = {}
    for file_node in mc.ls(type=['file', 'ramp']):
        links = mc.uvLink( q=True, texture=file_node)
        if links:
            uv_links[file_node] = links
    return uv_links

#########################################################
#########################################################







#########################################################
#########################################################


def save_scene_shader_assignments(file_path, partial=False):
    """ Export the shader assignments
        and attribute settings.

        Args:
           file_path (str): File path to save to.

        Kwargs:
            partial (bool): The short name of geometry.
    """
    uv_links = get_uv_links()
    old_iter = om.MItDag(om.MItDag.kDepthFirst)
    shaders = []
    shader_dict = {'full'       : {},
                   'partial'    : {},
                   'attrs'      : {},
                   'uv_links'   : uv_links
                   }
    while(1):
        shader_path = om.MDagPath()
        old_iter.getPath(shader_path)
        partial_name = shader_path.partialPathName()
        full_name =    shader_path.fullPathName()


        if old_iter.currentItem().hasFn( om.MFn.kNurbsSurface  ):
            mfn_dep = om.MFnDependencyNode(old_iter.currentItem())
            attr = mfn_dep.findPlug ("intermediateObject")
            if not attr.asBool():
               
                #print old_iter.partialPathName(),"is nurb"
                #print "is nurs"
                try:
                    mfn_nurb = om.MFnNurbsSurface(old_iter.currentItem())
                except:
                    old_iter.next()
                shape_path = om.MDagPath()
                old_iter.getPath(shape_path)
                shader_array = om.MObjectArray()
                shader_face_array = om.MIntArray()
               
                inst_id = shape_path.instanceNumber()
               
                instObjGroupsAttr  =  mfn_nurb.attribute("instObjGroups")
                instPlug = om.MPlug(old_iter.currentItem() , instObjGroupsAttr)
                instPlugElem  = instPlug.elementByLogicalIndex( inst_id )
                SGPlugArray = om.MPlugArray()
                instPlugElem.connectedTo(SGPlugArray, False, True)
               
                attributes = get_attrs(full_name)
                #print SGPlugArray.length()
                if SGPlugArray.length() == 0:
                    shader = None
                    #print "No Shader " ,old_iter.partialPathName()
                    shaders.append(None)
                    shader_dict['full'][full_name] = shader
                    shader_dict['partial'][partial_name] = shader
                    shader_dict['attrs'][full_name] = attributes
                else:
                    for x in range(SGPlugArray.length()):
                        #print "DERP"
                        node_check = om.MFnDependencyNode(SGPlugArray[x].node())
                        shaders.append(node_check.name())
                        shader = node_check.name()
                        #print node_check.name() , old_iter.partialPathName()
                        shader_dict['full'][full_name] = shader
                        shader_dict['partial'][partial_name] = shader
                        shader_dict['attrs'][full_name] = attributes
                       
        elif old_iter.currentItem().hasFn( om.MFn.kMesh  ):
           
            shape = True
            mfn_dep = om.MFnDependencyNode(old_iter.currentItem())
            attr = mfn_dep.findPlug ("intermediateObject")
            if not attr.asBool():
                try:
                    mfn_mesh = om.MFnMesh(old_iter.currentItem())
   
                    shape_path = om.MDagPath()
                    old_iter.getPath(shape_path)
                    shader_array = om.MObjectArray()
                    shader_face_array = om.MIntArray()
                    if old_iter.isInstanced():
                        inst_id = shape_path.instanceNumber()
                    else:
                        inst_id = "not_instanced"
                    if inst_id == "not_instanced":

                        mfn_mesh.getConnectedShaders(0,shader_array,shader_face_array)
                    else:
                        mfn_mesh.getConnectedShaders(inst_id,shader_array,shader_face_array)
                   
                    if not shader_array.length() == 0:
                        shader = om.MFnDependencyNode(shader_array[0])
                        shader_name = shader.name()
                        shaders.append(shader.name())
                        attributes = get_attrs(full_name)
                        shader_dict['full'][full_name] = shader_name
                        shader_dict['partial'][partial_name] = shader_name
                        shader_dict['attrs'][full_name] = attributes
                       
                       
                except RuntimeError as e:
                    if not str(e).find("Shape has no geometry") == -1:
                   
        old_iter.next()
        if old_iter.isDone():
           break
    if partial:
        with open(file_path, 'w') as outfile:
            json.dump(shader_dict['partial'], outfile)
    else:
        with open(file_path, 'w') as outfile:
            json.dump(shader_dict, outfile)

def return_m_obj(name):
    l = om.MSelectionList()
    l.add(name)
    o_obj = om.MObject()
    l.getDependNode(0, o_obj)
    return o_obj

def get_attrs(node):
    """ Get the commands to add
        attributes and set attributes.

        Args:
            node (str): Object to get attributes.

        Return:
            attr_dict (dictionary): Set and add attributes command.
    """
    attr_dict = { "addAttr":[] , "setAttr":[] }
    mobj = return_m_obj(node)
    mfn_dep = om.MFnDependencyNode(mobj)
    attr_count = mfn_dep.attributeCount()
    for x in range( attr_count ):
        attr = mfn_dep.attribute(x)
        mfn_attr = om.MFnAttribute(attr)
        attr_name = mfn_attr.name()

        # ignore redshift attributes
        #if attr_name in ignore_attr_list:
          #  continue
        m_plug = mfn_dep.findPlug(attr_name )
        if len(attr_name) > 2:
            if attr_name[0:2] in ['rs', 'ai', 'im'] or mfn_attr.isDynamic() or attr_name in renderStats:
                attr_dict['addAttr'].append( mfn_attr.getAddAttrCmd().replace("\t","") )

                # get all changed attributes
                # and write to file
                commands = []
                m_plug.getSetAttrCmds( commands , om.MPlug.kNonDefault, True ) or []
                for command in commands:
                    attr_dict['setAttr'].append(command.replace("\t",""))

            # get the redshift attributes enabled.
            if attr_name in write_rs_attr:
                rs_commands = []
                m_plug.getSetAttrCmds( rs_commands, om.MPlug.kAll, True ) or []
                for rs_command in rs_commands:
                    attr_dict['setAttr'].append(rs_command.replace("\t",""))

    return attr_dict




def get_shading_network(sg):
    """ From a shading node get a list
        of all nodes in the shading network.

        Args:
            sg (str): The shading engine to check.

        Return:
            shading_network (list): All nodes in the shading network.
    """
    shading_network = []
    shader_types = mc.listNodeTypes("shader")
    sg = mc.hyperShade( lun=sg )
    for node in sg:
        if mc.objectType( node ) in shader_types:
            shading_network.append(node)
            material = mc.hyperShade( lun=node )
            for mat in material:
                shading_network.append(mat)

        # fix for bumps
        incoming_connections = mc.listConnections(node, d=False, s=True)
        if not incoming_connections:
            return shading_network
           
        for con in incoming_connections:
            if con in shading_network:
                continue
            shading_network.append(con)
 
            material = mc.hyperShade( lun=con )
            for mat in material:
                if mat in shading_network:
                    continue
                shading_network.append(mat)
    return shading_network


def return_match( raw_obj, geo_namespace ):
    """ Slightly messy function but good to
        find the matching piece of geometry
        to the namespace and raw name.

        Args:
            raw_obj (str): The object full name without namespace.
            geo_namespace (str): Geometry namespace to search for.

        Returns:
            returner (str): Object in scene name.
    """
    returner = False
    new_space = ""
    old_hier_split = raw_obj.split("|")
    for split in old_hier_split[1:]:
        geo_grp =  split.split(":")[-1]
        new_space += ( "|" +geo_namespace +":"+ geo_grp)
   

    new_space_splitup = copy.copy( new_space )
    if not mc.objExists(new_space):
        new_space_found = False
    else:
        new_space_found = True
        returner = new_space
       
    if not new_space_found:
        new_space_found_split = new_space_splitup.split("|")
        new_space_found_len = len( new_space_found_split )
        for x in range( new_space_found_len ):
            find_str = ""
            for grp in new_space_found_split[x:]:
               
                find_str =  find_str + "|" + grp
               
               
            if not mc.objExists(find_str):
                new_space_found = False
            else:
                new_space_found = True
                returner = find_str
                return returner
       
    # If Still failed - does not have single
    # namespace, try adding to original path
    # a:b:geo + "ns" = ns:a:b:geo  
    if not returner:
        new_find = "|"
        for hier in  raw_obj.split("|"):
            #print hier
            new_space = geo_namespace+":"
            if len(hier.split(":")) >1:
                for grp in hier.split(":")[1:]:
                    new_space = new_space +  grp +":"
                new_find = new_find + new_space[:-1] +"|"
        if mc.objExists(new_find):
            returner = new_find

    if not returner:
        added_namespace = raw_obj.replace("|","|"+geo_namespace+":")
        if mc.objExists( added_namespace ):
            returner = added_namespace
        else:
            ls_find =  mc.ls("*"+ added_namespace)
            if not ls_find == []:
                returner = ls_find[0]

    # get the object based on the short name.
    if not returner:
        shortName = raw_obj.split('|')[-1]
        name = shortName.split(':')[-1]
        basename = '%s:%s' % (geo_namespace, name )
        try:
            objectExists = mc.ls(basename, l=True)
           
           
            if objectExists:
                returner = objectExists[0]
        except:
            pass
   
    # Assume Longnaming is the partial path
    #  name ie namespace:Transform|namespace:Shape
    if not returner:
        if raw_obj.count("|") > 1:
            lowest = ""
            hier = raw_obj.split("|")
            hier.reverse()
            for node in hier:
                if not lowest == "":
                    lowest = geo_namespace+":"+node+"|"+lowest
                else:
                    lowest = geo_namespace+":"+node+lowest
               
                search = []
                try:
                    search = mc.ls(lowest)
                except:
                    search = []
                if not search == []:
                    returner = lowest
                    break
   
    return returner
    

def find_shader(shaders, geo_namespace, raw_assignment):
    """ Find the shader to assign to the geometry.

        Args:
            shaders (str): Shaders namespaces to assign to.
            geo_namespace (str): Geometry namespace to search for.
            raw_assignment (str): Geometry name without the namespace.

        Returns:
            shader_w_ns (str): Shader name to assign.
    """
    shader_w_ns = "%s:%s" %  (shaders, raw_assignment)
    if mc.objExists( shader_w_ns ):
        return shader_w_ns

    shader_w_ns = shader_w_ns[1:]
    if mc.objExists( shader_w_ns ):
        return shader_w_ns

    shader_w_ns = "%s:%s" %  (geo_namespace, raw_assignment)
    if mc.objExists( shader_w_ns ):
        return shader_w_ns

    shader_w_ns = shader_w_ns[1:]
    if mc.objExists( shader_w_ns ):
        return shader_w_ns


def uv_link_to_objects(uv_links, shaders, geo_namespace):
    """ Connect the uvlinked to the object.

        Args:
            uv_links (dictionary): UV to the geometry to connect to .
            shaders (str): Shaders namespaces to assign to.
            geo_namespace (str): Geometry namespace to search for.
    """
    # uv links
    for link, geometry in uv_links.iteritems():
        texture_name = '%s:%s' % (shaders, link)
        texture_node = mc.ls(texture_name)
        if not texture_node:
            continue

        for geo in geometry:
            uv_set_name = '%s:%s' % (geo_namespace, geo)
            uv_sets = mc.ls(uv_set_name)
            if not uv_sets:
                uv_set_name = uv_set_name[1:]
                uv_sets = mc.ls(uv_set_name)

            if not uv_sets:
                continue
            for uv_set in uv_sets:
                mc.uvLink( make=True, uvSet=uv_set, texture=texture_node[0] )


def reassign_shaders(assignments, shaders, geo_namespace, clean_up_shader=True):
    """ From the json file reassign the
        shaders to the geometry.

        Args:
            assignments (str): Path to the assignments json.
            shaders (str): Shaders namespaces to assign to.
            geo_namespace (str): Geometry namespace to search for.

        Kwargs:
            clean_up_shader (bool): If true remove reference
                                    edits before assigning.
    """
    if clean_up_shader:
        clean_up_shader_assignments(shaders)

    selection = mc.ls(sl=1)
    message = "Would you like to set Rendering Attributes from Shader File also on\n\nReference: "+geo_namespace
    message+="\n( sets Attributes Such as aiSmoothing etc)"

    set_attr = False
    with open(assignments) as filepath:
        assignment_dict = json.load(filepath)
    breaker = 0

    for raw_obj, raw_assignment in assignment_dict['full'].items():
        if not raw_assignment == "initialShadingGroup":

            # find the geometry in the scene
            returned_match = return_match(raw_obj, geo_namespace)
            if not returned_match:
                returned_match = return_match("%sDeformed" % raw_obj, geo_namespace)

            if returned_match:
                shader_w_ns = find_shader(shaders, geo_namespace, raw_assignment)
                if shader_w_ns and mc.objExists( shader_w_ns ):
                    assignShaders = 'assignSG "%s"  "%s";'  %  (shader_w_ns, returned_match)
                    mel.eval(assignShaders)
                    apply_attrs( raw_obj , returned_match , assignment_dict["attrs"][raw_obj])
                   
                else:
                    pass
            else:

       
        breaker +=1
        if breaker >500:pass

    if selection:
        mc.select(selection)

    # uv links
    if "uv_links" in assignment_dict.keys():
        uv_links = assignment_dict["uv_links"]
        uv_link_to_objects(uv_links, shaders, geo_namespace)
        mc.select(selection)

    # if the attribute exists
    attribute = '%s:C_attributePublish_LOC.attributePublish' % geo_namespace

    if mc.objExists(attribute):
        connect_ctl_to_shd(shaders, geo_namespace)


def assign_default(name_space):
    """ Assigning default to objects with namespace.

        Args:
            name_space (str): The namespace to assign it to.
    """

    meshes = mc.ls(name_space + ':*', type='mesh')
    for mesh in meshes:
        transform = mc.listRelatives(mesh, p=True)[0]
        assignShaders = 'assignSG "initialShadingGroup" "%s";'  %  transform
        mel.eval(assignShaders)



def apply_attrs( raw_obj, returned_match, assignment_dict):
    """ Add and set any attributes
        that were in the json file.

        Args:
            raw_obj (str): The object name without its namespace.
            returned_match (str): Object to add the attributes to.
            assignment_dict (dictionary): The objects attributes to add and set.
    """
    mc.select(returned_match)
    for attr in assignment_dict['addAttr']:
        try:
            mel.eval('catchQuiet ( "'+attr.replace('"',"'")+'" );' )
        except:
            pass
       
    mc.select(returned_match)
    for attr in assignment_dict['setAttr']:
        attrname = attr.split('"')[1].replace('.','')
        try:
            mel.eval(attr)
        except:
            pass


def getEdits():
    """ Get the reference edits on an object.
    """
    referenceNode=om.MObject()
    selectionList=om.MSelectionList()
    selectionList.add(ref)
    selectionList.getDependNode(0, referenceNode)
    referenceNodeMFn=om.MFnDependencyNode(referenceNode)
    editsPlug=referenceNodeMFn.findPlug("edits")
    attrCommands = []
    editsPlug.getSetAttrCmds(attrCommands, om.MPlug.kAll, True)



def clean_up_shader_assignments(shader_namespace):
    """ Clean and remove any shader assignments reference edits.

        Args:
            shader_namespace (str): Shader namespace.
    """
    reference_node = False
    for node in mc.ls(et="reference"):
        if node == 'sharedReferenceNode':
            continue
        try:
            ns = mc.referenceQuery(node,ns=1)
            if ns == (':%s' % shader_namespace) or ns == shader_namespace:
                reference_node = node
        except:
            pass

    if reference_node:
        for ref_edit in mc.referenceQuery( reference_node ,es=1, fld=1):
            try:
                ref_edit_split = ref_edit.split('"')
                command = ref_edit_split[0]
                destination = ref_edit_split[1]
                if command == "connectAttr ":
                    plug_split = destination.split(".")
                    if len(plug_split) == 2:
                        node = plug_split[0]
                        plug = plug_split[1]
           
                        if plug.find( "instObjGroups" ) != -1 :
                            mc.referenceEdit( destination  ,editCommand="connectAttr" ,removeEdits=True ,failedEdits =True,successfulEdits=True)
            except:
                pass                  


def get_shader(mesh):
    """ Get the shader assignments from the mesh.

        Args:
            mesh (str): The mesh to find the assingments for.

        Return:
            shaders (list): Shaders assigned.
    """
    shaders = []
    shader_dict = {'full':{},
                   'partial':{},
                   'attrs':{}}
    shader_path = api_tools.return_m_dag(mesh)
    shader_mobj = api_tools.return_m_obj(mesh)
   
    partial_name = shader_path.partialPathName()
    full_name =    shader_path.fullPathName()
   
    if shader_path.hasFn( om.MFn.kNurbsSurface  ):
        mfn_dep = om.MFnDependencyNode( shader_mobj )
        attr = mfn_dep.findPlug ("intermediateObject")
        if not attr.asBool():
           
            mfn_nurb = om.MFnNurbsSurface( shader_path )
            shape_path = shader_path
           
            shader_array = om.MObjectArray()
            shader_face_array = om.MIntArray()
           
            inst_id = shape_path.instanceNumber()
           
            instObjGroupsAttr  =  mfn_nurb.attribute("instObjGroups")
            instPlug = om.MPlug( shader_mobj , instObjGroupsAttr)
            instPlugElem  = instPlug.elementByLogicalIndex( inst_id )
            SGPlugArray = om.MPlugArray()
            instPlugElem.connectedTo(SGPlugArray, False, True)
           
            attributes = get_attrs(full_name)
            # SGPlugArray.length()
            if SGPlugArray.length() == 0:
                shader = None
                shaders.append(None)
                shader_dict['full'][full_name] = shader
                shader_dict['partial'][partial_name] = shader
                shader_dict['attrs'][full_name] = attributes
   
   
            else:
                for x in range(SGPlugArray.length()):
                    node_check = om.MFnDependencyNode(SGPlugArray[x].node())
                    shaders.append(node_check.name())
                    shader = node_check.name()
                    shader_dict['full'][full_name] = shader
                    shader_dict['partial'][partial_name] = shader
                    shader_dict['attrs'][full_name] = attributes
                   
    elif shader_path.hasFn( om.MFn.kMesh  ):
       
        shape = True
        mfn_dep = om.MFnDependencyNode( shader_mobj )
        attr = mfn_dep.findPlug ("intermediateObject")
        if not attr.asBool():
            try:
                mfn_mesh = om.MFnMesh( shader_path )

                shape_path = shader_path
                shader_array = om.MObjectArray()
                shader_face_array = om.MIntArray()
                if shader_path.isInstanced():
                    inst_id = shape_path.instanceNumber()
                else:
                    inst_id = "not_instanced"
                if inst_id == "not_instanced":
                    mfn_mesh.getConnectedShaders(0,shader_array,shader_face_array)
                else:
   
                    mfn_mesh.getConnectedShaders(inst_id,shader_array,shader_face_array)
               
                if not shader_array.length() == 0:
                    shader = om.MFnDependencyNode(shader_array[0])
                    shader_name = shader.name()
                    shaders.append(shader.name())
                    attributes = get_attrs(full_name)
                    shader_dict['full'][full_name] = shader_name
                    shader_dict['partial'][partial_name] = shader_name
                    shader_dict['attrs'][full_name] = attributes
                   
                   
            except RuntimeError as e:
                if not str(e).find("Shape has no geometry") == -1:
   
    return shaders
