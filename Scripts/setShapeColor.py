def setRGBColor(ctrl, colora = (1,1,1)):
    
    rgb = ("R","G","B")
    
    cmds.setAttr(ctrl + ".overrideEnabled",1)
    cmds.setAttr(ctrl + ".overrideRGBColors",1)
    
    for channel, colora in zip(rgb, colora):
        
        cmds.setAttr(ctrl + ".overrideColor%s" %channel, colora)

sel=cmds.ls(sl=True)

for s in sel:
    setRGBColor(s, colora = (0,.5,.5))
