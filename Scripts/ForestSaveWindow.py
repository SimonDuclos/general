import maya.cmds as cmds
import os
import users
  

def FCreateSaveWindow(*args):
    jobs=['MOD','LAYOUT','RIG','Animation','LIGHTING','RENDER']
    if cmds.window("FSaveWindow", exists=True):
        cmds.deleteUI("FSaveWindow")
    window = cmds.window("FSaveWindow", title= "Forest Saving Tool", w=300, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Project Number')
    cmds.textField('pnum', tx='0000')

    cmds.text('Project Name')
    cmds.textField('pname', tx='Project')

    cmds.text('Asset/Shot Name')
    cmds.textField('asset') 

    cmds.text('Job')
    cmds.optionMenu('job')
    cmds.menuItem('MOD')
    cmds.menuItem('SHD')
    cmds.menuItem('LAYOUT')
    cmds.menuItem('RIG')
    cmds.menuItem('Animation')
    cmds.menuItem('Lighting')
    cmds.menuItem('RENDER') 

    cmds.text('Initials')
    cmds.optionMenu('initials')
    for user in (users.users):
        cmds.menuItem(label = user[1])
    
    cmds.text('Note')
    cmds.textField('note')
    
    cmds.setParent("..")
    cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
    
    cmds.separator()
    cmds.text("  ")

    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=2, columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])
    
    cmds.button('Save', backgroundColor= [0.1,.4,.25], command=forestSave)    
    cmds.button('Exit', backgroundColor= [0.4,.1,.25], command=exitWindow)

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=185)
 
 
def filepath_Browse(*args):
    filepath = cmds.fileDialog2(dialogStyle=2, fileMode =3) [0]
 
def FIncSaveWindow(*args):
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])
    jobs=['MOD','LAYOUT','RIG','Animation','Lighting','RENDER', 'SHD']
    former=filename.split('_')
    fprojectnum=former[0]
    fproject=former[1]+'_'+former[2]
    fasset=former[3]
    fjob=former[4]
    fname=former[6]
    fvrs=former[5]
    jobs.remove(fjob) 
    
    if cmds.window("FSaveWindow", exists=True):
        cmds.deleteUI("FSaveWindow")
    window = cmds.window("FSaveWindow", title= "Forest Saving Tool", w=300, h=150, sizeable=True, backgroundColor= [0.1,.2,.2]) 

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Project Number')
    cmds.textField('pnum', tx=fprojectnum)

    cmds.text('Project Name')
    cmds.textField('pname', tx=fproject)

    cmds.text('Asset/Shot Name')
    cmds.textField('asset', tx=fasset) 

    cmds.text('Job')
    cmds.optionMenu('job')
    cmds.menuItem(fjob)
    for job in jobs:
        cmds.menuItem(job)
        

    cmds.text('Initials')
    cmds.optionMenu('initials')
    cmds.menuItem(fname)
    for user in (users.users):
        if user[1] != fname:
            cmds.menuItem(label = user[1])
    
    cmds.text('Note')
    cmds.textField('note')
    
    cmds.setParent("..")
    cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
        
    cmds.separator()
    
    cmds.text("  ")

    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=3, columnWidth=[(1,100), (2,100),(3,80)], columnSpacing=[(1,3), (2,3),(3,4)])

    cmds.button('Inc', backgroundColor= [0.1,.4,.25], command=forestIncSave)
    cmds.button('Save', backgroundColor= [0.1,.4,.25], command=forestSave)    
    cmds.button('Exit', backgroundColor= [0.4,.1,.25], command=exitWindow)

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=175) 
    
def exitWindow(*args):
    cmds.deleteUI("FSaveWindow")
    
def forestSave(*args):
    
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])
    filepath = (os.path.split(filePath)[0])

    inc=False
    
    username= cmds.optionMenu('initials', q=True, v=True)
    pnum= cmds.textField('pnum', q=True, tx=True)
    pname= cmds.textField('pname', q=True, tx=True)
    asset= cmds.textField('asset', q=True, tx=True)
    job= cmds.optionMenu('job', q=True, v=True)
    note= cmds.textField('note', q=True, tx=True) 
    vrs=1


    filepath = cmds.fileDialog2(dialogStyle=2, fileMode =3) [0]
    if asset=="":
        cmds.warning("Asset/Shot field must be filled")
        
    else:
        if note=="": 
            filename= pnum+'_'+pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+username
            
        else:
            filename= pnum+'_'+pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+username+'_'+note
        
        cmds.file( rename=filepath+'/'+filename)
        cmds.file( save=True, type='mayaAscii' )
        cmds.deleteUI('FSaveWindow')

def forestIncSave(*args):
    
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])
    filepath = (os.path.split(filePath)[0])

    inc=False
    
    username= cmds.optionMenu('initials', q=True, v=True)
    pnum= cmds.textField('pnum', q=True, tx=True)
    pname= cmds.textField('pname', q=True, tx=True)
    asset= cmds.textField('asset', q=True, tx=True)
    job= cmds.optionMenu('job', q=True, v=True)
    note= cmds.textField('note', q=True, tx=True) 
    vrs=1

    if filename!='':
        former=filename.split('_')
        inc=True
        fvrs=former[5]

    if inc==True:
        vrs=int(fvrs)+1
    else:
        filepath = cmds.fileDialog2(dialogStyle=2, fileMode =3) [0]
    if asset=="":
        cmds.warning("Asset/Shot field must be filled")
        
    else:
        if note=="": 
            filename= pnum+'_'+pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+username
            
        else:
            filename= pnum+'_'+pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+username+'_'+note
        
        cmds.file( rename=filepath+'/'+filename)
        cmds.file( save=True, type='mayaAscii' )
        cmds.deleteUI('FSaveWindow')


def FSaveWindow(*args):
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])

    filepath = (os.path.split(filePath)[0])

    if filename!='':
        former=filename.split('_')
        if len(former)>3:
            FIncSaveWindow()
        else:
            FCreateSaveWindow()
    else:
        FCreateSaveWindow()


