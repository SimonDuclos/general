import maya.cmds as cmds

def changeMayaColor(*args):
	targetWindows = ['MayaWindow']

	rgbColor = [.1,.2,.2]
	uis = cmds.lsUI(windows=1)
	for ui in uis:
		if any([t in ui for t in targetWindows]):
			cmds.window(ui, e=1, bgc=rgbColor)