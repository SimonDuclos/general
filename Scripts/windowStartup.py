def setparam(self):
    usernum = cmds.optionMenu('User', q=True, sl=True)
    username = users[usernum-1]
    pname = cmds.textField('pname', q=True, tx=True)
    
    
    return username, pname

def userstartup(users):

	if cmds.window('userSetting', exists = True):
	    cmds.deleteUI('userSetting')

	userSetting = cmds.window('userSetting', title="User Setting", widthHeight=(300, 100), titleBarMenu=False, sizeable=False )

	cmds.rowColumnLayout('lay', numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 150), (2, 150)])

	cmds.text('User   ')
	cmds.optionMenu('User')
	for user in users:
		cmds.menuItem(label = user)

	cmds.text('Project   ')
	cmds.textField('pname')

	cmds.setParent('..')

	cmds.columnLayout('ok', adj=True)

	param = cmds.button(l='OK!', command=setparam)

	cmds.showWindow(userSetting)


	return param

