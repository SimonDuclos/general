import maya.cmds as cmds
import maya.mel as mel
import forestToolBar
import ForestSaveWindow
import AutoBendyArms
import SubstanceToRS
import forestRenameTool
import forestRivetTool
import forestCmakerTool
import ChTab
import MoveAllAnim
import forestPublish
import clusterFunctions
import PointLoft
import SelectSkinnedJoints
import Resetter
import showJointOrients
import lucieFeature
import changeMayaColor
import propRig
import AdvanceNaming
import ForestFacialRig
import ChadV_InvertShape
import RigBase
import CtlOnJNT
import addDivider
import AssetManager
import ShotManager
import forestUsersCreate
import forestExportToSubstance
import STBTS_AssetManager
import STBTS_ShotManager
import STBTS_Scripts
import STBTS_ColorSetCreate
import PointWrap
import maxImportClean
import Assembly

def forestMayaMenu():
    gMainWindow = mel.eval('$temp1 = $gMainWindow')
    customMenu = cmds.menu(parent=gMainWindow, label= 'Forest Tools')
    cmds.menuItem(parent=customMenu, label= "Forest Tool Bar", c=forestToolBar.forestToolBar)
    cmds.menuItem(divider=True)
    cmds.menuItem(parent=customMenu, label="Pipeline Tools", itl=True)
    cmds.menuItem(parent=customMenu, label= "Forest Asset Manager", c=AssetManager.forestAssetManager)
    cmds.menuItem(parent=customMenu, label= "Forest Shot Manager", c=ShotManager.forestShotManager)
    cmds.menuItem(parent=customMenu, label= "Forest Saving Tool", c=ForestSaveWindow.FSaveWindow)
    cmds.menuItem(parent=customMenu, label= "Forest Publish Tool", c=forestPublish.pubWindow)
    cmds.menuItem(parent=customMenu, label= "Forest Assembly Tool", c=Assembly.assemblyWin)
    pipeTools= cmds.menuItem(parent =customMenu, label= "Forest Pipeline Management", subMenu=True)
    cmds.menuItem(parent=pipeTools, label= "Forest Create User", c=forestUsersCreate.createUI)
    cmds.menuItem(parent=customMenu, divider=True)
    cmds.menuItem(parent=customMenu, label="Scene Tools", itl=True)
    cmds.menuItem(parent=customMenu, label= "Forest Naming Tool", c=forestRenameTool.fRenameTool)
    modTools= cmds.menuItem(parent =customMenu, label= "Forest Modelling Tools", subMenu=True)
    cmds.menuItem(parent=modTools, label= "Clean FBX Import", c=maxImportClean.maxImportClean)
    cmds.menuItem(parent=modTools, label= "Point Loft", c=PointLoft.Gui)
    riggingTools= cmds.menuItem(parent =customMenu, label= "Forest Rigging Tools", subMenu=True)
    cmds.menuItem(parent=riggingTools, label="Quick Rigs", itl=True)
    cmds.menuItem(parent=riggingTools, label="Base Rig", c=RigBase.createUI)
    cmds.menuItem(parent=riggingTools, label="AutoRig Prop", c=propRig.createUI)
    cmds.menuItem(parent=riggingTools, label="Controls from Joints", c=CtlOnJNT.createUI)
    cmds.menuItem(parent=riggingTools, label="Control Maker", c=forestCmakerTool.controlMaker)
    cmds.menuItem(parent=riggingTools, divider=True)
    cmds.menuItem(parent=riggingTools, label="Tools", itl=True)
    cmds.menuItem(parent=riggingTools, label="Cluster Functions", c=clusterFunctions.ClsFuncWin)
    cmds.menuItem(parent=riggingTools, label="Add Divider", c=addDivider.addDivider)
    cmds.menuItem(parent=riggingTools, label="Rivet", c=forestRivetTool.rivet)
    cmds.menuItem(parent=riggingTools, label="Invert Shape", c=ChadV_InvertShape.shapeinvert)
    cmds.menuItem(parent=riggingTools, label="Select Skinned Joints", c=SelectSkinnedJoints.selectSkinnedJnts)
    cmds.menuItem(parent=riggingTools, divider=True)
    cmds.menuItem(parent=riggingTools, label="Auto Bendy Arms", c=AutoBendyArms.autoBendyArms)
    cmds.menuItem(parent=riggingTools, label="Facial Tool", c=ForestFacialRig.facialCrvWin)
    cmds.menuItem(parent=riggingTools, divider=True)
    cmds.menuItem(parent=riggingTools, label="Show Joint Orient", c=showJointOrients.showJntOrient)
    cmds.menuItem(parent=riggingTools, label="Advance Naming", c=AdvanceNaming.advanceNamingWin)
    animTools = cmds.menuItem(parent =customMenu, label= "Forest Animation Tools", subMenu=True)
    #cmds.menuItem(parent=animTools, label="Character Monitor", c=ChTab.chTab)
    cmds.menuItem(parent=animTools, label="Move All Animation", c=MoveAllAnim.moveAnimWindow)
    #cmds.menuItem(parent=animTools, label="Resetter", c=Resetter.GUI)
    sbsTools= cmds.menuItem(parent =customMenu, label= "Forest Substance Tools", subMenu=True)
    cmds.menuItem(parent=sbsTools, label="Forest Substance To Redshift", c=SubstanceToRS.createUI)
    cmds.menuItem(parent=sbsTools, label="Forest Export To Substance", c=forestExportToSubstance.createUI)
    stbtsTools= cmds.menuItem(parent =customMenu, label= "Storybots Tools", subMenu=True)
    cmds.menuItem(parent=stbtsTools, label="Storybots Asset Manager", c=STBTS_AssetManager.forestAssetManager)
    cmds.menuItem(parent=stbtsTools, label="Storybots Shot Manager", c=STBTS_ShotManager.forestShotManager)
    cmds.menuItem(parent=stbtsTools, label="Storybots Color Set", c=STBTS_ColorSetCreate.createUI)
    cmds.menuItem(parent=stbtsTools, label="Storybots Tools", c=STBTS_Scripts.STBTWindow)
    cmds.menuItem(parent=stbtsTools, label="Point Wrap", c=PointWrap.pointWrap)
    #Lucie = cmds.menuItem(parent =customMenu, label= "Lucie Tools", subMenu=True)
    #cmds.menuItem(parent=Lucie, label="Rainbooooow", c=lucieFeature.lucieFeature)
    #cmds.menuItem(parent=Lucie, label="Forest Color", c=changeMayaColor.changeMayaColor)


forestMayaMenu()