import maya.cmds as cmds
import maya.mel as mel

def fRenameTool(*args):

    path = cmds.internalVar(upd=True)+ "ForestTools.txt"
    f=open(path, 'r')
    toolDir= f.readline()
    path = toolDir+"/General/Scripts"
    source='source "'+path+'/rename_tool.mel"'
    mel.eval(source)
    mel.eval('Quick_rename_tool()')