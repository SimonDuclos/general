import maya.cmds as cmds
import maya.mel as mel

def moveToOrig(*args):
    # Gets the selected object
    selection = cmds.ls(sl=True)
    # Gets the bounding box of the object
    bbox = cmds.exactWorldBoundingBox(selection[0])
    # Gets the middle bottom part of the bounding box
    bottom = [(bbox[0]+bbox[3])/2, bbox[1], (bbox[2]+bbox[5])/2]
    # Sets the pivot in that point
    cmds.xform(selection, piv=bottom, worldSpace=True)
    # Moves the mesh to the world origin
    cmds.move(0, 0, 0, selection[0], rpr=True)

def orig(sel):
    
    if "_" in sel:
        name_split = sel.split("_")
        name_split[-1] = "GRP"
        group_name = "_".join(name_split)
    else:
        group_name = "{}_GRP".format(sel)
    trans = cmds.xform(sel, q=True, ws=True, t=True)
    rot = cmds.xform(sel, q=True, ws=True, ro=True)
    orig = cmds.group(em=True, n=group_name)
    cmds.xform(orig, t=trans, ws=True)
    cmds.xform(orig, ro=rot, ws=True)
    par = cmds.listRelatives(sel, p=True, f=True)
    if par:
        cmds.parent(orig, par)
    cmds.parent(sel, orig)
    return orig

def RigBase(*args):
    sel= cmds.ls(sl=True)
    if cmds.checkBox('UseCurrentNaming', q=True, v=True):
        name='name'
    else:
        name=cmds.textField('RigName', q=True, tx=True)
        listnew=[]
        for i in sel:
            if cmds.listRelatives(i, s=True):
                new = cmds.rename(i, i+'_'+name+'_MOD')
            else :
                new = cmds.rename(i, i+'_'+name+'_GRP')
            listnew.append(new)
        sel=listnew
    if cmds.listRelatives(sel, s=True):
        geos=sel
        listnew=[]
        for i in sel:
            new=orig(i)
            listnew.append(new)
        sel=listnew
    else:
        geos= cmds.listRelatives(sel)
    if cmds.checkBox('Center', q=True, v=True):
        moveToOrig(sel)
    name=sel[0]
    sel=sel[0]
    if ':' in sel:
        name=sel.split(':')[1]
    namesplit = name.split('_')
    namelength= len(namesplit)
    name = name.replace('_'+namesplit[namelength-1],'')
    if cmds.checkBox('UseCurrentNaming', q=True, v=True):
        name=name
    else:
        name = cmds.textField('RigName', q=True, tx=True)


    #Hierarchy
    groups=['_GEO', '_RIG', '_JOINT', '_NODES']
    for g in groups:
        g=cmds.group(n=name+g+'_GRP', em=True)
        cmds.parent(g, sel)
    cmds.parent(geos, name+'_GEO_GRP')

    #Controls
    minX = cmds.getAttr(sel + '.boundingBoxMinX')
    maxX = cmds.getAttr(sel + '.boundingBoxMaxX')
    X= maxX-minX

    minZ = cmds.getAttr(sel + '.boundingBoxMinZ')
    maxZ = cmds.getAttr(sel + '.boundingBoxMaxZ')
    Z=maxZ-minZ

    minY = cmds.getAttr(sel + '.boundingBoxMinY')
    maxY = cmds.getAttr(sel + '.boundingBoxMaxY')
    Y=maxY-minY
    
    t=X**2+Z**2
    size=mel.eval('sqrt({})'.format(t))/2


    midY=minY+(maxY-minY)/2
    midX=minX+(maxX-minX)/2
    midZ=minZ+(maxZ-minZ)/2


    ctlmain, creation_node = cmds.circle(n=name + '_Main_CTL', r=size*1.25, nr=(0, 1, 0))
    maingrp= orig(ctlmain)
    cmds.setAttr("{}.overrideEnabled".format(ctlmain), 1)
    cmds.setAttr("{}.overrideColor".format(ctlmain), 22)
    ctloffset, creation_node =cmds.circle(n=name + '_Offset_CTL', r=size, nr=(0, 1, 0))
    offsetgrp= orig(ctloffset)
    cmds.setAttr("{}.overrideEnabled".format(ctloffset), 1)
    cmds.setAttr("{}.overrideColor".format(ctloffset), 17)
    cmds.parent(offsetgrp, ctlmain)
    cmds.parent(maingrp, name+'_RIG_GRP')

    jnt = cmds.createNode('joint', n=name + '_JNT')
    cmds.parentConstraint(ctloffset, jnt)
    cmds.scaleConstraint(ctloffset, jnt)
    cmds.parent(jnt, name+'_JOINT_GRP')

    #Main Attributes
    cmds.addAttr(ctlmain, ln="globaldivider_01", nn=' ', at="enum",  en="---------------:", k=True)
    cmds.addAttr(ctlmain, ln='Joint_Visibility', at='bool', k=True)
    cmds.setAttr(ctlmain+'.Joint_Visibility', 1)
    cmds.addAttr(ctlmain, ln='Rig_Visibility', at='bool', k=True)
    cmds.setAttr(ctlmain+'.Rig_Visibility', 1)
    cmds.addAttr(ctlmain, ln="globaldivider_02", nn=' ', at="enum",  en="---------------:", k=True)
    cmds.addAttr(ctlmain, ln='Model_Unselectable', at='bool', k=True)
    cmds.addAttr(ctlmain, ln="globaldivider_03", nn=' ', at="enum",  en="---------------:", k=True)

    cmds.connectAttr(ctlmain+'.Joint_Visibility', name+'_JOINT_GRP.visibility')
    cmds.connectAttr(ctlmain+'.Rig_Visibility', offsetgrp+'.visibility')
    
    modeldisp= cmds.createNode( 'multiplyDivide', n=name+'_ModelDisplay')
    cmds.setAttr(modeldisp+'.input1X', 2)
    cmds.connectAttr(ctlmain+'.Model_Unselectable', modeldisp+'.input2X')
 
    for geo in geos:
        cmds.setAttr(geo+'.overrideEnabled', 1)
        cmds.connectAttr(modeldisp+'.outputX', geo+'.overrideDisplayType')

    #Skin meshes    
    if cmds.checkBox('BindSkin', q=True, v=True):
        cmds.skinCluster( jnt, geos)

def closeWindow(*args):
    
    # Closes UI

    cmds.deleteUI('ForestBaseRig')

def createUI(*args):
    
    windowprop = 'ForestBaseRig'
    
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window(windowprop, t='Forest Base Rig',w=300, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('Layout')
    cmds.rowColumnLayout( numberOfColumns=1, columnWidth=[ (1,300) ], parent='Layout')
    cmds.text('Base Rig', al='center', font="boldLabelFont", h=40, w=100)
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 150), (2, 150)], parent='Layout')
    cmds.text('  Naming:', al='left', font="obliqueLabelFont")
    cmds.textField('RigName', text='RigName', width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none') 
    cmds.separator(h=10, style='none')
    cmds.checkBox('UseCurrentNaming', l='Use Current Naming') 
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')
    cmds.text('  Options:', al='left', font="obliqueLabelFont")
    cmds.checkBox('Center', v=True)
    cmds.separator(h=10, style='none')
    cmds.checkBox('BindSkin', l='Bind Skin', v=True)
    cmds.separator(h=5, style='none')
    cmds.separator(h=5, style='none')

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 300)], parent='Layout')
    cmds.button(label = 'Create Rig', h=30, backgroundColor= [0.1,.4,.25], command = RigBase)
    cmds.separator(h=10, style='none')
    cmds.button(label = 'Close Window', h=30, backgroundColor= [0.4,.1,.25], command = closeWindow)
    cmds.showWindow(windowprop)
    cmds.window(window, edit=True, w=305, h=220) 
