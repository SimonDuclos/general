import maya.cmds as cmds
import pymel.core as pm
from os import walk
import json

def createRedshiftMat(nameShader, matID):
    
    #create redshift material node
    redshiftMaterial = cmds.shadingNode('RedshiftMaterial', asShader = True, name=nameShader)
    
    #set the metallic attributes for substance
    cmds.setAttr(redshiftMaterial + '.refl_brdf', 1)
    cmds.setAttr(redshiftMaterial + '.refl_fresnel_mode', 2)
    
    materialSG = cmds.sets(name=nameShader + 'SG', renderable = True, noSurfaceShader = True)
    materialSGName = cmds.ls(materialSG)[0]

    #set matID
    cmds.setAttr(materialSGName + '.rsMaterialId', matID)
    cmds.connectAttr(redshiftMaterial + '.outColor', materialSG + '.surfaceShader')
    
    matNode = [redshiftMaterial, materialSG]
    
    return matNode 


def createFileNode():
    
    #create place texture node and file node
    placeTextNode = cmds.shadingNode('place2dTexture', asUtility=True)
    fileNode = cmds.shadingNode('file', asTexture=True, isColorManaged=True)

    #create connect the relevant attributes
    cmds.connectAttr(placeTextNode + '.coverage', fileNode + '.coverage')
    cmds.connectAttr(placeTextNode + '.translateFrame', fileNode + '.translateFrame')
    cmds.connectAttr(placeTextNode + '.rotateFrame', fileNode + '.rotateFrame')
    cmds.connectAttr(placeTextNode + '.mirrorU', fileNode + '.mirrorU')
    cmds.connectAttr(placeTextNode + '.mirrorV', fileNode + '.mirrorV')
    cmds.connectAttr(placeTextNode + '.stagger', fileNode + '.stagger')
    cmds.connectAttr(placeTextNode + '.wrapU', fileNode + '.wrapU')
    cmds.connectAttr(placeTextNode + '.wrapV', fileNode + '.wrapV')
    cmds.connectAttr(placeTextNode + '.repeatUV', fileNode + '.repeatUV')
    cmds.connectAttr(placeTextNode + '.offset', fileNode + '.offset')
    cmds.connectAttr(placeTextNode + '.rotateUV', fileNode + '.rotateUV')
    cmds.connectAttr(placeTextNode + '.noiseUV', fileNode + '.noiseUV')
    cmds.connectAttr(placeTextNode + '.vertexUvOne', fileNode + '.vertexUvOne')
    cmds.connectAttr(placeTextNode + '.vertexUvTwo', fileNode + '.vertexUvTwo')
    cmds.connectAttr(placeTextNode + '.vertexUvThree', fileNode + '.vertexUvThree')
    cmds.connectAttr(placeTextNode + '.vertexCameraOne', fileNode + '.vertexCameraOne')
    cmds.connectAttr(placeTextNode + '.outUV', fileNode + '.uv')
    cmds.connectAttr(placeTextNode + '.outUvFilterSize', fileNode + '.uvFilterSize')
    
    #set the path for the file node
    
    fileNode = [placeTextNode, fileNode]
    
    return fileNode


def createBumpNode():
    
    bumpNode = cmds.shadingNode('RedshiftBumpMap', asTexture = True)
    
    #set the tangent space normals
    cmds.setAttr(bumpNode + '.inputType', 1)
    cmds.setAttr(bumpNode + '.scale', 1)
    
    return bumpNode


def updateButtonShd():
    '''
    Reload the UI.
    '''

    closeWindow()
    createUI()


def createShader(nameShader, pathTex, matID, checkUDIM):
    '''
    Calls the creation of material, SG and file shader nodes, and then proceeds to connect them. Before doing so check that the Redshift Material and SG name do not exist already.
    :param nameShader: Name of the redshift material node.
    :param pathTex: Path to the textures to be loaded.
    :param matID: The material ID of the object to create.
    :param checkUDIM: Whether the folder contains textures for multiple UDIM tiles.
    :return: Return if the shader name is already taken (before throwing a Maya error)
    '''

    allShaders = []
    allSG = []

    everything = cmds.ls()

    for thing in everything:

        if pm.objectType(thing) == 'RedshiftMaterial':
            allShaders.append(thing)
        if pm.objectType(thing) == 'shadingEngine':
            allSG.append(thing)

    if nameShader in allShaders:
        cmds.error(nameShader + ' already exists in this file.')
    if nameShader + 'SG' in allSG:
        cmds.error(nameShader + 'SG' + ' already exists in this file.')

    # Connects and sets everything in the shader
    
    matNode = createRedshiftMat(nameShader, matID)

    # Areas to connect in the redshift shader
    whereConnect = ['.diffuse_color', '.refl_roughness', '.bump_input', '.refl_metalness']
    
    # SET NAMING CONVENTIONS HERE FOR MAPS
    whereConnectDict = { whereConnect[0] : 'Color', whereConnect[1] : 'Roughness', whereConnect[2] : 'Normal', whereConnect[3] : 'Metalness' }
    
    filesInDir = getFiles(pathTex)
    
    
    for i in range(len(whereConnect)):
        
        fileNode = createFileNode()
        
        curFileName = getFileName(filesInDir, whereConnectDict[whereConnect[i]])
        
        # Set the file name
        cmds.setAttr(fileNode[1] + '.fileTextureName', pathTex + '/' + curFileName, type = 'string')
        
        # Set the UDIM setting too if neccessary
        if checkUDIM == True:
            cmds.setAttr(fileNode[1] + '.uvTilingMode', 3)
        
        if whereConnect[i] == '.refl_metalness' or whereConnect[i] == '.refl_roughness':
            cmds.connectAttr(fileNode[1] + '.outAlpha', matNode[0] + whereConnect[i])
            
        elif whereConnect[i] == '.bump_input':
            bumpNode = createBumpNode()
            cmds.connectAttr(fileNode[1] + '.outColor', bumpNode + '.input')
            cmds.connectAttr(bumpNode + '.out', matNode[0] + whereConnect[i])
            
        else:
            cmds.connectAttr(fileNode[1] + '.outColor', matNode[0] + whereConnect[i])

def closeWindow(*pArgs):
    
    # Closes UI
    
    cmds.deleteUI('Substance_to_Redshift_Shader_Generator')

def setupCreateShader(nameShader, pathTex, matID, checkUDIM):

    # Updates the path input appropriately based on windows machines
    newPathTex = []
    
    for letter in pathTex:
        
        if letter == '\\':
            letter = '/'
        
        newPathTex.append(letter)
    
    pathTex = ''.join(newPathTex)

    createShader(nameShader, pathTex, matID, checkUDIM)
        
def browsePath(*args):
    tx = cmds.fileDialog2(dialogStyle=2, fileMode =3)[0]

    cmds.textField('Path' , e=True, tx=tx)

def createUI(*args):

    projectFunctions = True
    

    # Create the GUI window and all the input options/buttons
    if cmds.window('Substance to Redshift', exists=True):
        cmds.deleteUI('Substance to Redshift')
    cmds.window('Substance to Redshift', backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('RedshiftShaderGeneratorMainLayout', adjustableColumn=False)
    cmds.rowColumnLayout( numberOfColumns=1, columnWidth=[ (1,400) ], columnOffset=[ (1,'right', 1) ], parent='RedshiftShaderGeneratorMainLayout')
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 200), (2, 200)], parent='RedshiftShaderGeneratorMainLayout')
    # Name of the shader
    cmds.text('  Naming:', al='left', font="obliqueLabelFont")
    nameShader = cmds.textField(text='Default_Default_RS', width=200, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')    
    # The path to the textures folder (all textures should be located in here)
    cmds.text('  Path of Textures Folder:', al='left', font="obliqueLabelFont")
    pathTex = cmds.textField('Path' , tx='...', backgroundColor= [0.05,.15,.15])
    cmds.separator(h=10, style='none')
    cmds.button(label="Browse Path", width=200, backgroundColor= [0.1,.4,.25], command=browsePath)
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    
    # What material ID to set on the shader
    cmds.text('  Material ID:', al='left', font="obliqueLabelFont")
    matID = cmds.intField(value=1, minValue=1, maxValue=10, step=1, width=200, backgroundColor= [0.05,.15,.15] )
    
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    # Whether the textures have multiple tiles
    cmds.text('  UDIMs?', al='left', font="obliqueLabelFont")
    checkUDIM = cmds.checkBox(label='')
    
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 400)], columnOffset=[(1, 'right', 1)], parent='RedshiftShaderGeneratorMainLayout')
    # Final buttons
    cmds.button(label="Create Shader", width=400, backgroundColor= [0.1,.4,.25], command=lambda *args: setupCreateShader(cmds.textField(nameShader, query=True, text=True), cmds.textField(pathTex, query=True, text=True), cmds.intField(matID, query=True, value=True), cmds.checkBox(checkUDIM, query=True, value=True)))
    cmds.separator(h=10, style='none')

    cmds.button(label = 'Close Window ', width =400, backgroundColor= [0.4,.1,.25],command = closeWindow)

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 400)], columnOffset=[(1, 'right', 1)], parent='RedshiftShaderGeneratorMainLayout')

    
    cmds.showWindow()
