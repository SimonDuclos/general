import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import users

def exitWindow(*args):
    cmds.deleteUI("FAssetCreate")

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def MakeFolder(name, path, *args):
    path=path+'/'+name
    make_dir(path)
    make_dir(path+'/_OLD')
    make_dir(path+'/_Useful')
    make_dir(path+'/_PUB')

def MakeCHAssetDir(filepath, ref, parent, *args):
    jobs=['01_MOD', '02_SHD', '03_RIG', '04_CFX', '05_ASSEMBLY']
    newAssetName=cmds.textField('aName', q=1, tx=1)
    newAssetPath=filepath+'/'+newAssetName
    make_dir(newAssetPath)
    for i in jobs:
        MakeFolder(i, newAssetPath)
    assetsDir=cmds.textField('ADir', q=True, tx=True)
    assetMenu(assetsDir, ref, parent)
    exitWindow()

def MakeAssetDir(filepath, ref, parent, *args):
    jobs=['01_MOD', '02_SHD', '03_RIG']
    newAssetName=cmds.textField('aName', q=1, tx=1)
    newAssetPath=filepath+'/'+newAssetName
    make_dir(newAssetPath)
    for i in jobs:
        MakeFolder(i, newAssetPath)
    assetsDir=cmds.textField('ADir', q=True, tx=True)
    assetMenu(assetsDir, ref, parent)
    exitWindow()

def delUI(name, *args):
    if cmds.optionMenu(name, exists=True):
        cmds.deleteUI(name)

def AssetCreateWindow(type, filepath, ref, parent, *args):
    if cmds.window("FAssetCreate", exists=True):
        cmds.deleteUI("FAssetCreate")
    window = cmds.window("FAssetCreate", title= "Create Asset", w=200, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Asset Name')
    cmds.textField('aName', tx='')
    
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    
    cmds.setParent("..")

    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=2, columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])
    if type=='01_Characters':
        cmds.button('Create', backgroundColor= [0.1,.4,.25], command=partial(MakeCHAssetDir, filepath, ref, parent)) 
    else:
        cmds.button('Create', backgroundColor= [0.1,.4,.25], command=partial(MakeAssetDir, filepath, ref, parent)) 
    cmds.button('Exit', backgroundColor= [0.4,.1,.25], command=exitWindow)

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=185)

def forestSave(filepath, project, asset, job, saveas, *args):
    username= cmds.optionMenu('initials', q=True, v=True)
    pname= project
    vrs=1
    filename= pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+username
            
    if not saveas:
        cmds.file(new=True, force=True)    
    cmds.file( rename=filepath+'/'+filename)
    cmds.file( save=True, type='mayaAscii' )
    cmds.deleteUI('FSaveWindow')



def CreateVRSWindow(filepath, *args):

    project=cmds.text('project', q=True, l=True)
    asset=cmds.optionMenu('AssetMenu', q=True, v=True)
    job=cmds.optionMenu('JobMenu', q=True, v=True).split('_')[1]
    name=''
    
    if cmds.window("FSaveWindow", exists=True):
        cmds.deleteUI("FSaveWindow")
    window = cmds.window("FSaveWindow", title= "Forest Saving Tool", w=300, h=150, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Project Name')
    cmds.textField('pname', tx=project)

    cmds.text('Asset Name')
    cmds.textField('asset', tx=asset) 
        
    cmds.text('Initials')
    cmds.optionMenu('initials')
    cmds.menuItem(name)
    for user in (users.users):
        if user[1] != name:
            cmds.menuItem(label = user[1])
    
    
    cmds.setParent("..")
    cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
        
    cmds.separator()
    
    cmds.text("  ")

    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=1, columnWidth=(1,300))

    cmds.button('Create', backgroundColor= [0.1,.4,.25], command=partial(forestSave, filepath, project, asset, job, False))    
    cmds.button('SaveAs', l='Save As', backgroundColor= [0.1,.4,.25], command=partial(forestSave, filepath, project, asset, job, True))  
    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=150) 


def clearMenu(menu, *args):
    menuItems = cmds.optionMenu(menu, q=True, itemListLong=True) # itemListLong returns the children
    if menuItems:
        cmds.deleteUI(menuItems)

def editProject(*args):
    pdir= cmds.workspace(q=True, rd=True)
    ppath = cmds.fileDialog2(fm=3, dir=pdir) 
    pname =pdir.split('/')[-2]
    cmds.text('project', e=True, l= pname)
    projectdirs = [f for f in os.listdir(ppath) if os.path.isdir(os.path.join(ppath, f))]
    for dirs in projectdirs:
        if '3D' in dirs:
            assetsDir = ppath+dirs+'/01_Assets/'

    
    cmds.textField('ADir', e=True, tx=assetsDir)

    types = [f for f in os.listdir(assetsDir) if os.path.isdir(os.path.join(assetsDir, f))]
    clearMenu('typeMenu')
    if path.isdir(assetsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem( parent = ( 'typeMenu'), l=type)
    else :
        cmds.menuItem( parent = ( 'typeMenu'), l='Check Asset Directory')

def forestAssetManager(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    pname =pdir.split('/')[-2]
    projectdirs = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]

    if path.isdir(pdir):
        episodes = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]

    if cmds.window("AssetManagerWindow", exists=True):
        cmds.deleteUI("AssetManagerWindow")
    window = cmds.window("AssetManagerWindow", title= "Storybots Asset Manager", w=500, h=200, sizeable=True, backgroundColor= [0.1,.2,.2])

    cmds.columnLayout('TypeMenu')
    cmds.rowColumnLayout("ProjectLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=30, style='none')
    cmds.text('project', al='center', font='fixedWidthFont', l=pname)
    cmds.setParent('..')
    cmds.rowColumnLayout("editLayout", numberOfColumns=3, columnWidth=[(1,230),(2,40),(3,230)])
    cmds.separator( height=20, style='none')
    cmds.button('Edit', backgroundColor= [0.1,.4,.25], h=12, command=editProject) 
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.separator( height=20, style='none')
    cmds.setParent('..')

    cmds.rowColumnLayout("EpisodeLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.rowColumnLayout('EpisodeColumnLayout', nc=2, cw=[(1,150),(2,330)], cs=(2,10))
    cmds.text('Episode', backgroundColor= [0.1,.4,.25])

    cmds.optionMenu('EpisodeMenu',backgroundColor= [0.1,.4,.4], cc= partial(typeMenu, "EpisodeLayout", pdir))
    for episode in episodes:
        if episode!='.DS_Store':
            cmds.menuItem(episode) 
    
    cmds.setParent('..')
    cmds.rowColumnLayout("separatorLayout", numberOfColumns=1)

    cmds.separator( height=20, style='none')
 
    typeMenu("EpisodeLayout", pdir) 

    cmds.showWindow(window)
    #cmds.window(window, edit=True, w=200, h=170) 

def typeMenu(parent, pdir, *args):
    
    if cmds.optionMenu('typeMenu', exists=True):
        cmds.deleteUI('typeMenu', 'typeLayout', 'AssetDirLayout', 'NamesLayout', 'tabLayout')
    if cmds.optionMenu('AssetMenu', exists=True):
        cmds.deleteUI('AssetMenu', 'assetLayout', 'assetSep')
    if cmds.optionMenu('JobMenu', exists=True):
        cmds.deleteUI('JobMenu','jobLayout','jobSep')
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.setParent('..')
    episodeName = cmds.optionMenu('EpisodeMenu', q=True, v=True)
    edir = pdir+'/'+episodeName
    episodedirs = [f for f in os.listdir(edir) if os.path.isdir(os.path.join(edir, f))]
    for dirs in episodedirs:
        if '3D' in dirs:
            assetsDir = edir+'/'+dirs+'/01_Assets/'

            if path.isdir(assetsDir):
                types = [f for f in os.listdir(assetsDir) if os.path.isdir(os.path.join(assetsDir, f))]

    cmds.rowColumnLayout("AssetDirLayout", numberOfColumns=2, columnWidth=[(1,100),(2,400)])
    cmds.text('Asset Directory')
    cmds.textField('ADir', tx=assetsDir, backgroundColor= [0.05,.15,.15])
    cmds.setParent('..')


    tabs= cmds.tabLayout('tabLayout', imw=5, imh=5, w=500, h=250, hlc=[0.1,.4,.25], cr=True)
    cmds.columnLayout('                                  VRS                                  ', h=180, parent=tabs, cal='center', adj=True)
    cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=10, style='in')
    cmds.rowColumnLayout('typeLayout', nc=2, cw=[(1,150),(2,330)], cs=(2,10))
    cmds.text('Asset Type', backgroundColor= [0.1,.4,.25])

    cmds.optionMenu('typeMenu',backgroundColor= [0.1,.4,.4], cc= partial(assetMenu, assetsDir, False, "NamesLayout"))
    if path.isdir(assetsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem(type) 
    else :
        cmds.menuItem( parent = ('typeMenu'), l='Check Asset Directory')
    cmds.setParent('..')
    cmds.separator(h=15, style='in' )       
    assetMenu(assetsDir, False, "NamesLayout")


    parentr=cmds.columnLayout('                         Reference PUB                         ', cal='center', h=180, parent=tabs, adj=True)
    cmds.rowColumnLayout("RefNamesLayout", numberOfColumns=1, columnWidth=(1,500))
    cmds.separator( height=10, style='in')
    cmds.rowColumnLayout('ReftypeLayout', nc=2, cw=[(1,150),(2,330)], cs=(2,10))
    cmds.text('Asset Type', backgroundColor= [0.1,.4,.25])
    cmds.optionMenu('ReftypeMenu',backgroundColor= [0.1,.4,.4], cc= partial(assetMenu, assetsDir, True, "RefNamesLayout"))
    if path.isdir(assetsDir):
        for type in types:
            if type!='.DS_Store':
                cmds.menuItem(type) 
    else :
        cmds.menuItem( parent = ('typeMenu'), l='Check Asset Directory')
    cmds.setParent('..')
    cmds.separator(h=15, style='in' )           
    assetMenu(assetsDir, True, "RefNamesLayout")


def assetMenu(assetsDir, ref, parent, *args):
    if cmds.rowColumnLayout('AssetMenu', exists=True):
        cmds.deleteUI('AssetMenu')
    if cmds.rowColumnLayout('RefassetLayout', exists=True):
        cmds.deleteUI('RefassetLayout')
    if cmds.rowColumnLayout('assetLayout', exists=True):
        cmds.deleteUI('assetLayout')   
    if cmds.separator('assetSep', exists=True):
        cmds.deleteUI('assetSep')   
    if cmds.optionMenu('JobMenu', exists=True):
        cmds.deleteUI('JobMenu','jobLayout','jobSep')
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    if ref:
        cmds.rowColumnLayout('RefassetLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    else:
        cmds.rowColumnLayout('assetLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    
    cmds.text('assetText', l='Asset', backgroundColor= [0.1,.4,.25])
    assetType = cmds.optionMenu('typeMenu', q=True, v=True)
    assets = [f for f in os.listdir(assetsDir+assetType) if os.path.isdir(os.path.join(assetsDir+assetType, f))]
    cmds.optionMenu('AssetMenu', backgroundColor= [0.1,.4,.4], cc=partial(assetJob, assetType, assetsDir, ref, parent))
    for asset in assets:
        if asset!='.DS_Store':
            cmds.menuItem(asset) 
    cmds.button('createButton', l='Create',  backgroundColor= [0.4,.1,.25], c=partial(AssetCreateWindow, assetType, assetsDir+assetType, ref, parent))
    cmds.setParent('..')
    cmds.separator('assetSep', h=15, style='in' )



def assetJob(assetType, assetsDir, ref, parent, *args):
    if cmds.optionMenu('JobMenu', exists=True):
        cmds.deleteUI('JobMenu','jobLayout','jobSep')
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.rowColumnLayout('jobLayout', nc=2, cw=[(1,150),(2,230)], cs=(2,10), p=parent)
    cmds.text('jobText', l='Job', backgroundColor= [0.1,.4,.25])
    assetName = cmds.optionMenu('AssetMenu', q=True, v=True)
    jobs = [f for f in os.listdir(assetsDir+assetType+'/'+assetName) if os.path.isdir(os.path.join(assetsDir+assetType+'/'+assetName, f))]
    if ref:
        cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(jobPub, assetName, assetType, assetsDir, parent))
    else:
        cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(jobVRS, assetName, assetType, assetsDir, parent))
    for job in jobs:
        if job!='.DS_Store':
            cmds.menuItem(job) 
    cmds.setParent('..')
    cmds.separator('jobSep', h=15, style='in' )


def jobVRS(assetName, assetType, assetsDir, parent, *args):
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.rowColumnLayout('VRSLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    cmds.text('VRS', l='VRS', backgroundColor= [0.1,.4,.25])
    jobType = cmds.optionMenu('JobMenu', q=True, v=True)
    vrs = [f for f in os.listdir(assetsDir+assetType+'/'+assetName+'/'+jobType) if os.path.isfile(os.path.join(assetsDir+assetType+'/'+assetName+'/'+jobType, f))]
    listvrs=[]
    filepath=assetsDir+'/'+assetType+'/'+assetName+'/'+jobType+'/'
    vrssep=False
    cmds.optionMenu('FileMenu', backgroundColor= [0.1,.4,.4], cc=partial(openButton, assetName, assetType, assetsDir, jobType, parent))
    if vrs:
        for vr in vrs:
            vr=vr.split('.')[0]
            num=vr.split('_')[5]
            listvrs.append(int(num))    
        if listvrs:
            vrsmax=max(listvrs)
        
        for vr in vrs:
            if vr!='.DS_Store':
                cmds.menuItem(vr) 
                if str(vrsmax) in vr.split('_')[5]:
                    cmds.optionMenu('FileMenu', e=True, v=vr)
                    cmds.setParent('..')
                    vrssep=cmds.separator('vrsSep', h=15, style='in' )
                    openButton(assetName, assetType, assetsDir, jobType, parent)

    
    if not vrssep:
        cmds.button('createVRSButton', l='Create',  backgroundColor= [0.4,.1,.25], c=partial(CreateVRSWindow, filepath))
        cmds.setParent('..')
        cmds.separator('vrsSep', h=15, style='in' )

def jobPub(assetName, assetType, assetsDir, parent, *args):
    if cmds.optionMenu('FileMenu', exists=True):
        cmds.deleteUI('FileMenu','VRSLayout','vrsSep')
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    cmds.rowColumnLayout('VRSLayout', nc=3, cw=[(1,150),(2,230), (3,90)], cs=[(2,10),(3,10)], p=parent)
    cmds.text('VRS', l='PUB', backgroundColor= [0.1,.4,.25])
    jobType = cmds.optionMenu('JobMenu', q=True, v=True)
    vrs = [f for f in os.listdir(assetsDir+assetType+'/'+assetName+'/'+jobType+'/_PUB') if os.path.isfile(os.path.join(assetsDir+assetType+'/'+assetName+'/'+jobType+'/_PUB', f))]
    listvrs=[]
    vrssep=False
    cmds.optionMenu('FileMenu', backgroundColor= [0.1,.4,.4], cc=partial(refButton, assetName, assetType, assetsDir, jobType, parent))
    if vrs:
        for vr in vrs:
            vr=vr.split('.')[0]
            num=vr.split('_')[6]
            listvrs.append(int(num))    
        if listvrs:
            vrsmax=max(listvrs)
        
        for vr in vrs:
            if vr!='.DS_Store':
                cmds.menuItem(vr) 
                if str(vrsmax) in vr.split('_')[6]:
                    cmds.optionMenu('FileMenu', e=True, v=vr)
                    cmds.setParent('..')
                    vrssep=cmds.separator('vrsSep', h=15, style='in' )
                    cmds.rowColumnLayout('vrsbuttonLayout', nc=2, cw=[(1,250),(2,250)], p=parent)
                    refButton(assetName, assetType, assetsDir, jobType, parent)
                    replaceRefButton(assetName, assetType, assetsDir, jobType, parent)

    
    if not vrssep:
        cmds.setParent('..')
        cmds.separator('vrsSep', h=15, style='in' )

def openButton(assetName, assetType, assetsDir, jobType, parent, *args):
    if cmds.button('openButton', exists=True):
        cmds.deleteUI('openButton')
    filename = cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = assetsDir+assetType+'/'+assetName+'/'+jobType+'/'+filename
    cmds.button('openButton',  l='Open', p=parent, w=500, h=35,  backgroundColor= [0.4,.1,.25], c=partial(openScene, filepath, assetName, assetType, assetsDir, jobType))


def openScene(filepath, *args):
    cmds.file(filepath, o=True, force=True)

def refButton(assetName, assetType, assetsDir, jobType, parent, *args):
    if cmds.button('refButton', exists=True):
        cmds.deleteUI('refButton')
    pub= cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = assetsDir+assetType+'/'+assetName+'/'+jobType+ '/_PUB/'+pub
    cmds.button('refButton', l='Reference', h=35,  p='vrsbuttonLayout', backgroundColor= [0.4,.1,.25], c=partial(importRef, filepath, assetName, assetType, assetsDir, jobType, pub))

def replaceRefButton(assetName, assetType, assetsDir, jobType, parent, *args):
    if cmds.button('replacerefButton', exists=True):
        cmds.deleteUI('replacerefButton')
    pub= cmds.optionMenu('FileMenu', q=True, v=True)
    filepath = assetsDir+assetType+'/'+assetName+'/'+jobType+ '/_PUB/'+pub
    cmds.button('replacerefButton', l='Replace Reference', p='vrsbuttonLayout', h=35,  backgroundColor= [0.4,.1,.25], c=partial(replaceRef, filepath, assetName, assetType, assetsDir, jobType, pub))

def importRef(filepath, assetName, assetType, assetsDir, jobType, pub, *args):
    if cmds.file(filepath, q=True, ex=True):
        cmds.file(filepath, r=True, ns=assetName+'_'+jobType.split('_')[1])

def replaceRef(filepath, assetName, assetType, assetsDir, jobType, pub, *args):
    refnode=cmds.referenceQuery(cmds.ls(sl=True)[0], rfn=True)
    if cmds.file(filepath, q=True, ex=True):
        cmds.file(filepath, lr=refnode)