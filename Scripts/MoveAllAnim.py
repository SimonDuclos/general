import maya.cmds as cmds
from functools import partial

def moveAnimWindow(*args):
	if cmds.window("mAnimWindow", exists=True):
		    cmds.deleteUI("mAnimWindow")
	window = cmds.window("mAnimWindow", title= "Move Anim Tool", w=200, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])
	cmds.rowColumnLayout('buttons', numberOfColumns=1, columnWidth=[(1, 180)], columnAlign=(1,'center'), columnSpacing=(1,10))
	cmds.separator(h=10, style='none')
	cmds.text('Frames to shift the animation')
	cmds.intField('shFrame', w=50, v=0)
	cmds.separator(h=10, style='none')
	cmds.checkBox('sdKeys', label='Include Set Driven Keys')
	cmds.separator(h=10, style='none')
	cmds.button('Shift All Animation', h=20, backgroundColor= [0.4,.1,.25], c=partial(moveAllAnim, cmds.intField('shFrame', q=True, v=True), cmds.checkBox('sdKeys', q=True, v=True)))
	cmds.separator(h=30)
	cmds.button('Delete All Animation', backgroundColor= [0.8,.1,.2], command=deleteAllAnim )
	cmds.showWindow(window)

def moveAllAnim(frame, keys, *args):	
	if cmds.intField('shFrame', q=True, v=True)!=0:
		stime=cmds.intField('shFrame', q=True, v=True)
	else:
		stime = cmds.currentTime(q=True)

	anim_curves = cmds.ls(type=['animCurveTA', 'animCurveTL', 'animCurveTT', 'animCurveTU'])  

	for curve in anim_curves:  
	    cmds.keyframe(curve, edit=True, relative=True, timeChange=stime)
	
	if keys:	
		anim_curves = cmds.ls(type=["animCurveUL","animCurveUU","animCurveUA","animCurveUT"])  
		for curve in anim_curves:  
		    cmds.keyframe(curve, edit=True, relative=True, timeChange=stime)

def deleteAllAnim(*args):
	anim_curves = cmds.ls(type=['animCurveTA', 'animCurveTL', 'animCurveTT', 'animCurveTU'])  
	for curve in anim_curves :
		cmds.delete(curve)





# remplacer timeChange par valueChange pour changer les valeurs de cles