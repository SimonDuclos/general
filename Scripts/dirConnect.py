import maya.cmds as cmds

def dirConnect(*args):
	
	driver = cmds.ls(sl=True)[0]
	driven = cmds.ls(sl=True)[1]
	for tr in ['translate', 'rotate', 'scale']:
		for ax in 'XYZ':
			current_tr = tr + ax
			cmds.connectAttr('{}.{}'.format(driver, current_tr), '{}.{}'.format(driven, current_tr))