import maya.cmds as cmds

def snapt(ls):
	tosnap= ls[0]
	snapto=ls[1]
	translate = cmds.xform(snapto, t=True, q=True, ws=True)
	cmds.xform(tosnap, t=translate, ws=True)