import maya.cmds as cmds

suff = ['_M', '_L', '_R']
cstr = ['parentConstraint', 'pointConstraint', 'orientConstraint', 'poleVectorConstraint', 'scaleConstraint', 'aimConstraint']
ik =['ikEffector','ikHandle']
crvNames=[]
grpNames=[]
cstrNames=[]
ikNames=[]
jntNames=[]
locNames=[]

later=['Main', 'FitSkeleton']
		
def advanceNaming(*args):
	asset=cmds.textField('aname', q=True, tx=True)

	locators = cmds.ls(type='locator') or []
	loc_parents = cmds.listRelatives(*locators, p=True) or []
	loc_parents.sort(reverse=True)
	for lp in loc_parents:
		nlp = cmds.rename(lp, lp+'_LOC')
		locNames.append(lp+'_LOC')

	cmds.select('Main', hi=True)
	names = cmds.ls(sl=True)
	for i in later:
		names.remove(i)
		names.remove(i+'Shape') 

	for name in names : 
		if cmds.objectType( name, isType='nurbsCurve'):
			trans= name[:-5]
			if trans[-2:]:
				crvNames.append(name)
			 
		if cmds.objectType( name, isType='joint'):
			jntNames.append(name)


			
		for i in cstr:    
			if cmds.objectType( name, isType=i):
				cstrNames.append(name)

				
		for i in ik:
			if cmds.objectType( name, isType=i):
				ikNames.append(name)

				
	locNames.append(crvNames)
	locNames.append(jntNames)
	locNames.append(cstrNames)
	locNames.append(ikNames)


	for name in names :         
		if name not in locNames:
			grpNames.append(name)       


	for tran in crvNames:
		if cmds.objExists(tran):
			ctlTrans = cmds.listRelatives(tran, p=True)
			cmds.rename(ctlTrans[0], ctlTrans[0]+'_CTL')
			


	for cstrName in cstrNames:
		cmds.rename(cstrName, cstrName+'_CSTR') 
		
	for ikName in ikNames:
		cmds.rename(ikName, ikName+'_IK') 

	for jntName in jntNames:
		cmds.rename(jntName, jntName+'_JNT') 
		
	for grpName in grpNames:
		if cmds.objExists(grpName):
			cmds.rename(grpName, grpName+'_GRP')



	cmds.select('Main', hi=True)
	names = cmds.ls(sl=True)
	for name in names:
		if name[:2] not in 'R_':
			if 'Shape' not in name:
				cmds.rename(name, 'R_'+asset+'_'+name)
	for i in later:
		cmds.rename('R_'+asset+'_'+i, 'R_'+asset+'_'+i+'_CTL')


def advanceNamingWin(*args):

	if cmds.window("advanceNamingWin", exists=True):
		cmds.deleteUI("advanceNamingWin")
	window = cmds.window("advanceNamingWin", title= "Advance Naming Tool", w=300, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])	

	cmds.columnLayout('buttons')
	cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

	cmds.text("  ")
	cmds.text("  ")	

	cmds.textField('aname', tx='Asset Name')

	cmds.button('Rename', backgroundColor= [0.4,.1,.25], command=advanceNaming)

	cmds.showWindow(window)
	cmds.window(window, edit=True, w=300, h=50)
