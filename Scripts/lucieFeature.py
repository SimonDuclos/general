import maya.cmds as cmds

def lucieFeature(*args):

	cmds.colorEditor()
	targetWindows = ['MayaWindow']
	if cmds.colorEditor(q=1, result=1): 
		rgbColor = cmds.colorEditor(q=1, rgb=1)
		uis = cmds.lsUI(windows=1)
		for ui in uis:
			if any([t in ui for t in targetWindows]):
				cmds.window(ui, e=1, bgc=rgbColor)