import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial

def checkAttribute(objectName, attrName, *args):
    if attrName:
        fullName = objectName+'.'+attrName
    if cmds.objExists(fullName):
        return True
    else :
        return False

def characterID(mesh, *args):
    cmds.addAttr (mesh, ln="chID" , nn='Character_ID', at='long', k=True)

def setCharacterID(*args):
    sel = cmds.ls(sl=True)
    for i in sel:
        cmds.select(i, add=True, hi=True)
    selection = cmds.ls(sl=True)
    v=1
    if cmds.optionMenu('CharacterType', q=True, v=True)=='Boop':
        v=2
    if cmds.optionMenu('CharacterType', q=True, v=True)=='Bing':
        v=3
    if cmds.optionMenu('CharacterType', q=True, v=True)=='Bang':
        v=4
    if cmds.optionMenu('CharacterType', q=True, v=True)=='Bo':
        v=5

    for i in selection:
        if '_MOD' in i:
            if not checkAttribute(i, 'chID'):
                characterID(i)
            if 'Body' or 'Head' or 'Eyebrows' or 'Eyelid' in i:
                cmds.setAttr(i+'.chID', v)
            if 'Arms' in i:
                cmds.setAttr(i+'.chID', 6)
            if 'Legs' in i:
                cmds.setAttr(i+'.chID', 6)
            if 'Wheels' in i:
                cmds.setAttr(i+'.chID', 7)
            if 'Hands' in i:
                cmds.setAttr(i+'.chID', 8)
            if 'Eyeball' in i:
                cmds.setAttr(i+'.chID', 9)
            if 'EyeSpec' in i:
                cmds.setAttr(i+'.chID', 10)
            if 'Eyespec' in i:
                cmds.setAttr(i+'.chID', 10)
            if 'Iris' in i:
                cmds.setAttr(i+'.chID', 11)
    cmds.select(sel)



def Colors(*args):
    sel = cmds.ls(sl=True)
    for s in sel:
        cmds.addAttr (s, ln="colorID" , nn='Color_ID', at='long', k=True)
        cmds.addAttr (s, ln="color" , nn='Shading_Color', at='float3', uac= True, k=True)
        cmds.addAttr (s, ln="colorR" , at='float', p= 'color',  k=True)
        cmds.addAttr (s, ln="colorG" , at='float', p= 'color', k=True)
        cmds.addAttr (s, ln="colorB" , at='float', p= 'color', k=True)

def characterName(*args):
    sel = cmds.ls(sl=True)
    for s in sel:
        if 'ENV_' in s:
            cmds.rename(s, s.replace('ENV_',''))
        if 'PR_' not in s:  
            cmds.rename(s, s.replace('PR_',''))
        if 'CH_' in s:
            cmds.rename(s, 'CH_'+s)

def propName(*args):
    sel = cmds.ls(sl=True)
    for s in sel:
        if 'CH_' in s:
            cmds.rename(s, s.replace('CH_',''))
        if 'ENV_' in s:
            cmds.rename(s, s.replace('ENV_',''))
        if 'PR_' not in s:  
            cmds.rename(s, 'PR_'+s)

def envName(*args):
    sel = cmds.ls(sl=True)
    for s in sel:
        if 'CH_' in s:
            cmds.rename(s, s.replace('CH_',''))
        if 'PR_' in s:
            cmds.rename(s, s.replace('PR_',''))
        if 'ENV_' in s:
            cmds.rename(s, 'ENV_'+s)

def STBTWindow(*args):
    if cmds.window("STBTWndow", exists=True):
        cmds.deleteUI("STBTWndow")
    window = cmds.window("STBTWndow", title= "Storybots Tool", w=300, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])    

    tabs= cmds.tabLayout(imw=5, imh=5, w=297, hlc=(0,.15,.1))
    cmds.columnLayout('                 MOD                ', w=300, h=180, parent=tabs)

    cmds.columnLayout('CharactersLay', w=295)
    cmds.separator(style='none', h=10)
    cmds.text('Character', fn='boldLabelFont', w=280)
    cmds.separator( height=7, style='none' )
    cmds.rowColumnLayout("sepchLayout", numberOfColumns=1, columnWidth=(1,280))
    cmds.separator( height=7, style='out' )
    cmds.setParent("..")

    cmds.separator( height=12, style='single', hr=True)
    cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,280))
    cmds.optionMenu('CharacterType',backgroundColor= [0.1,.4,.4])
    cmds.menuItem(label='Beep')
    cmds.menuItem(label='Boop')
    cmds.menuItem(label='Bing')
    cmds.menuItem(label='Bang')
    cmds.menuItem(label='Bo')   
    cmds.separator( height=5, style='out' )  
    cmds.button('Set Character ID attribute', h= 30, backgroundColor= [0.1,.4,.4], c=setCharacterID)
    cmds.button('Character Prefix', h= 30, backgroundColor= [0.4,.1,.25], c=characterName)

    cmds.separator( height=10, style='in' )
    cmds.setParent("..")

    cmds.columnLayout('PrLay', w=295)
    cmds.separator(style='none', h=10)
    cmds.text('Props', fn='boldLabelFont', w=280)
    cmds.separator( height=7, style='none' )
    cmds.rowColumnLayout("sepprLayout", numberOfColumns=1, columnWidth=(1,280))
    cmds.separator( height=7, style='out' )
    cmds.setParent("..")

    cmds.rowColumnLayout("EnvColorLay", numberOfColumns=1, columnWidth=(1,280))
    cmds.button('Props Prefix', h= 30, backgroundColor= [0.4,.1,.25], c=propName)
    cmds.separator( height=10, style='in' )
    cmds.setParent("..")

    cmds.columnLayout('EnvLay', w=295)
    cmds.separator(style='none', h=10)
    cmds.text('Environment', fn='boldLabelFont', w=280)
    cmds.separator( height=7, style='none' )
    cmds.rowColumnLayout("sepEnvLayout", numberOfColumns=1, columnWidth=(1,280))
    cmds.separator( height=7, style='out' )
    cmds.setParent("..")

    cmds.rowColumnLayout("EnvColorLay", numberOfColumns=1, columnWidth=(1,280))
    cmds.button('Environment Color', h= 30, backgroundColor= [0.1,.4,.4], c=Colors)
    cmds.button('Environment Prefix', h= 30, backgroundColor= [0.4,.1,.25], c=envName)
    cmds.separator( height=10, style='in' )
    cmds.setParent("..")

    cmds.setParent("..")


    cmds.columnLayout('                 RIG                ', w=300, h=180, parent=tabs)
    cmds.columnLayout('buttons', w=295)
    cmds.separator(style='none', h=10)
    cmds.text('Check', fn='boldLabelFont', w=280)


    cmds.showWindow(window)
    cmds.window(window, edit=True, w=293, h=350) 
