import maya.cmds as cmds

def snapr(ls):
	tosnap= ls[0]
	snapto=ls[1]
	rotation = cmds.xform(snapto, ro=True, q=True, ws=True)
	cmds.xform(tosnap, ro=rotation, ws=True)