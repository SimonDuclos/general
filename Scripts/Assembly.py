import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import users
import maya.app.renderSetup.model.renderSetup as renderSetup
import json


def exitWindow(*args):
	cmds.deleteUI("FAssetCreate")

def groupCreate(names, *args):
	grpList=names
	for i in grpList:
		if not cmds.objExists(i):
			cmds.group(name=i, em=True)

def referenceABC(type, update, animPubDir, *args):
	vrs= cmds.optionMenu(type+'vrsMenu', q=True, v=True)
	directory=animPubDir+'/'+vrs
	if path.isdir(directory):
		abcs = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
	for abc in abcs:
		if '_'+type+'_' in abc:
			filename=abc
	filepath=directory+'/'+filename
	if not update:
		if cmds.file(filepath, q=True, ex=True):
			cmds.file(filepath, type="Alembic", r=True, ns=type+'_ABC')
			if type=='CH':
				groupCreate(['ANIM_GRP'])
				cmds.parent('*CH*:*_GEO_*', 'ANIM_GRP')
			if type=='PR':
				groupCreate(['ANIM_GRP'])
				cmds.parent('*PR*:*_GEO_*', 'ANIM_GRP')
			if type=='CAM':
				groupCreate(['CAM_GRP'])
				cmds.parent('*CAM*:*CAM*', 'CAM_GRP')
			shaderSection('DirMenu')
	else:
		if cmds.file(filepath, q=True, ex=True):
			cmds.file(filepath, lr=type+'_ABCRN')


def referenceSHD(ch, path, filename, update, *args):
	vrs= cmds.optionMenu(ch+'vrsMenu', q=True, v=True)
	filepath=path+'/'+filename+vrs+'.mb'
	if not update:
		if cmds.file(filepath, q=True, ex=True):

			cmds.file(filepath, r=True, ns=ch+'_SHD')
	else:
		if cmds.file(filepath, q=True, ex=True):
			cmds.file(filepath, lr=ch+'_SHDRN')

	shaderSG = cmds.listConnections('*'+ch+'*:*',type='shadingEngine')[0]
	cmds.select('*:*'+ch+'*GEO*', hi=True)
	sel=cmds.ls(sl=True)
	for i in sel:
		if 'GRP' in i:
			sel.remove(i)

	cmds.sets(sel, e=True, forceElement=shaderSG)


	groupCreate(['SHD_GRP'])
	uti=cmds.ls(ch+'_SHD:*', tr=True)
	cmds.parent(uti, 'SHD_GRP')


def shaderSection(parent, *args):
	if cmds.rowColumnLayout("SHDLayout", exists=True):
		cmds.deleteUI("SHDLayout")
	cmds.rowColumnLayout('SHDLayout', nc=4, cw=[(1,150),(2,75), (3,100), (4,100)], cs=[(2,10),(3,20),(4,10)], p=parent)

	assetgroups=cmds.ls('*:*_GEO_*')
	CHassets=[]
	PRassets=[]
	for grp in assetgroups:
		grpn=grp.split(':')[1]
		asset=grpn.split('_')[0]+'_'+grpn.split('_')[1]
		if 'CH_' in asset:
			CHassets.append(asset)
		if 'PR_' in asset:
			PRassets.append(asset)

	#foreach ch
	for ch in CHassets:
		ch=ch.split('_')[1]
		filePath = cmds.file(q=True, sn=True)

		shaderPubDir=filePath.split('05_Lighting')[0]+'01_Assets/01_Characters/'+ch+'/02_SHD/_PUB'
		if path.isdir(shaderPubDir):
			if not cmds.text(ch, exists=True):
				vrs = [f for f in os.listdir(shaderPubDir) if os.path.isfile(os.path.join(shaderPubDir, f))]
				cmds.text(ch, backgroundColor= [0.1,.4,.25])
				cmds.optionMenu(ch+'vrsMenu',backgroundColor= [0.1,.4,.4])
				listvrs=[]
				if vrs:
					filename=vrs[0][:-6]
					for vr in vrs:
						vr=vr.split('.')[0]
						num=vr.split('_')[6]

						listvrs.append(int(num))    
					if listvrs:
						vrsmax=max(listvrs)
					
					for vr in vrs:
						if vr!='.DS_Store':
							vr=vr.split('.')[0]
							num=vr.split('_')[6]
							cmds.menuItem(vr, l=num) 
							if str(vrsmax) in vr.split('_')[5]:
								cmds.optionMenu(ch+'vrsMenu', e=True, v=vr)
				cmds.button(ch+'Reference', l='Reference', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceSHD, ch, shaderPubDir, filename, False))
				cmds.button(ch+'Update', l='Update', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceSHD, ch, shaderPubDir, filename,True)) 
	#foreach pr
	for pr in PRassets:
		pr=pr.split('_')[1]
		filePath = cmds.file(q=True, sn=True)

		shaderPubDir=filePath.split('05_Lighting')[0]+'01_Assets/02_Props/'+pr+'/02_SHD/_PUB'
		if path.isdir(shaderPubDir):
			if not cmds.text(pr, exists=True):
				vrs = [f for f in os.listdir(shaderPubDir) if os.path.isfile(os.path.join(shaderPubDir, f))]
				cmds.text(pr, backgroundColor= [0.1,.4,.25])
				cmds.optionMenu(pr+'vrsMenu',backgroundColor= [0.1,.4,.4])
				listvrs=[]
				if vrs:
					filename=vrs[0][:-6]
					for vr in vrs:
						vr=vr.split('.')[0]
						num=vr.split('_')[6]

						listvrs.append(int(num))    
					if listvrs:
						vrsmax=max(listvrs)
					
					for vr in vrs:
						if vr!='.DS_Store':
							vr=vr.split('.')[0]
							num=vr.split('_')[6]
							cmds.menuItem(vr, l=num) 
							if str(vrsmax) in vr.split('_')[5]:
								cmds.optionMenu(pr+'vrsMenu', e=True, v=vr)

				cmds.button(pr+'Reference', l='Reference', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceSHD, pr, shaderPubDir, filename, False))
				cmds.button(pr+'Update', l='Update', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceSHD, pr, shaderPubDir, filename,True))  

def importRenderSetup(directory, *ags):
	filename=directory+'/'+cmds.optionMenu('rdrSettingsMenu', q=True, v=True)
	with open(str(filename), "r") as file:
		renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)

def setFR(*args):
	if cmds.objExists('*CH_ABC*:*AlembicNode'):
		end=int(cmds.getAttr('*CH_ABC*:*AlembicNode.endFrame'))
		cmds.playbackOptions(maxTime=end, aet=end)
	elif cmds.objExists('*PR_ABC*:*AlembicNode'):
		end=int(cmds.getAttr('*PR_ABC*:*AlembicNode.endFrame'))
		cmds.playbackOptions(maxTime=end, aet=end)

def setFrameRate(*args):
	frnum=cmds.optionMenu('FrameRate', q=True, v=True)
	if frnum=='24':
		fps='film'
	elif frnum=='25':
		fps='pal'
	elif frnum=='30':
		fps='ntsc'
	else:
		fps=frnum
	cmds.currentUnit(t=fps)

def assemblyWin(*args):
	filePath = cmds.file(q=True, sn=True)
	filename=(os.path.split(filePath)[1].split('.')[0])
	shotname=filename.split('_')[3]
	animPubDir=filePath.split('05_Lighting')[0]+'04_Animation/'+shotname+'/_PUB'

	groups=['ANIM_GRP', 'ENV_GRP', 'CAM_GRP', 'LIGHTING_GRP', 'SHD_GRP', 'UTILITY_GRP']

	if path.isdir(animPubDir):
		vrss = [f for f in os.listdir(animPubDir) if os.path.isdir(os.path.join(animPubDir, f))]

	chvrss=[]
	prvrss=[]
	camvrss=[]

	if path.isdir(animPubDir):
		for vrs in vrss:
			if path.isdir(animPubDir+'/'+vrs):
				content = [f for f in os.listdir(animPubDir+'/'+vrs) if os.path.isfile(os.path.join(animPubDir+'/'+vrs, f))]
			for i in content:
				if '_CH_' in i:
					chvrss.append(vrs)
				
				if '_PR_' in i:
					prvrss.append(vrs)
				if '_CAM_' in i:
					camvrss.append(vrs)     
			maxchvrs=[]
			for chvrs in chvrss:
				maxchvrs.append(int(chvrs))
			dispchvrs='None'
			for chvrs in chvrss:
				if int(chvrs)==max(maxchvrs):
					dispchvrs=chvrs
			
			maxprvrs=[]
			for prvrs in prvrss:
				maxprvrs.append(int(prvrs))
			dispprvrs='None'
			for prvrs in prvrss:
				if int(prvrs)==max(maxprvrs):
					dispprvrs=prvrs

			maxcamvrs=[]
			for camvrs in camvrss:
				maxcamvrs.append(int(camvrs))
			dispcamvrs='None'
			for camvrs in camvrss:
				if int(camvrs)==max(maxcamvrs):
					dispcamvrs=camvrs

		#RENDERSETTINGS
		pdir= cmds.workspace(q=True, rd=True) 
		rdrSetDir=pdir+'/04_3D/_Useful/Scripts/RenderSettings'
		if path.isdir(rdrSetDir):
			rdrsetups = [f for f in os.listdir(rdrSetDir) if os.path.isfile(os.path.join(rdrSetDir, f))]
		
		#WINDOW

		if cmds.window("AssemblyWindow", exists=True):
			cmds.deleteUI("AssemblyWindow")
		window = cmds.window("AssemblyWindow", title= "Forest Assembly Tool", w=400, h=200, sizeable=True, backgroundColor= [0.1,.2,.2])

		parent=cmds.columnLayout('DirMenu')
		cmds.rowColumnLayout("DirLayout", numberOfColumns=1, columnWidth=(1,470))
		cmds.separator( height=30, style='none')
		cmds.text('directory', al='center', font='fixedWidthFont', l=shotname)


		cmds.separator( height=20, style='none')
		cmds.separator( height=20, style='none')

		cmds.text('Groups', al='center', font='fixedWidthFont')
		cmds.separator(h=15, style='in' )
		

		cmds.button('CreateGroups', l='Create Groups', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(groupCreate, groups))  


		cmds.separator( height=20, style='none')
		cmds.separator( height=20, style='none')
		framerates=['24', '25', '30', '29.97fps']
		cmds.text('Framerate', al='center', font='fixedWidthFont')
		cmds.separator(h=15, style='in' )	
		cmds.rowColumnLayout('FramerateLayout', nc=3, cw=[(1,150),(2,195), (3,100)], cs=[(2,10),(3,10)])
		cmds.text('Framerate', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('FrameRate',backgroundColor= [0.1,.4,.4])
		for framerate in framerates:
			cmds.menuItem(framerate) 
		cmds.button('Set Framerate', backgroundColor= [0.1,.4,.25], h=40, w=100, c=setFrameRate)  
		cmds.setParent('..')
		cmds.separator( height=30, style='none')



		cmds.text('Render Settings', al='center', font='fixedWidthFont')
		cmds.separator(h=15, style='in' )
		
		cmds.rowColumnLayout('RenderLayout', nc=3, cw=[(1,150),(2,195), (3,100)], cs=[(2,10),(3,10)])
		cmds.text('Settings', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('rdrSettingsMenu',backgroundColor= [0.1,.4,.4])
		if path.isdir(rdrSetDir):
			for rdrsetup in rdrsetups:
				if rdrsetup!='.DS_Store':
					cmds.menuItem(rdrsetup) 
		cmds.button('ImportSettings', l='Import', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(importRenderSetup, rdrSetDir))  
		cmds.setParent('..')
		cmds.separator( height=30, style='none')



		cmds.text('Alembics', al='center', font='fixedWidthFont', l='Alembics')
		cmds.separator(h=15, style='in' )
		cmds.rowColumnLayout('ABCLayout', nc=4, cw=[(1,150),(2,75), (3,100), (4,100)], cs=[(2,10),(3,20),(4,10)])

		#CH
		cmds.text('CH', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('CHvrsMenu',backgroundColor= [0.1,.4,.4])
		cmds.menuItem('None')
		if path.isdir(animPubDir):
			for chvrs in chvrss:
				if chvrs!='.DS_Store':
					cmds.menuItem(chvrs)
		cmds.optionMenu('CHvrsMenu', e=True, v=dispchvrs) 
		cmds.button('CHReference', l='Reference', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'CH', False, animPubDir))
		cmds.button('CHUpdate', l='Update', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'CH', True, animPubDir))

		#PR
		cmds.text('PR', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('PRvrsMenu',backgroundColor= [0.1,.4,.4])
		cmds.menuItem('None')
		if path.isdir(animPubDir):
			for prvrs in prvrss:
				if prvrs!='.DS_Store':
					cmds.menuItem(prvrs) 
		cmds.optionMenu('PRvrsMenu', e=True, v=dispprvrs) 
		cmds.button('PRReference', l='Reference', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'PR', False, animPubDir))
		cmds.button('PRUpdate', l='Update', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'PR', True, animPubDir))

		#CAM
		cmds.text('CAM', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('CAMvrsMenu',backgroundColor= [0.1,.4,.4])
		cmds.menuItem('None')
		if path.isdir(animPubDir):
			for camvrs in camvrss:
				if camvrs!='.DS_Store':
					cmds.menuItem(camvrs)
		cmds.optionMenu('CAMvrsMenu', e=True, v=dispcamvrs)  
		cmds.button('CAMReference', l='Reference', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'CAM', False, animPubDir))
		cmds.button('CAMUpdate', l='Update', backgroundColor= [0.1,.4,.25], h=40, w=100, c=partial(referenceABC, 'CAM', True, animPubDir))

		cmds.setParent('..')
		cmds.separator( height=20, style='none')
		cmds.separator( height=20, style='none')

		cmds.text('Frame Range', al='center', font='fixedWidthFont')
		cmds.separator(h=15, style='in' )
		

		cmds.button('SetFR', l='Set Frame Range', backgroundColor= [0.1,.4,.25], h=40, w=100, c=setFR)  

		cmds.separator( height=30, style='none')
		cmds.text('Shaders', al='center', font='fixedWidthFont', l='Shaders')
		cmds.separator(h=15, style='in' )

		shaderSection(parent)
		
		cmds.showWindow(window)
		cmds.window(window, edit=True, w=400, h=125) 




