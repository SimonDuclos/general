import maya.cmds as cmds 
import os
import os.path
from os import path

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path
