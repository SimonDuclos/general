import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import importlib

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path


pdir= cmds.workspace(q=True, rd=True) 
colordir = pdir+'/_Useful/Scripts/ColorSets'
make_dir(colordir)
colorSets = [f for f in os.listdir(colordir) if os.path.isdir(os.path.join(colordir, f))]