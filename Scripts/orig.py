import maya.cmds as cmds

def orig(selection):
    for sel in selection:
        if "_" in sel:
            name_split = sel.split("_")
            name_split[-1] = "GRP"
            group_name = "_".join(name_split)
        else:
            group_name = "{}_GRP".format(sel)
        trans = cmds.xform(sel, q=True, ws=True, t=True)
        rot = cmds.xform(sel, q=True, ws=True, ro=True)
        orig = cmds.group(em=True, n=group_name)
        cmds.xform(orig, t=trans, ws=True)
        cmds.xform(orig, ro=rot, ws=True)
        par = cmds.listRelatives(sel, p=True, f=True)
        if par:
            cmds.parent(orig, par)
        cmds.parent(sel, orig)
