import maya.cmds as cmds
import maya.mel as mel

def selectSkinnedJnts (*args):
	selection = cmds.ls(sl=True)
	sc=mel.eval('findRelatedSkinCluster '+selection[0])
	cmds.select(cmds.skinCluster(q=True, inf=sc))