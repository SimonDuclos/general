import maya.cmds as cmds

def addDivider(*args):
    sel = cmds.ls(sl=True)
    for i in sel:
        x=0
        n=1
        while x==0:
            if cmds.attributeQuery("globaldivider_"+str(n).zfill(3), node=i, exists=True):
                n+=1
            else:
                cmds.addAttr(i, ln="globaldivider_"+str(n).zfill(3), nn=' ', at="enum",  en="---------------:", k=True)
                x=1
