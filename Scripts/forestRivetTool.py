import maya.cmds as cmds
import maya.mel as mel

def rivet(*args):

    path = cmds.internalVar(upd=True)+ "ForestTools.txt"
    f=open(path, 'r')
    toolDir= f.readline()
    path = toolDir+"/General/Scripts"
    source='source "'+path+'/rivet.mel"'
    mel.eval(source)
    mel.eval('rivet()')