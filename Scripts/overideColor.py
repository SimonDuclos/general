from maya import cmds
from functools import partial

def overideColor(*args):
   
   shapes = cmds.listRelatives(cmds.ls(sl=True), s=True)
   color=cmds.colorIndexSliderGrp('colorPicker', q=True, v=True)-1
   for s in shapes:
       cmds.setAttr("{}.overrideEnabled".format(s), 1)
       cmds.setAttr("{}.overrideColor".format(s), color)

def overideColorWindow(*args):
    if cmds.window("FColorWindow", exists=True):
        cmds.deleteUI("FColorWindow")
    window = cmds.window("FColorWindow", title= "Forest Color Ctrl Tool", w=480, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('Color', w=300, h=180, cat=('left', 1))
    cmds.separator(h=10)
    cmds.colorIndexSliderGrp('colorPicker', label='Select Color', min=1, max=31, value=10 )
    cmds.separator(h=10)
    cmds.rowColumnLayout('Colbutton', cal=(1,'center'), cw=(1,473))
    cmds.button('changeColor', l='Apply',  h=40, c=overideColor, backgroundColor= [0.4,.1,.25])

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=480, h=90) 
    

    
