import pymel.core as pm

import maya.cmds as cmds 

def orig(ctl):
    group_name = str(ctl.replace('CTL', 'GRP'))
    trans = cmds.xform(ctl, q=True, ws=True, t=True)
    rot = cmds.xform(ctl, q=True, ws=True, ro=True)
    orig = cmds.group(em=True, n=group_name)
    cmds.xform(orig, t=trans, ws=True)
    cmds.xform(orig, ro=rot, ws=True)
    par = cmds.listRelatives(ctl, p=True, f=True)
    if par:
        cmds.parent(orig, par)
    cmds.parent(ctl, orig)

def ctlsphere(name, ctlsize):
    curve_01 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_02 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_03 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    
    cmds.rotate(90, 0, 0, curve_02[0], relative=True, objectSpace=True)
    cmds.rotate(0, 0, 90, curve_03[0], relative=True, objectSpace=True)
        
    cmds.makeIdentity(curve_02[0], curve_03[0], apply=True, rotate=True)
    
    curveShape_01 = cmds.listRelatives(curve_01[0], shapes=True) or []    
    curveShape_02 = cmds.listRelatives(curve_02[0], shapes=True) or []
    curveShape_03 = cmds.listRelatives(curve_03[0], shapes=True) or []
                
    cmds.parent(curveShape_02[0], curve_01[0], relative=True, shape=True)
    cmds.parent(curveShape_03[0], curve_01[0], relative=True, shape=True)
        
    cmds.delete(curve_02[0], curve_03[0])
        
    ctl = cmds.rename(curve_01[0], name)
    
    ctlshape= cmds.listRelatives(ctl, shapes=True) or []
    
    return ctl, ctlshape


def createFolicule(name, surf, paramU, paramV):
    folicle=pm.createNode("follicle")
    folicleTrans=pm.listRelatives(folicle,type='transform',p=True)
    cmds.rename( folicleTrans, name)
    pm.connectAttr(folicle + ".outRotate", folicleTrans[0] + ".rotate")
    pm.connectAttr(folicle + ".outTranslate", folicleTrans[0] + ".translate")
    pm.connectAttr(surf+".worldSpace",folicle+".inputSurface")
    pm.setAttr(folicle + ".simulationMethod", 0)
    pm.setAttr(folicle+'.parameterU', paramU)
    pm.setAttr(folicle+'.parameterV', paramV)
    cmds.hide(folicleTrans[0]+'Shape')
    return folicleTrans


def createRib(name, rig, puppet):
    
    mCtlSize = cmds.floatField('mCtlSize', q=True, v=True)
    sCtlSize = mCtlSize/2
       
    rig=rig
    
    puppet=puppet
    
    nbr_cv = cmds.intField('nbr_cv', q=True, v=True)
    if nbr_cv%2==0: 
        nbr_cv += 1
        
    size = float(cmds.intField('size', q=True, v=True))
    
    lr = 1/size
    
    coef = size/(nbr_cv-1)
    
    cv_mid = (nbr_cv-1)/2
    
    cv_max = nbr_cv-1
    
    nbr_cv_min = [0, cv_mid ,cv_max ]
    
    ctls=[]
    
    for i in xrange(nbr_cv):
        ctls.append(i)
    
    secctl = [item for item in ctls if item not in nbr_cv_min]
       
  #Create all Surface
  
    #Main CTLs Surf
    surforig = cmds.nurbsPlane(n=name+'_SurfOrig_NSURF', degree=1, w=size, lr=lr,  ax=[0,1,0], u=2)
    
    #Ribbon Surf
    surfsec = cmds.nurbsPlane(n=name+'_SurfSec_NSURF', degree=3, w=size, lr=lr,  ax=[0,1,0], u=nbr_cv-1)

    
    #Create Main Clusters (1)
    ctlgrp = cmds.group(em=True, n=name+'_CTL_GRP', p=rig)
    
    x= 0
    for i in nbr_cv_min:
        
        cls = cmds.cluster(name+'_SurfOrig_NSURF' + '.cv[' + str(x) + '][*]', n='C_'+name+'_MainCluster_{}_CLS'.format(str(i).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, name+'_MainClusterHandle_{}_CLH'.format(str(i).zfill(3)))
        x = x+1
        
        
    #Create Main CTLS (1)
        
        a = ctlsphere(name+'_RibbonMain_{}_CTL'.format(str(i).zfill(3)), mCtlSize)
        ctl=a[0]
        ctlshape= a[1]
        cmds.hide(ctlshape)
        
        if cmds.checkBox('fk', q=True, v=True):
            cmds.showHidden(ctlshape)
        
        cmds.delete(cmds.pointConstraint(clh, ctl, maintainOffset = False))
        cmds.parent(clh,ctl)
        
        orig(ctl)
        
        cmds.hide(clh)

        
    #Create Main Fol (1) 
    
    nbr_fol = nbr_cv-3
    fol_mid = (nbr_fol/2)+1
    U=0.5
    V=0
    coef = (1/(float(nbr_cv)-1))
    
    for i in xrange (nbr_fol):
        
        if i+1 == fol_mid:            
            V = V+(coef*2) 
        else :            
            V = V+coef 
                       
        createFolicule(name+'_RibbonMain_{}_FCTL'.format(str(secctl[i]).zfill(3)), surforig[0], V, U)
           
    
    #SurfRIB
    
    #Create Clusters (2)
    
    skip = [1, nbr_cv]
    do =[]
    for i in xrange(nbr_cv+2):
        do.append(i)
    
    do = [item for item in do if item not in skip]
    for i in xrange(nbr_cv):
        cls = cmds.cluster(surfsec[0] + '.cv[' + str(do[i]) + '][*]', n=name+'_RibCluster_{}_CLS'.format(str(do[i]).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, name+'_RibClusterHandle_{}_CLH'.format(str(do[i]).zfill(3)))
                     
        
    #Create CTLS (2)
        
        b = ctlsphere(name+'_RibbonSec_{}_CTL'.format(str(i).zfill(3)), sCtlSize)
        
        ctl = b[0]
        ctlshape= b[1]
        
        cmds.delete(cmds.pointConstraint(clh, ctl, maintainOffset = False))
        cmds.parent(clh,ctl)
        
        orig(ctl)
        cmds.hide(clh)

    for i in skip:
        cls = cmds.cluster(surfsec[0] + '.cv[' + str(i) + '][*]', n=name+'_RibCluster_{}_CLS'.format(str(i).zfill(3)))
        handle = cls[1]
        clh = cmds.rename(handle, name+'_RibClusterHandle_{}_CLH'.format(str(i).zfill(3)))
        
        if i == 1:
            par = name+'_RibbonSec_000_CTL'
        else:
            par = name+'_RibbonSec_{}_CTL'.format(str(cv_max).zfill(3))
        cmds.parent(clh,par)
        cmds.hide(clh)
        
    #Create Fol (2)
    
    folnum = (nbr_cv*2)+1
    V =0.5
    
     
    for i in xrange(folnum): 
        
        U = float(i)/(folnum-1)  
        
        a = createFolicule(name+'_RibbonSec_{}_FOL'.format(str(i).zfill(3)), surfsec[0], U, V)
        folicleTrans = a
        bone = cmds.joint(p=(0,0,0), n=name+'_ArmRibbon_{}_JNT'.format(str(i).zfill(3)), r=0.25)
        translate = cmds.xform(folicleTrans, t=True, q=True, ws=True)
        cmds.xform(bone, t=translate, ws=True)
        cmds.parent (bone, folicleTrans)
        if cmds.checkBox('jntvis', q=True, v=True):
            cmds.hide(bone)
            

    #Parent CRVRIB to CRV
    
    for i in secctl:
        child = name+'_RibbonSec_{}_GRP'.format(str(i).zfill(3))
        par = name+'_RibbonMain_{}_FCTL'.format(str(i).zfill(3))
        cmds.parent(child,par)
        
    for i in nbr_cv_min:
        child = name+'_RibbonSec_{}_GRP'.format(str(i).zfill(3))       
        par = name+'_RibbonMain_{}_CTL'.format(str(i).zfill(3))
        cmds.parent(child,par)     
            
            
    
    surfaces  = cmds.ls(name+'*'+'NSURF')
    group_name = name+'_Surfaces_GRP'
    surfgrp = cmds.group(surfaces, n=group_name, p=puppet)
    if cmds.checkBox('surfvis',q=True, v=True):
        cmds.hide(surfgrp)
    
    follicles = cmds.ls(name+'*'+'FCTL')
    group_name = name+'_SecCTL_GRP'
    cmds.group(follicles , n=group_name, p=ctlgrp)
    
    
    cmds.select(name+'*'+'FOL')
    follicles = cmds.ls(sl=True)
    group_name = name+'_Follicles_GRP'
    cmds.group(follicles , n=group_name, p=puppet)
    
    
    if cmds.checkBox('fk', q=True, v=True):
        cmds.parent(name+'_RibbonMain_{}_GRP'.format(str(cv_mid).zfill(3)),name+'_RibbonMain_000_CTL')
        cmds.parent(name+'_RibbonMain_{}_GRP'.format(str(cv_max).zfill(3)),name+'_RibbonMain_00{}_CTL'.format(cv_mid))

    #
    #
    #

def createNew(*args):
    name= cmds.textField('name', q=True, tx=True)
    rig = cmds.group(em=True, n=name+'_RIG_GRP')
    puppet= cmds.group(em=True, n=name+'_PUPPET_GRP', p=rig)
    createRib(name, rig, puppet)
      
    #
    #
    #     

def snap(child, par):
    translate = cmds.xform(par, t=True, q=True, ws=True)
    rotation = cmds.xform(par, ro=True, q=True, ws=True)
    cmds.xform(child, t=translate, ro=rotation, ws=True)
    cmds.parentConstraint(par, child)      

def snapAdvance(*args): 
    vrs=1         
         
def snapMgear(*args):
    
    nbr_cv = cmds.intField('nbr_cv', q=True, v=True)
    if nbr_cv%2==0: 
        nbr_cv += 1
        
    size = float(cmds.intField('size', q=True, v=True))
    
    coef = size/(nbr_cv-1)
    
    cv_mid = (nbr_cv-1)/2
    
    cv_max = nbr_cv-1
    
    nbr_cv_min = [0, cv_mid ,cv_max ]
    
     

    createRib('Lf_Arm_Ribbon', cmds.ls('world_ctl'), cmds.ls('setup'))
    
    snap(cmds.ls('Lf_Arm_Ribbon_RibbonMain_000_GRP'), cmds.ls('arm_L2_0_jnt'))
    snap(cmds.ls('Lf_Arm_Ribbon_RibbonMain_{}_GRP'.format(str(nbr_cv_min[1]).zfill(3))), cmds.ls('arm_L2_4_jnt'))
    snap(cmds.ls('Lf_Arm_Ribbon_RibbonMain_{}_GRP'.format(str(nbr_cv_min[2]).zfill(3))), cmds.ls('arm_L2_end_jnt'))
       
    createRib('Rt_Arm_Ribbon', cmds.ls('world_ctl'), cmds.ls('setup'))

    snap(cmds.ls('Rt_Arm_Ribbon_RibbonMain_000_GRP'), cmds.ls('arm_R2_0_jnt'))
    snap(cmds.ls('Rt_Arm_Ribbon_RibbonMain_{}_GRP'.format(str(nbr_cv_min[1]).zfill(3))), cmds.ls('arm_R2_4_jnt'))
    snap(cmds.ls('Rt_Arm_Ribbon_RibbonMain_{}_GRP'.format(str(nbr_cv_min[2]).zfill(3))), cmds.ls('arm_R2_end_jnt'))
    
    
def autoBendyArms(*args):
    #Window Parameters

    if cmds.window('Create Ribbon - Define Parm', exists = True):
        cmds.deleteUI('Create Ribbon - Define Parm')

    define = cmds.window('Create Ribbon - Define Parm', width=150, maximizeButton=True, minimizeButton=True, titleBar=True, backgroundColor= [0.1,.2,.2])

    cmds.columnLayout(adj=True)

    cmds.text('    ')

    cmds.button (label='  Create Ribbon Chain  ', recomputeSize=True, command=createNew, backgroundColor=[0.1,.4,.4], height=50)

    cmds.text('    ')

    cmds.separator()

    cmds.rowColumnLayout('buttons', numberOfColumns=2, columnWidth=[(1, 150), (2, 150)])

    cmds.text('    ')
    cmds.text('    ')

    cmds.button (label=' Snap to Mgear  ', recomputeSize=True, command=snapMgear, backgroundColor=[0.1,.4,.25], height=40)
    cmds.button (label=' Snap to Advance  ', recomputeSize=True, command=snapAdvance, backgroundColor=[0.4,.1,.25], height=40, enable=False)

    cmds.text('    ')
    cmds.text('    ')

    cmds.separator()
    cmds.separator()

    cmds.text('    ')
    cmds.text('    ')

    cmds.setParent('..')

    cmds.rowColumnLayout('param', numberOfColumns=2, columnAttach=(1, 'right', 0), columnWidth=[(1, 150), (2, 150)])

    cmds.text( label='Rig Name  ')
    cmds.textField('name', text='Ribbon')

    cmds.text( label='Secondary Ctls Number  ')
    cmds.intField('nbr_cv', minValue=5, maxValue= 20, value=5)

    cmds.text( label='Ribbon Size  ')
    cmds.intField('size', minValue=1, value=10)

    cmds.text( label='Main CTLs Size  ')
    cmds.floatField('mCtlSize', minValue=0.001, maxValue= 20, value=1)

    cmds.text( label='FK Chain  ')
    cmds.checkBox('fk')

    cmds.text( label='Hide Joints  ')
    cmds.checkBox('jntvis')

    cmds.text( label='Hide Ribbon  ')
    cmds.checkBox('surfvis', value=True)

    cmds.text('    ')


    cmds.showWindow(define)  
    
    
    





