import maya.cmds as cmds
import maya.mel as mel
from functools import partial
import pymel.core as pm
import os

def batchSetAttr (*args):
    attrib=cmds.textField('attrname', q=True, tx=True)
    value=cmds.floatField('attrval', q=True, v=True)

    selection = cmds.ls(sl=True)
    for i in selection:
        nodes=[i]
        count=0
        
        for a in (cmds.listRelatives(i, f=True)):
            if a is not None: nodes.append(a)
        if cmds.listConnections(cmds.listRelatives(i, f=True), type='AlembicNode'):
            for b in cmds.listConnections(cmds.listRelatives(i, f=True), type='AlembicNode'):
                nodes.append(b)
        for c in nodes:
            attr=c+'.'+attrib
            if cmds.attributeQuery(attrib, node=c, exists=True):
                cmds.setAttr(attr, value)
                count+=1
        print (i+": node : "+c+"'s "+attrib+" attribute as been set to "+str(value))
        if count==0:
            print('No corresponding attribute found for '+i)

def BSAWindow (*args):
    OpenCapture()
    if cmds.window("BSAWindow", exists=True):
            cmds.deleteUI("BSAWindow")
    window = cmds.window("BSAWindow", title= "Batch Set Attribute Tool", w=250, h=150, sizeable=False, backgroundColor= [0.1,.2,.2])    

    cmds.columnLayout('BSA')
    cmds.rowColumnLayout("name", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,90), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Attribute Name')
    cmds.textField('attrname', tx='Name')
    cmds.text("  ")
    cmds.button('Get Last Attribute', backgroundColor= [0.1,.4,.25], command=GetLastAttrib)
    cmds.text("  ")
    cmds.text('Example : OverrideColor', font="obliqueLabelFont")

    cmds.setParent("..")
    cmds.rowColumnLayout("value", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,90), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ")

    cmds.text('Attribute Value')
    cmds.floatField('attrval', v=1)
    
    
    cmds.setParent("..")
    cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,250])
    cmds.text("  ")
    cmds.setParent("..")
    cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=1, columnWidth=(1,250))

    cmds.button('Batch Set Attribute', backgroundColor= [0.1,.4,.25], command=batchSetAttr) 
    cmds.setParent("..")
    cmds.rowColumnLayout("CloseLayout", numberOfColumns=1, columnWidth=(1,250))  
    cmds.separator(h=10, style='none')
    cmds.button('Close Window ', backgroundColor= [0.4,.1,.25],command = closeWindow)
    cmds.showWindow(window)
    cmds.window(window, edit=True, w=250, h=200) 






def OpenCapture(*args):

    TEMP_FILE = os.getenv("TEMP") + ("/maya_script_editor_capture.txt")
    with open(TEMP_FILE, "w") as f:
        f.write("")

    pm.scriptEditorInfo(writeHistory=True, historyFilename=TEMP_FILE)



def GetLastAttrib(*args):

#    pm.scriptEditorInfo(writeHistory=False)
    TEMP_FILE = os.getenv("TEMP") + ("/maya_script_editor_capture.txt")
    with open(TEMP_FILE, 'r') as f:
        lines = f.read().splitlines()
        lnum=0
        while 'setAttr' not in lines[lnum]:   
            print (lines[lnum])
            print (str(lnum))
            lnum-=1
        
        last_line = lines[lnum]
        attrib=last_line.split('.')[1]
        attrib=attrib.split('"')[0]
        cmds.textField('attrname', e=True, tx=attrib)


def closeWindow(*pArgs):
    
    # Closes UI
    
    cmds.deleteUI('BSAWindow')
    pm.scriptEditorInfo(writeHistory=False)

