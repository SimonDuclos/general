import maya.cmds as cmds

def ctlsphere(name, ctlsize):
    curve_01 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_02 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    curve_03 = cmds.circle(center=(0, 0, 0), normal=(0, 1, 0), sweep=360, radius=ctlsize, degree=3, useTolerance=False, constructionHistory=True) or []
    
    cmds.rotate(90, 0, 0, curve_02[0], relative=True, objectSpace=True)
    cmds.rotate(0, 0, 90, curve_03[0], relative=True, objectSpace=True)
        
    cmds.makeIdentity(curve_02[0], curve_03[0], apply=True, rotate=True)
    
    curveShape_01 = cmds.listRelatives(curve_01[0], shapes=True) or []    
    curveShape_02 = cmds.listRelatives(curve_02[0], shapes=True) or []
    curveShape_03 = cmds.listRelatives(curve_03[0], shapes=True) or []
                
    cmds.parent(curveShape_02[0], curve_01[0], relative=True, shape=True)
    cmds.parent(curveShape_03[0], curve_01[0], relative=True, shape=True)
        
    cmds.delete(curve_02[0], curve_03[0])
        
    ctl = cmds.rename(curve_01[0], name)
    
    ctlshape= cmds.listRelatives(ctl, shapes=True) or []
    
    return ctl, ctlshape