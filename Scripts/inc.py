import maya.cmds as cmds
import os
import users

def inc(*args):
    filePath = cmds.file(q=True, sn=True) 
    filename=(os.path.split(filePath)[1].split('.')[0])
    filepath = (os.path.split(filePath)[0])
    former=filename.split('_')


    if filename=='':
        cmds.warning('Please use Saving Tool to create scene name first')

    else:
        former=filename.split('_')
        pnum=former[0]
        pname=former[1]+'_'+former[2]
        fasset=former[3]
        fjob=former[4]
        fname=former[6]
        fvrs=former[5]
        vrs=int(fvrs)+1

        filename= pnum+'_'+pname+'_'+fasset+'_'+fjob+'_'+str(vrs).zfill(3)+'_'+fname
        
        cmds.file( rename=filepath+'/'+filename)
        cmds.file( save=True, type='mayaAscii' )
