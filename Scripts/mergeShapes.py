import maya.cmds as cmds 

def mergeShapes(*args):
	sel= cmds.ls(sl=True)
	parent = sel[0]
	sel.remove(parent)

	for i in sel:
		shape = cmds.listRelatives(i, shapes=True) or []
		cmds.parent(shape, parent, relative=True, shape=True)
