import maya.cmds as cmds
import MoveAllAnim
import pymel.core as pymel
import maya.mel as mel
import os
import sys

def checkName(*args):
	toRename=[]
	cmds.select(ado=True)
	objs=cmds.ls(sl=True)
	ignore = ['TEMP','left','back', 'bottom']
	for obj in objs:
		if obj not in ignore:
			if len(obj.split('_'))<2:
				cmds.warning(obj + ' - object needs to be renamed before publish')
				toRename.append(obj)
	
	if toRename==[]:
		cmds.select(clear=True)
		return True
	else:
		cmds.select(toRename)
		return False

def resetCTL(*args):
	cmds.select('*_CTL')
	ctls=cmds.ls(sl=True, fl=True)
	cmds.select(clear=True)
	transforms = ['.translateX','.translateY','.translateZ','.rotateX','.rotateY','.rotateZ']
	scales = ['.scaleX','.scaleY','.scaleZ']
	for ctl in ctls:
		for transform in transforms:
			attr = ctl+transform
			if cmds.getAttr(attr, k=True):
				if not cmds.getAttr(attr, l=True):
				    if  not cmds.connectionInfo(attr, id=True):
					    cmds.setAttr(attr, 0)
		for transform in scales:
			attr = ctl+transform
			if cmds.getAttr(attr, k=True):
				if not cmds.getAttr(attr, l=True):
				    if not cmds.connectionInfo(attr, id=True):
					    cmds.setAttr(attr, 1)

def delNS(*args):
    ''' 
    Find all namespaces in scene and remove them.
    Except for default namespaces
    '''

    # Get a list of namespaces in the scene
    # recursive Flag seraches also children
    # internal Flag excludes default namespaces of Maya
    namespaces = []
    for ns in pymel.listNamespaces( recursive  =True, internal =False):
        namespaces.append(ns)

    # Reverse Iterate through the contents of the list to remove the deepest layers first
    for ns in reversed(namespaces):
        currentSpace = ns
        pymel.namespace(removeNamespace = ns, mergeNamespaceWithRoot = True)

    # Empty the List
    namespaces[:] = [] 

def delTemp(*args):
	if cmds.objExists('TEMP'):
		cmds.select('TEMP', hi=True)
		temp = cmds.ls(sl=True)
		cmds.delete(temp)

def selTriangle(*args):
	cmds.select(all=True, hi=True)
	cmds.polySelectConstraint( m=3, t=8, sz=1 ) 
	tgl = cmds.ls(sl=True, fl=True)
	cmds.polySelectConstraint( dis=True, m=0 )
	cmds.select(tgl)

def selNQuad(*args):
	cmds.select(all=True, hi=True)
	cmds.polySelectConstraint( m=3, t=8, sz=3 )
	tgl = cmds.ls(sl=True, fl=True)
	cmds.polySelectConstraint( dis=True, m=0 )
	cmds.select(tgl) 

def selHoled(*args):
	cmds.select(all=True, hi=True)
	cmds.polySelectConstraint( m=3, t=8, h=1 ) 
	tgl = cmds.ls(sl=True, fl=True)
	cmds.polySelectConstraint( dis=True, m=0 )
	cmds.select(tgl) 

def resetSelect(*args):
	cmds.polySelectConstraint( sz=0, h=0 )

def checkQuad(*args):
	toCheck=[]
	#triangles
	selTriangle()
	tr=cmds.ls(sl=True)
	toCheck.append(tr)
	#Non Quads
	selNQuad()
	nq=cmds.ls(sl=True)
	toCheck.append(nq)
	#Holed
	selHoled()
	hf=cmds.ls(sl=True)
	toCheck.append(hf)

	#delete constraints
	cmds.polySelectConstraint( sz=0, h=0 )
	if toCheck==[[], [], []]:
		return True
	else:
		cmds.warning(str(len(tr))+' mesh with triangles')
		cmds.warning(str(len(nq))+' mesh with non quads')
		cmds.warning(str(len(hf))+' mesh with holed face')
		return False

def delCam(*args):
	cam=['left','back', 'bottom']
	for c in cam:
		if cmds.objExists(c):
			cmds.delete(c)

def checkMS():
	list_error = set()
	list_transform = [cmds.listRelatives(geo, p=True, fullPath=True)[0] for geo in cmds.ls(geometry=True)]
	for mesh_trans in list_transform:
		shapes = cmds.listRelatives(mesh_trans, s=True, fullPath=True)
		if len(shapes) > 1:
			list_error.add(mesh_trans)
	if list_error:
		list_error = list(list_error)
	else:
		list_error = []
	return list_error

def fixMS(list_error):
	for mesh_trans in list_error:
		cmds.delete(mesh_trans, all=True, constructionHistory=True)
		shapes = cmds.listRelatives(mesh_trans, s=True, fullPath=True)
		if len(shapes) > 1:
			name_trans = str(mesh_trans)
			duplicate_mesh = cmds.duplicate(mesh_trans, n='duplicate_{}'.format(mesh_trans))[0]
			cmds.delete(mesh_trans)
			clean_mesh = cmds.rename(duplicate_mesh, name_trans)
			shapes = cmds.listRelatives(clean_mesh, s=True, fullPath=True)
			cmds.delete(shapes[1:])


def save(*args):
	#save current
	cmds.file(s=True, type='mayaAscii')
	#save Inc
	filePath = cmds.file(q=True, sn=True) 
	filename=(os.path.split(filePath)[1].split('.')[0])
	filepath = (os.path.split(filePath)[0])
	former=filename.split('_')
	pnum=former[0]
	pname=former[1]+'_'+former[2]
	asset=former[3]
	job=former[4]
	name=former[6]
	fvrs=former[5]
	vrs=int(fvrs)+1
	filename= pnum+'_'+pname+'_'+asset+'_'+job+'_'+str(vrs).zfill(3)+'_'+name  
	cmds.file( rename=filepath+'/'+filename)
	cmds.file(s=True, type='mayaAscii')

def moveToOrig(*args):
	# Gets the selected object
	selection = cmds.ls(sl=True)
	# Gets the bounding box of the object
	bbox = cmds.exactWorldBoundingBox(selection[0])
	# Gets the middle bottom part of the bounding box
	bottom = [(bbox[0]+bbox[3])/2, bbox[1], (bbox[2]+bbox[5])/2]
	# Sets the pivot in that point
	cmds.xform(selection, piv=bottom, worldSpace=True)
	# Moves the mesh to the world origin
	cmds.move(0, 0, 0, selection[0], rpr=True)

def PUB(*args):
	origPath = cmds.file(q=True, sn=True) 

	filePath = cmds.file(q=True, sn=True) 
	filename=(os.path.split(filePath)[1].split('.')[0])
	filepath = (os.path.split(filePath)[0])
	pubPath = filepath+'/_PUB'
	if not os.path.exists(pubPath):
		os.makedirs(pubPath)
	former=filename.split('_')
	
	pubs = [f for f in os.listdir(pubPath) if os.path.isfile(os.path.join(pubPath, f))]

	listvrs=[]
	vrs=1
	for pub in pubs:
		pub=pub.split('.')[0]
		vrs=pub.split('_')[6]
		listvrs.append(int(vrs))	
	if listvrs:
		vrs=max(listvrs)+1

	pnum=former[0]
	pname=former[1]+'_'+former[2]
	asset=former[3]
	job=former[4]


	#Save PUB
	filename= pnum+'_'+pname+'_'+asset+'_'+job+'_PUB_'+str(vrs).zfill(3)
	cmds.file( rename=pubPath+'/'+filename)
	cmds.file(s=True, type='mayaBinary')


	cmds.file(origPath, o=True)




def ModPub(*args):
	if cmds.checkBox('MODignore', q=True, v=True):
		save()
		ms = checkMS()
		fixMS(ms)
		list_meshes = cmds.ls(type='mesh')
		for m in list_meshes:
			cmds.delete(m, ch=True)
			transform = cmds.listRelatives(m, p=True)[0]
			cmds.makeIdentity(transform, a=True, t=True, r=True, s=True, n=False)
			cmds.makeIdentity(transform, a=False, t=True, r=True, s=True)
		delCam()
		delTemp()
		mel.eval('MLdeleteUnused;')
		PUB()
	else:   
		if checkName():
			if checkQuad():
				save()
				ms = checkMS()
				fixMS(ms)
				list_meshes = cmds.ls(type='mesh')
				for m in list_meshes:
					cmds.delete(m, ch=True)
					transform = cmds.listRelatives(m, p=True)[0]
					cmds.makeIdentity(transform, a=True, t=True, r=True, s=True, n=False)
					cmds.makeIdentity(transform, a=False, t=True, r=True, s=True)
				delCam()
				delTemp()
				mel.eval('MLdeleteUnused;')
				PUB()
			else:
				cmds.warning('Quad check failure')
		else:
			cmds.warning('Name check failure')

def RigPub(*args):
	if cmds.checkBox('RIGignore', q=True, v=True):
		save()
		cmds.currentTime(0)         
		MoveAllAnim.deleteAllAnim()
		resetCTL()

		if cmds.objExists('sharedReferenceNode'):
			cmds.delete('sharedReferenceNode')
		refs = cmds.ls(type='reference')
		for i in refs:
			rFile = cmds.referenceQuery(i, f=True)
			cmds.file(rFile, importReference=True)  
		delCam()
		delTemp()
		delNS()
		mel.eval('MLdeleteUnused;')
		PUB()
	else:   
		if checkName():
			save()
			cmds.currentTime(0)         
			MoveAllAnim.deleteAllAnim()
			resetCTL()

			if cmds.objExists('sharedReferenceNode'):
				cmds.delete('sharedReferenceNode')
			refs = cmds.ls(type='reference')
			for i in refs:
				rFile = cmds.referenceQuery(i, f=True)
				cmds.file(rFile, importReference=True)			

			delCam()
			delTemp()
			delNS()
			mel.eval('MLdeleteUnused;')
			PUB()
		else:
			cmds.warning('Name check failure')

def pubWindow(*args):
	if cmds.window("FPubWindow", exists=True):
		cmds.deleteUI("FPubWindow")
	window = cmds.window("FPubWindow", title= "Forest Publish Tool", w=300, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])    

	tabs= cmds.tabLayout(imw=5, imh=5, w=297, hlc=(0,.15,.1))
	cmds.columnLayout('        MOD       ', w=100, h=180, parent=tabs)

	cmds.columnLayout('buttons', w=295)
	cmds.separator(style='none', h=10)
	cmds.text('Check', fn='boldLabelFont', w=280)
	cmds.separator( height=7, style='none' )
	cmds.rowColumnLayout("sepBegLayout", numberOfColumns=1, columnWidth=(1,280))
	cmds.separator( height=7, style='out' )
	cmds.setParent("..")
	cmds.rowColumnLayout("checks", numberOfColumns=3, columnAttach=[1, 'both', 0], columnWidth=[(1,90), (2,90), (3,90)], columnSpacing=[(1,0), (2,5),(3,5)])
	cmds.button('Triangles', h=30, backgroundColor= [0.4,.1,.25], c=selTriangle)
	cmds.button('Non Quads', h=30, backgroundColor= [0.4,.1,.25], c=selNQuad)
	cmds.button('Holed', h= 30, backgroundColor= [0.4,.1,.25], c=selHoled)
	cmds.setParent("..")
	cmds.separator( height=12, style='single', hr=True)
	cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,280))
	cmds.button('Check Names', h= 30, backgroundColor= [0.4,.1,.25], c=checkName)
	cmds.separator( height=10, style='in' )
	cmds.setParent("..")

	cmds.rowColumnLayout("CenterLayout", numberOfColumns=1, columnWidth=(1,280))
	cmds.button('Move selected to center', h= 30, backgroundColor= [0.1,.4,.4], c=moveToOrig)
	cmds.separator( height=10, style='in' )
	cmds.setParent("..")

	cmds.rowColumnLayout('TextInfo', numberOfColumns=1, columnWidth=[1,280])
	cmds.separator(h=27, style='none')
	cmds.text("Publish will delete all history and freeze")
	cmds.text("all transforms. 'TEMP' group will be deleted")
	cmds.setParent("..")

	cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
	
	cmds.text("  ")
	cmds.checkBox('MODignore', label='Ignore Sanity Check')

	cmds.setParent("..")


	cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=1, columnWidth=(1,280))
	
	cmds.button('Publish', backgroundColor= [0.1,.4,.25], h=40, w=200,  command=ModPub) 
	cmds.setParent("..")
	cmds.setParent("..")




	cmds.columnLayout('         RIG        ', w=100, h=180, parent=tabs)
	cmds.columnLayout('buttons', w=295)
	cmds.separator(style='none', h=10)
	cmds.text('Check', fn='boldLabelFont', w=280)
	cmds.separator( height=7, style='none' )
	cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,280))
	cmds.separator( height=7, style='out' )
	cmds.button('Check Names', h= 30, backgroundColor= [0.4,.1,.25], c=checkName)
	cmds.separator( height=10, style='in' )
	cmds.setParent("..")
	cmds.rowColumnLayout('TextInfo', numberOfColumns=1, columnWidth=[1,280])
	cmds.separator(h=27, style='none')
	cmds.text("Publish will delete all keys, set controlers'")
	cmds.text("transforms to 0 and import all references.")
	cmds.text("'TEMP' group will be deleted")
	cmds.setParent("..")
	cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300]) 

	cmds.separator(h=27, style='none')
	cmds.checkBox('RIGignore', label='Ignore Sanity Check')
	cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=1, columnWidth=(1,280))
	cmds.button('Publish', backgroundColor= [0.1,.4,.25], h=40, w=200, c=RigPub) 

	cmds.columnLayout('        ANIM       ', w=100, h=180, parent=tabs)
	cmds.columnLayout('buttons', w=295)
	cmds.separator(style='none', h=10)
	cmds.text('Time', fn='boldLabelFont', w=280)
	cmds.separator( height=7, style='none' )
	cmds.separator( height=7, style='out' )
	cmds.rowColumnLayout("NamesLayout", numberOfColumns=2, columnWidth=[(1,75), (2,205)])
	start=cmds.playbackOptions(q=True, ast=True)
	end=cmds.playbackOptions(q=True, aet=True)
	cmds.text('Start', fn='boldLabelFont')
	cmds.intField('start', v=start)
	cmds.text('End', fn='boldLabelFont')
	cmds.intField('end', v=end)

	cmds.setParent("..")
	cmds.separator( height=10, style='in' )
	cmds.rowColumnLayout('TextInfo', numberOfColumns=1, columnWidth=[1,280])
	cmds.separator(h=27, style='none')
	cmds.text("Publish export characters ('CH_')")
	cmds.text("and props ('PR_') as alembics.")
	cmds.text("Camera will be published as .mb.")
	cmds.text("Camera naming : 'CAM_shotName(_v if multiple)")
	cmds.setParent("..")
	cmds.rowColumnLayout("sepLayout", numberOfColumns=1, columnWidth=[1,300])
	cmds.separator(h=27, style='none')
	cmds.button('Publish', backgroundColor= [0.1,.4,.25], h=40, w=200, c=animPub) 


	cmds.showWindow(window)
	cmds.window(window, edit=True, w=293, h=350) 

def abcExport(start, end, root, path, *args):
	command = "-frameRange " + str(start) + " " + str(end) +" -attr ColorID -uvWrite -worldSpace -writeVisibility" + root + " -file " + path
	cmds.AbcExport ( j = command )

def typePUB(pubPath, type, start, end, root, *args):
	origPath = cmds.file(q=True, l=True)[0]

	filePath = cmds.file(q=True, l=True)[0]
	filename=(os.path.split(filePath)[1].split('.')[0])

	former=filename.split('_')
	
	
	pnum=former[0]
	pname=former[1]+'_'+former[2]
	asset=former[3]
	job=former[4]

	#Save PUB
	filename= pnum+'_'+pname+'_'+asset+'_'+type+'_PUB.abc'
	abcExport(start, end, root, pubPath+'/'+filename)

def animPub(*args):
	origPath = cmds.file(q=True, l=True)[0]

	filePath = cmds.file(q=True, l=True)[0]
	filename=(os.path.split(filePath)[1].split('.')[0])
	filepath = (os.path.split(filePath)[0])
	pubPath = filepath+'/_PUB'
	if not os.path.exists(pubPath):
		os.makedirs(pubPath)
	former=filename.split('_')
	
	pubfolders = [f for f in os.listdir(pubPath) if os.path.isdir(os.path.join(pubPath, f))]

	listvrs=[]
	vrs=1
	for pub in pubfolders:
		listvrs.append(int(pub))	
	if listvrs:
		vrs=max(listvrs)+1
	pubPath=pubPath+'/'+str(vrs).zfill(3)
	if not os.path.exists(pubPath):
		os.makedirs(pubPath)


	meshes = cmds.ls(type='mesh')
	start=cmds.intField('start', q=True, v=True)
	end=cmds.intField('end', q=True, v=True)
	chAnim = cmds.ls('*:*CH_*GEO*')
	chAnim+=cmds.ls('*CH_*GEO*')
	roots=''
	for root in chAnim:
		if root !='':
			roots= roots+' -root '+root

	if roots!='':
		typePUB(pubPath, 'CH', start, end, roots)
	
	prAnim = cmds.ls('*:*PR_*GEO*')

	prAnim+=cmds.ls('*PR_*GEO*')
	roots=''
	for root in prAnim:
		if root !='':
			roots= roots+' -root '+root
	if roots!='':
		typePUB(pubPath, 'PR', start, end, roots)

	cams= cmds.listCameras()
	for i in cams:
		root=i.replace('Shape', '')
		if 'CAM_' in root:
			typePUB(pubPath, root, start, end, ' -root '+root)
