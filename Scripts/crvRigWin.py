import maya.cmds as cmds
import orig
import snapr
import snapt

def crvRig(*args):
    middle_edge = cmds.ls( sl= True)
    mesh = middle_edge[0].split('.')[0]
    ctlscale= 1.000
    aname=cmds.textField('aname', q=True, tx=True)
    crv, node = cmds.polyToCurve (form =2 , degree =1 )
    if cmds.checkBox('rcrv', q=True, v=True):
        cmds.reverseCurve(crv, rpo=True)
    cmds.delete(node)
    crv_shape = cmds.listRelatives(crv, s=True)[0]
    jntlist = []
    hlpnb = cmds.intField('hlpnb', q=True, v=True)
    ctllist= []

    for n in xrange(hlpnb+1):
        h = cmds.spaceLocator (n= 'R_'+aname+'_'+str(n)+'_HLP')
        place = cmds.pathAnimation (h,c= crv_shape,f=True,fm =True,  )
        cmds.setAttr(str(place)+'.uValue' , n*(1.000/hlpnb) )
        
        jnt= cmds.joint(n= 'R_'+aname+'_'+str(n)+'_JNT')
        jntlist.append(jnt)
        snap=[jnt, h]
        snapr.snapr(snap)
        snapt.snapt(snap)
        
        c= cmds.circle (n= 'R_'+aname+'_'+str(n)+'_CTL', nr =(0,1,0), r=ctlscale)
        ctllist.append('R_'+aname+'_'+str(n)+'_CTL')
        snap=[c, h]
        snapr.snapr(snap)
        snapt.snapt(snap)

    orig.orig(ctllist)


    for n in xrange(hlpnb+1):
        if cmds.objExists('R_'+aname+'_'+str(n+1)+'_CTL'):
            if cmds.checkBox('fk', q=True, v=True):
                cmds.parent ('R_'+aname+'_'+str(n+1)+'_GRP','R_'+aname+'_'+str(n)+'_CTL')
                cmds.parent ('R_'+aname+'_'+str(n+1)+'_JNT','R_'+aname+'_'+str(n)+'_JNT')
            cmds.parentConstraint ('R_'+aname+'_'+str(n)+'_CTL','R_'+aname+'_'+str(n)+'_JNT', mo=True ) 
            cmds.scaleConstraint  ('R_'+aname+'_'+str(n)+'_CTL','R_'+aname+'_'+str(n)+'_JNT')   

    if cmds.checkBox('skin', q=True, v=True):
        cmds.skinCluster(jntlist, mesh, mi=5) 
    main = cmds.circle (n= 'R_'+aname+'_Main_CTL', nr =(0,1,0),r=ctlscale*2)
    snap=['R_'+aname+'_Main_CTL', 'R_'+aname+'_0_GRP']
    snapt.snapt(snap)
    cmds.parent('R_'+aname+'_0_GRP', 'R_'+aname+'_Main_CTL')
    cmds.parent('R_'+aname+'_Main_CTL', cmds.group(em=True, n='R_'+aname+'_GRP'))     

def crvRigWin(*args):

    if cmds.window("crvRigWin", exists=True):
        cmds.deleteUI("crvRigWin")
    window = cmds.window("crvRigWin", title= "Rig from Curve", w=300, h=200, sizeable=False, backgroundColor= [0.1,.2,.2])  

    cmds.columnLayout('buttons')
    cmds.rowColumnLayout("layout", numberOfColumns=2, columnAttach=[1, 'right', 0], columnWidth=[(1,140), (2,140)], columnSpacing=[(1,5), (2,5)])

    cmds.text("  ")
    cmds.text("  ") 

    cmds.textField('aname', tx='Asset Name')
    cmds.text('Ctl Number')
    cmds.intField('hlpnb', v=5)
    cmds.checkBox('skin', l='Skin Mesh', v=True)
    cmds.checkBox('fk', l='FK Chain', v=True)
    cmds.checkBox('rcrv', l='Reverse Curve')
    cmds.button('Build', backgroundColor= [0.4,.1,.25], command=crvRig)

    cmds.showWindow(window)
    cmds.window(window, edit=True, w=300, h=150)

crvRigWin()
