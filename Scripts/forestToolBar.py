import maya.cmds as cmds
import os
from functools import partial
import maya.mel as mel

import orig
import snapr
import snapt
import dirConnect
import overideColor
import inc
import batchSetAttr



widgets={}

def forestToolBar(*args):

	if cmds.dockControl("toolbarDock", exists=True):
		cmds.deleteUI("toolbarDock")

	widgets["window"]=cmds.window(w=50,h=700, title=" ", mnb=False, mxb=False, sizeable=True, backgroundColor= [0.1,.2,.2])
	widgets["scrollLayout"]=cmds.scrollLayout(hst=0, w=50)
	widgets["mainLayout"]=cmds.columnLayout(adj=True, parent=widgets["scrollLayout"])

	populateUI()

	widgets["docks"]=cmds.dockControl("toolbarDock", label=" ", area='left', content=widgets["window"],allowedArea=['right','left'])

def populateUI():

	path = cmds.internalVar(upd=True)+ "ForestTools.txt"

	f=open(path, 'r')
	toolDir= f.readline()
	path = toolDir+"/General/icons/"
	

	icons=os.listdir(path)


	categories=[]

	for icon in icons:
		categoryName=icon.partition("__")[0]
		categories.append(categoryName)

	categoryNames=list(set(categories))


	for category in categoryNames:
#		widgets[(category+'_frameLayout')]=cmds.frameLayout(label=category,collapsable=True, parent=widgets["mainLayout"], backgroundColor= [0.1,.4,.25])
		widgets[(category+"_mainLayout")]=cmds.rowColumnLayout(nc=1)

	for icon in icons:
		if icon!= 'Thumbs.db':
			if icon=='*__00*':
				widgets[(icon+"button")]=cmds.symbolButton(w=50,h=50, image=(path+icon), parent=widgets[(category+"_mainLayout")], en=False)
			else:
				category=icon.partition("__")[0]

				command=icon.partition("__")[2].partition(".")[0]

				widgets[(icon+"button")]=cmds.symbolButton(w=50,h=50, image=(path+icon), parent=widgets[(category+"_mainLayout")], c=partial(runMethod, command))	

def runMethod(method,*args):
	exec(method+"()")

def snapr(*args):
	import snapr
	snapr.snapr(cmds.ls(sl=True))

def snapt(*args):

	import snapt
	snapt.snapt(cmds.ls(sl=True))

def orig(*args):
	import orig
	orig.orig(cmds.ls(sl=True))

def inc(*args):
	import inc
	inc.inc()

def forestRefWindow(*args):
	import referenceTool
	referenceTool.forestRefWindow()


def dirConnect(*args):
	import dirConnect
	dirConnect.dirConnect()

def overideColor(*args):
	import overideColor
	overideColor.overideColorWindow()

def batchSetAttr(*args):
	import batchSetAttr
	batchSetAttr.BSAWindow()




