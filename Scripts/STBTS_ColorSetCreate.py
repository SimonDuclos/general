import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import importlib
import STBTS_ColorSet
import ColorPickerButton
import shutil

def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def edit_dir(path, newName, *args):
    if os.path.exists(path):
        os.rename(path, newName)
    cmds.deleteUI('editSetWindow')
    setVis()
def delete_dir(path, *args):
    shutil.rmtree(path)
    cmds.deleteUI('editSetWindow')
    setVis()

def checkAttribute(objectName, attrName, *args):
    if attrName:
        fullName = objectName+'.'+attrName
    if cmds.objExists(fullName):
        return True
    else :
        return False

def createSet(colordir, *args):
    setName = cmds.textField('setName', q=True, text=True)
    directoryA=colordir+'/'+setName
    
    bodyR= cmds.floatField('Body_R', q=True, v=True)
    bodyG= cmds.floatField('Body_G', q=True, v=True)
    bodyB= cmds.floatField('Body_B', q=True, v=True)
    directoryB=directoryA+'/Body_'+str(bodyR)+'_'+str(bodyG)+'_'+str(bodyB)
    
    limbsR= cmds.floatField('Limbs_R', q=True, v=True)
    limbsG= cmds.floatField('Limbs_G', q=True, v=True)
    limbsB= cmds.floatField('Limbs_B', q=True, v=True)
    directoryC=directoryA+'/Limbs_'+str(limbsR)+'_'+str(limbsG)+'_'+str(limbsB)
    
    wheelsR= cmds.floatField('Wheels_R', q=True, v=True)
    wheelsG= cmds.floatField('Wheels_G', q=True, v=True)
    wheelsB= cmds.floatField('Wheels_B', q=True, v=True)
    directoryD=directoryA+'/Wheels_'+str(wheelsR)+'_'+str(wheelsG)+'_'+str(wheelsB)
    

    make_dir(directoryA)
    make_dir(directoryB)
    make_dir(directoryC)
    make_dir(directoryD)
    cmds.deleteUI('createSetWindow')
    setVis()


def createSetWindow(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    colordir = pdir+'/_Useful/Scripts/ColorSets'
    make_dir(colordir)

    if cmds.window( 'createSetWindow', exists = True):
        cmds.deleteUI( 'createSetWindow' )
    
    window = cmds.window('createSetWindow', t='Create Color Set',w=300, h=250, sizeable=True, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('LayoutCreateSet')

    cmds.rowColumnLayout( numberOfColumns=2, parent='LayoutCreateSet')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, w=20, style='none')
    cmds.textField('setName')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, w=20, style='none')
    cmds.button('createSet', l='Create Set', c=partial(createSet, colordir))
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')

    cmds.showWindow('createSetWindow')
    cmds.window(window, edit=True, w=100, h=60) 

def editSetWindow(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    colordir = pdir+'/_Useful/Scripts/ColorSets'
    make_dir(colordir)
    setname=cmds.optionMenu('SetMenu',  q=True, v=True)
    setdir=colordir+'/'+setname

    if cmds.window( 'editSetWindow', exists = True):
        cmds.deleteUI( 'editSetWindow' )
    
    window = cmds.window('editSetWindow', t='Create Color Set',w=300, h=250, sizeable=True, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('LayoutCreateSet')

    cmds.rowColumnLayout( numberOfColumns=2, parent='LayoutCreateSet')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    cmds.text(setname)
    cmds.textField('NewSetName', tx='New Name')
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    cmds.button('changeSet', l='Change Set Name', backgroundColor= [0.1,.4,.25], c=partial(edit_dir, setdir,  cmds.textField('NewSetName', q=True, tx=True)))
    cmds.button('deleteSet', l='Delete Set', backgroundColor= [0.4,.1,.25], c=partial(delete_dir, setdir))
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='none')
    cmds.showWindow('editSetWindow')
    cmds.window(window, edit=True, w=100, h=60) 

def getColorFolder(*args):
    reload(STBTS_ColorSet)
    setName= cmds.optionMenu('SetMenu',  q=True, v=True)
    pdir= cmds.workspace(q=True, rd=True) 
    setdir = pdir+'/_Useful/Scripts/ColorSets/'+setName
    parts = [f for f in os.listdir(setdir) if os.path.isdir(os.path.join(setdir, f))]
    for part in parts:
        partname= part.split('_')[0]
        tempColors= part.split('_')[1:]
        colorVal = [float(tempColors[0]),float(tempColors[1]),float(tempColors[2])]
        cmds.button(partname, edit=True, bgc=colorVal)
        cmds.floatField(partname+'_R', e=True, v=colorVal[0])
        cmds.floatField(partname+'_G', e=True, v=colorVal[1])
        cmds.floatField(partname+'_B', e=True, v=colorVal[2])


def setVis(*args):
    reload(STBTS_ColorSet)
    if cmds.rowColumnLayout( 'SetListLayout', exists=True):
        cmds.deleteUI('SetListLayout', 'exitSep', 'exitLayout', 'createSet', 'editSet', 'buttonlayout', 'seplayout2')

    cmds.rowColumnLayout('SetListLayout', numberOfColumns=2, columnWidth=[ (1,75), (2,225)], parent='Layout')   
    cmds.text(label = 'PRESET')
    cmds.optionMenu('SetMenu',backgroundColor= [0.1,.4,.4], cc=getColorFolder)
    for sets in (STBTS_ColorSet.colorSets):
        cmds.menuItem(sets)
    cmds.setParent('..')

    cmds.rowColumnLayout('seplayout2', numberOfColumns=1, parent='Layout')
    cmds.separator(h=10, style='none')
    cmds.setParent('..')

    cmds.rowColumnLayout('buttonlayout', numberOfColumns=4, parent='Layout')
    cmds.separator(w=30, style='none')
    cmds.button('createSet', label = 'Create Set', h=30, backgroundColor= [0.1,.4,.25], w=100, command = createSetWindow)
    cmds.separator(w=30, style='none')
    cmds.button('editSet', label = 'Edit Set', h=30, backgroundColor= [0.1,.4,.25], w=100, command = editSetWindow)
    cmds.separator(h=15, style='none')

    cmds.separator('exitSep', h=10, style='none')
    cmds.rowColumnLayout( 'exitLayout', numberOfColumns=1, columnWidth=[ (1,300)], parent='Layout')
    cmds.button('close', label = 'Close Window', h=30, backgroundColor= [0.4,.1,.25], command = closeWindow)

def Colors(sel, *args):
    for s in sel:
        cmds.addAttr (s, ln="SHDcolor" , nn='Shading_Color', at='float3', uac= True, k=True)
        cmds.addAttr (s, ln="SHDcolorR" , at='float', p= 'SHDcolor', k=True)
        cmds.addAttr (s, ln="SHDcolorG" , at='float', p= 'SHDcolor', k=True)
        cmds.addAttr (s, ln="SHDcolorB" , at='float', p= 'SHDcolor', k=True)

def getSet(*args):
    sel = cmds.ls(sl=True)
    for i in sel:
        cmds.select(i, add=True, hi=True)
    selection = cmds.ls(sl=True)
    cmds.select(sel)
    for i in selection:
        if 'Shape' not in i:
            if 'Body' in i:
                bodyColor=[cmds.getAttr(i+'.SHDcolorR'), cmds.getAttr(i+'.SHDcolorG'), cmds.getAttr(i+'.SHDcolorB')]
                cmds.button('Body', edit=True, bgc=bodyColor)
                cmds.floatField('Body_R', e=True, v=bodyColor[0])
                cmds.floatField('Body_G', e=True, v=bodyColor[1])
                cmds.floatField('Body_B', e=True, v=bodyColor[2])
            if 'Arms' in i:
                limbsColor=[cmds.getAttr(i+'.SHDcolorR'), cmds.getAttr(i+'.SHDcolorG'), cmds.getAttr(i+'.SHDcolorB')]
                cmds.button('Limbs', edit=True, bgc=limbsColor)
                cmds.floatField('Limbs_R', e=True, v=limbsColor[0])
                cmds.floatField('Limbs_G', e=True, v=limbsColor[1])
                cmds.floatField('Limbs_B', e=True, v=limbsColor[2])
            if 'Wheels' in i:
                wheelsColor=[cmds.getAttr(i+'.SHDcolorR'), cmds.getAttr(i+'.SHDcolorG'), cmds.getAttr(i+'.SHDcolorB')]
                cmds.button('Wheels', edit=True, bgc=wheelsColor)
                cmds.floatField('Wheels_R', e=True, v=wheelsColor[0])
                cmds.floatField('Wheels_G', e=True, v=wheelsColor[1])
                cmds.floatField('Wheels_B', e=True, v=wheelsColor[2])







def applySet(*args):
    bodyR= cmds.floatField('Body_R', q=True, v=True)
    bodyG= cmds.floatField('Body_G', q=True, v=True)
    bodyB= cmds.floatField('Body_B', q=True, v=True)
    
    limbsR= cmds.floatField('Limbs_R', q=True, v=True)
    limbsG= cmds.floatField('Limbs_G', q=True, v=True)
    limbsB= cmds.floatField('Limbs_B', q=True, v=True)
    
    wheelsR= cmds.floatField('Wheels_R', q=True, v=True)
    wheelsG= cmds.floatField('Wheels_G', q=True, v=True)
    wheelsB= cmds.floatField('Wheels_B', q=True, v=True)

    sel = cmds.ls(sl=True)
    for i in sel:
        cmds.select(i, add=True, hi=True)
    selection = cmds.ls(sl=True)
    cmds.select(sel)
    body=['Body', 'Head', 'Eyebrows', 'Eyelid']
    limbs=['Arms', 'Legs']
    wheels=['Wheels']
    colorMesh=[]
    
    for s in selection:
        for i in body+limbs+wheels:
            if i in s:
                if 'Shape' not in s:
                    colorMesh.append(s)
                    if not checkAttribute(s, 'SHDcolor'):
                        Colors([s])

    for s in colorMesh:
        for i in body:
            if i in s:
                cmds.setAttr(s+'.SHDcolorR', bodyR)
                cmds.setAttr(s+'.SHDcolorG', bodyG)
                cmds.setAttr(s+'.SHDcolorB', bodyB)
        for i in limbs:
            if i in s:
                cmds.setAttr(s+'.SHDcolorR', limbsR)
                cmds.setAttr(s+'.SHDcolorG', limbsG)
                cmds.setAttr(s+'.SHDcolorB', limbsB)
        for i in wheels:
            if i in s:
                cmds.setAttr(s+'.SHDcolorR', wheelsR)
                cmds.setAttr(s+'.SHDcolorG', wheelsG)
                cmds.setAttr(s+'.SHDcolorB', wheelsB)


def closeWindow(*args):
    # Closes UI
    cmds.deleteUI('STBTSCreateSet')

def createUI(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    projectdirs = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]
    for dirs in projectdirs:
        if '3D' in dirs:
            usersDir = pdir+dirs+'/_Useful/Scripts/users'

    
    windowprop = 'STBTSColorSet'
    
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window(windowprop, t='STBTS Color Set',w=300, h=250, sizeable=True, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('Layout')

    cmds.rowColumnLayout( numberOfColumns=1, co=(1, 'left', 25), parent='Layout')
    cmds.separator(h=20, style='none')
    cmds.text('Current Color Set')
    cmds.separator(h=10, style='none')
    cmds.setParent('..')

    ColorPickerButton.colorPickerButton('Body', [1,1,1])
    ColorPickerButton.colorPickerButton('Limbs', [1,1,1])
    ColorPickerButton.colorPickerButton('Wheels', [1,1,1])

    cmds.rowColumnLayout( numberOfColumns=1)
    cmds.separator(h=10, style='none')
    cmds.setParent('..')

    cmds.rowColumnLayout( numberOfColumns=4)
    cmds.separator(w=30, style='none')
    cmds.button('Get From CH', c=getSet, w=100, backgroundColor= [0.1,.4,.25])
    cmds.separator(w=30, style='none')
    cmds.button('Apply to CH', c=applySet, w=100, backgroundColor= [0.1,.4,.25])
    cmds.setParent('..')

    cmds.rowColumnLayout( numberOfColumns=1)
    cmds.separator(h=10, style='none')
    cmds.separator(h=10, style='out')
    cmds.separator(h=10, style='none')
    cmds.setParent('..')

    setVis()

    cmds.showWindow(windowprop)
    cmds.window(window, edit=True, w=305, h=220) 



