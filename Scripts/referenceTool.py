from maya import cmds
from functools import partial
import os

#THIS TOOL IS NOW OBSOLETE

filePath = cmds.file(q=True, sn=True) 
filename =(os.path.split(filePath)[1].split('.')[0])
filepath = (os.path.split(filePath)[0])
goodtogo=0
if '3D' in filePath:
	assetsDir = filepath.split('3D')[0]+'3D/01_Assets/'
	goodtogo=1
	types = os.listdir(assetsDir)

else :
	cmds.warning('Folder hierarchy must include a 3D folder')


def forestRefWindow(*args):
	filePath = cmds.file(q=True, sn=True) 
	filename =(os.path.split(filePath)[1].split('.')[0])
	filepath = (os.path.split(filePath)[0])
	goodtogo=0
	if '3D' in filePath:
		assetsDir = filepath.split('3D')[0]+'3D/01_Assets/'
		goodtogo=1
		types = os.listdir(assetsDir)

		if cmds.window("FRefWindow", exists=True):
			cmds.deleteUI("FRefWindow")
		window = cmds.window("FRefWindow", title= "Forest Reference Tool", w=200, h=200, sizeable=True, backgroundColor= [0.1,.2,.2])

		cmds.columnLayout('TypeMenu')
		cmds.rowColumnLayout("NamesLayout", numberOfColumns=1, columnWidth=(1,200))
		cmds.separator( height=10, style='in' )
		cmds.rowColumnLayout('typeLayout', nc=2, cw=[(1,50),(2,140)], cs=(2,10))
		cmds.text('Type', backgroundColor= [0.1,.4,.25])
		cmds.optionMenu('typeMenu',backgroundColor= [0.1,.4,.4], cc= partial(assetMenu, assetsDir))
		for type in types:
			if type!='.DS_Store':
				cmds.menuItem(type) 
		cmds.setParent('..')
		cmds.separator(h=15, style='in' )		

		cmds.showWindow(window)
		cmds.window(window, edit=True, w=200, h=170) 
	else :
		cmds.warning('Hierarchy must include a 3D folder')


def assetMenu(assetsDir, *args):
	if cmds.optionMenu('AssetMenu', exists=True):
		cmds.deleteUI('AssetMenu', 'assetLayout', 'assetSep')
	if cmds.optionMenu('JobMenu', exists=True):
		cmds.deleteUI('JobMenu','jobLayout','jobSep')
	if cmds.button('refButton', exists=True):
		cmds.deleteUI('refButton')
	cmds.rowColumnLayout('assetLayout', nc=2, cw=[(1,50),(2,140)], cs=(2,10))
	cmds.text('assetText', l='Asset', backgroundColor= [0.1,.4,.25])
	assetType = cmds.optionMenu('typeMenu', q=True, v=True)
	assets = os.listdir(assetsDir+assetType)
	cmds.optionMenu('AssetMenu', backgroundColor= [0.1,.4,.4], cc=partial(assetJob, assetType, assetsDir))
	for asset in assets:
		if asset!='.DS_Store':
			cmds.menuItem(asset) 
	cmds.setParent('..')
	cmds.separator('assetSep', h=15, style='in' )


def assetJob(assetType, assetsDir, *args):
	if cmds.optionMenu('JobMenu', exists=True):
		cmds.deleteUI('JobMenu','jobLayout','jobSep')
	if cmds.button('refButton', exists=True):
		cmds.deleteUI('refButton')
	cmds.rowColumnLayout('jobLayout', nc=2, cw=[(1,50),(2,140)], cs=(2,10))
	cmds.text('jobText', l='Job', backgroundColor= [0.1,.4,.25])
	assetName = cmds.optionMenu('AssetMenu', q=True, v=True)
	jobs = os.listdir(assetsDir+assetType+'/'+assetName)
	cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(assetPub, assetName, assetType, assetsDir))
	for job in jobs:
		if job!='.DS_Store':
			cmds.menuItem(job) 
	cmds.setParent('..')
	cmds.separator('jobSep', h=15, style='in' )


def assetPub(assetName, assetType, assetsDir, *args):
	if cmds.optionMenu('JobMenu', exists=True):
		cmds.deleteUI('JobMenu','jobLayout','jobSep')
	if cmds.button('refButton', exists=True):
		cmds.deleteUI('refButton')
	cmds.rowColumnLayout('jobLayout', nc=2, cw=[(1,50),(2,140)], cs=(2,10))
	cmds.text('jobText', l='Job', backgroundColor= [0.1,.4,.25])
	assetName = cmds.optionMenu('AssetMenu', q=True, v=True)
	jobs = os.listdir(assetsDir+assetType+'/'+assetName)
	cmds.optionMenu('JobMenu', backgroundColor= [0.1,.4,.4], cc=partial(refButton, pub, assetName, assetType, assetsDir))
	for job in jobs:
		if job!='.DS_Store':
			cmds.menuItem(job) 
	cmds.setParent('..')
	cmds.separator('jobSep', h=15, style='in' )


def refButton(pub, assetName, assetType, assetsDir, *args):
	if cmds.button('refButton', exists=True):
		cmds.deleteUI('refButton')
	jobType = cmds.optionMenu('JobMenu', q=True, v=True)
	filepath = assetsDir+assetType+'/'+assetName+'/'+jobType+ '/_PUB/'+pub
	cmds.button('refButton', l='Reference', w=200, h=35,  backgroundColor= [0.4,.1,.25], c=partial(importRef, filepath, assetName, assetType, assetsDir, jobType))

def importRef(filepath, assetName, assetType, assetsDir, jobType, *args):
	pub='noPub'
	if cmds.file(filepath, q=True, ex=True):
		for file in files:
			if file.endswith('_PUB.mb'):
				cmds.file(filepath+file, r=True, ns=file[:-3])
				pub= file
		if pub=='noPub':
				cmds.warning('Requested PUB does not exist.')