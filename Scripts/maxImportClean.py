import pymel.core as pm
import maya.cmds as cmds 
import maya.mel as mel

def mergeUVSets(*args):
    sel=cmds.ls(sl=True)
    for i in sel:
        uvSets=cmds.polyUVSet(i, q=True, auv=True)
        if uvSets!=[]:
            for set in uvSets:
                if set!='map1': 
                    cmds.polyCopyUV(i, uvi=set, uvs='map1')
                    cmds.polyUVSet(i, delete=True, uvSet=set)

def delCustomAttr(*args):
    for n in sel:
        for a in n.listAttr(ud=True):
            a.unlock()
            pm.deleteAttr(a)

def delNS(*args):
    namespaces = []
    for ns in pymel.listNamespaces( recursive  =True, internal =False):
        namespaces.append(ns)

    for ns in reversed(namespaces):
        currentSpace = ns
        pymel.namespace(removeNamespace = ns, mergeNamespaceWithRoot = True)

    namespaces[:] = [] 

def delUnused(*args):
    mel.eval('MLdeleteUnused;')

def maxImportClean(*args):
    mergeUVSets()
    delCustomAttr()
    delNS()
    delUnused()