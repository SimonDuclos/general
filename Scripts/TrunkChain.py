import pymel.core as pm
import orig
import maya.cmds as cmds
import ctlSphere

def createFolicule(name, surf, paramU, paramV):
    folicle=cmds.createNode("follicle")
    folicleTrans=cmds.listRelatives(folicle,type='transform',p=True)
    folicleTrans=cmds.rename( folicleTrans, name+'_FOL')
    folicle=name+'_FOLShape'
    cmds.connectAttr(folicle + ".outRotate", folicleTrans + ".rotate")
    cmds.connectAttr(folicle + ".outTranslate", folicleTrans+ ".translate")
    cmds.connectAttr(surf+".worldSpace",folicle+".inputSurface")
    cmds.setAttr(folicle + ".simulationMethod", 0)
    cmds.setAttr(folicle+'.parameterU', paramU)
    cmds.setAttr(folicle+'.parameterV', paramV)
    cmds.hide(folicleTrans+'Shape')
    return folicleTrans

def createRib(name, nbr_jnt, *args):
    
    nbr_cv = nbr_jnt
    if nbr_cv%2==0: 
        nbr_cv += 1
        
    size = 10
    
    lr = .1
    
    coef = size/(nbr_cv-1)
    
    cv_mid = (nbr_cv-1)/2
    
    cv_max = nbr_cv-1
    
    nbr_cv_min = [0, cv_mid ,cv_max ]
    
    ctls=[]
    
    for i in xrange(nbr_cv):
        ctls.append(i)
    
    secctl = [item for item in ctls if item not in nbr_cv_min]
       

    surfsec = cmds.nurbsPlane(n=name+'_Surf_NSURF', degree=1, w=size, lr=lr,  ax=[0,1,0], u=nbr_cv-1)

def createJoints(name, nbr_jnt, *args):

    nbr_cv = nbr_jnt
    if nbr_cv%2==0: 
        nbr_cv += 1
        
    size = 10
    
    cv_mid = (nbr_cv-1)/2
    
    cv_max = nbr_cv-1

    P = -(size/2)
    inc=float(size)/(nbr_cv-1)

    maingrp= cmds.group(em=True, n=name+'_Main_GRP')
    skinSel = []
    for i in xrange(nbr_cv):
        j=cmds.joint(n=name+'_'+str(i+1).zfill(3)+'_JNT', p=(P, 0, 0), rad=float(size)/50 )
        cmds.addAttr(j, shortName='UPostion', longName='U_Position', at='float', dv=float(i)/(nbr_cv-1),  minValue=float(i)/(nbr_cv-1), maxValue=float(i)/(nbr_cv-1), k=True)
        skinSel.append(j)

        group_Tr1 = name+'_'+str(i+1).zfill(3)+'_Pos_GRP'
        group_rot1 = name+'_'+str(i+1).zfill(3)+'_rot1_GRP'
        group_rot2 = name+'_'+str(i+1).zfill(3)+'_rot2_GRP'
        group_rot3 = name+'_'+str(i+1).zfill(3)+'_rot3_GRP'
        pos1 = cmds.group(em=True, n=group_Tr1)
        rot1 = cmds.group(em=True, n=group_rot1)
        rot2 = cmds.group(em=True, n=group_rot2)
        rot3 = cmds.group(em=True, n=group_rot3)
        cmds.xform(pos1, t=(P, 0, 0), ws=True)
        cmds.xform(rot1, t=(P, 0, 0), ws=True)
        cmds.xform(rot2, t=(P, 0, 0), ws=True)
        cmds.xform(rot3, t=(P, 0, 0), ws=True)
        #if trunk rig then use this
        #if i!=0:
        #    cmds.parent(pos1, name+'_'+str(i).zfill(3)+'_rot3_GRP' )
        
        #if transform/scale rig then use this
        cmds.parent(pos1, maingrp)

        cmds.parent(rot1, pos1)
        cmds.parent(rot2, rot1)
        cmds.parent(rot3, rot2)
        cmds.parent(j, rot3)
        
        cu1=name+'_Main_001_CTL.UPostion'
        ## CONTROLER1
        #Condition for aheahd or behind CTL
        cdt1=cmds.createNode('condition', n=j+'_1relPos')
        cmds.connectAttr(j+'.UPostion', cdt1+'.firstTerm')
        cmds.connectAttr(name+'_Main_001_CTL.UPostion', cdt1+'.secondTerm')
        cmds.connectAttr('*1_CTL_falloffPos.output1D', cdt1+'.colorIfFalseR')
        cmds.connectAttr('*1_CTL_CtlMinFallof.output1D', cdt1+'.colorIfFalseG')

        cmds.connectAttr('*1_CTL_falloffPPos.output1D', cdt1+'.colorIfTrueR')
        cmds.connectAttr('*1_CTL_CtlPlusFallof.output1D', cdt1+'.colorIfTrueG')
        cmds.setAttr(cdt1 +'.operation', 2)

        jntFpos=cmds.createNode( 'plusMinusAverage', n=j+'_1falloffPos')
        cmds.connectAttr(j+'.UPostion', j+'_1falloffPos.input1D[0]')
        cmds.connectAttr(cdt1+'.outColorR', j+'_1falloffPos.input1D[1]')
        cmds.setAttr(jntFpos +'.operation', 2)

        rot1mult= cmds.createNode('multiplyDivide', n=j+'_Rot1Mult')
        cmds.connectAttr(jntFpos+'.output1D', j+'_Rot1Mult.input1X')
        cmds.connectAttr(cdt1+'.outColorG', j+'_Rot1Mult.input2X')
        cmds.setAttr(rot1mult +'.operation', 2)

        falloffrot1 = cmds.createNode('multiplyDivide', n=j+'_FalloffRot1')
        cmds.connectAttr(rot1mult+'.outputX', j+'_FalloffRot1.input1X')
        cmds.connectAttr('*1_CTL_AffectedJointsMult1.outputX', j+'_FalloffRot1.input2X')

        clamp1 = cmds.createNode('clamp', n=j+'_clampMult1')
        cmds.connectAttr(falloffrot1+'.outputX', clamp1+'.inputR')
        cmds.setAttr(clamp1+'.maxR', 1)

        #to use for tr and scale, not the trunk rig
        clampUndiv1 = cmds.createNode('clamp', n=j+'_clampUndivMult1')
        cmds.connectAttr(rot1mult+'.outputX', clampUndiv1+'.inputR')
        cmds.setAttr(clampUndiv1+'.maxR', 1)

        interpUndiv1 = cmds.createNode('remapValue', n=j+'_InterpUndiv1')
        cmds.connectAttr(clampUndiv1+'.outputR', interpUndiv1+'.inputValue')
        cmds.setAttr(interpUndiv1+'.value[0].value_Interp', 2)


        jrot1= cmds.createNode('multiplyDivide', n=j+'_Rot1')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(clamp1+'.outputR', jrot1+'.input1'+a)
            cmds.connectAttr('*1_CTL.rotate'+a, jrot1+'.input2'+a)
            cmds.connectAttr(jrot1+'.output'+a, rot1+'.rotate'+a)

        jscale1= cmds.createNode('multiplyDivide', n=j+'_Scale1')
        
        ax='XYZ'
        for a in ax:
            jscaleRel=cmds.createNode('plusMinusAverage', n=j+a+'_RelScale1')
            cmds.connectAttr('*1_CTL.scale'+a,  jscaleRel+'.input1D[0]')
            cmds.setAttr(jscaleRel+'.input1D[1]',1)
            cmds.setAttr(jscaleRel +'.operation', 2)

            cmds.connectAttr(interpUndiv1+'.outValue', jscale1+'.input1'+a)
            cmds.connectAttr(jscaleRel+'.output1D', jscale1+'.input2'+a)
            
            jscaleNew=cmds.createNode('plusMinusAverage', n=j+a+'_NewScale1')
            cmds.connectAttr(jscale1+'.output'+a,  jscaleNew+'.input1D[0]')
            cmds.setAttr(jscaleNew+'.input1D[1]',1)

            cmds.connectAttr(jscaleNew+'.output1D', rot1+'.scale'+a)

        jTr1= cmds.createNode('multiplyDivide', n=j+'_Transform1')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(interpUndiv1+'.outValue', jTr1+'.input1'+a)
            cmds.connectAttr('*1_CTL.translate'+a, jTr1+'.input2'+a)
            cmds.connectAttr(jTr1+'.output'+a, rot1+'.translate'+a)

        ## CONTROLER2
        #Condition for aheahd or behind CTL
        cdt2=cmds.createNode('condition', n=j+'_2relPos')
        cmds.connectAttr(j+'.UPostion', cdt2+'.firstTerm')
        cmds.connectAttr(name+'_Main_002_CTL.UPostion', cdt2+'.secondTerm')
        cmds.connectAttr('*2_CTL_falloffPos.output1D', cdt2+'.colorIfFalseR')
        cmds.connectAttr('*2_CTL_CtlMinFallof.output1D', cdt2+'.colorIfFalseG')

        cmds.connectAttr('*2_CTL_falloffPPos.output1D', cdt2+'.colorIfTrueR')
        cmds.connectAttr('*2_CTL_CtlPlusFallof.output1D', cdt2+'.colorIfTrueG')
        cmds.setAttr(cdt2 +'.operation', 2)

        jnt2Fpos=cmds.createNode( 'plusMinusAverage', n=j+'_2falloffPos')
        cmds.connectAttr(j+'.UPostion', j+'_2falloffPos.input1D[0]')
        cmds.connectAttr(cdt2+'.outColorR', j+'_2falloffPos.input1D[1]')
        cmds.setAttr(jnt2Fpos +'.operation', 2)

        rot2mult= cmds.createNode('multiplyDivide', n=j+'_Rot2Mult')
        cmds.connectAttr(jnt2Fpos+'.output1D', j+'_Rot2Mult.input1X')
        cmds.connectAttr(cdt2+'.outColorG', j+'_Rot2Mult.input2X')
        cmds.setAttr(rot2mult +'.operation', 2)

        falloffrot2 = cmds.createNode('multiplyDivide', n=j+'_FalloffRot2')
        cmds.connectAttr(rot2mult+'.outputX', j+'_FalloffRot2.input1X')
        cmds.connectAttr('*2_CTL_AffectedJointsMult1.outputX', j+'_FalloffRot2.input2X')

        clamp2 = cmds.createNode('clamp', n=j+'_clampMult2')
        cmds.connectAttr(falloffrot2+'.outputX', clamp2+'.inputR')
        cmds.setAttr(clamp2+'.maxR', 1)

        #to use for tr and scale, not the trunk rig
        clampUndiv2 = cmds.createNode('clamp', n=j+'_clampUndivMult2')
        cmds.connectAttr(rot2mult+'.outputX', clampUndiv2+'.inputR')
        cmds.setAttr(clampUndiv2+'.maxR', 1)

        interpUndiv2 = cmds.createNode('remapValue', n=j+'_InterpUndiv2')
        cmds.connectAttr(clampUndiv2+'.outputR', interpUndiv2+'.inputValue')
        cmds.setAttr(interpUndiv2+'.value[0].value_Interp', 2)


        jrot2= cmds.createNode('multiplyDivide', n=j+'_Rot2')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(clamp2+'.outputR', jrot2+'.input1'+a)
            cmds.connectAttr('*2_CTL.rotate'+a, jrot2+'.input2'+a)
            cmds.connectAttr(jrot2+'.output'+a, rot2+'.rotate'+a)

        jscale2= cmds.createNode('multiplyDivide', n=j+'_Scale2')
        
        ax='XYZ'
        for a in ax:
            jscale2Rel=cmds.createNode('plusMinusAverage', n=j+a+'_RelScale2')
            cmds.connectAttr('*2_CTL.scale'+a,  jscale2Rel+'.input1D[0]')
            cmds.setAttr(jscale2Rel+'.input1D[1]',1)
            cmds.setAttr(jscale2Rel +'.operation', 2)

            cmds.connectAttr(interpUndiv2+'.outValue', jscale2+'.input1'+a)
            cmds.connectAttr(jscale2Rel+'.output1D', jscale2+'.input2'+a)
            
            jscale2New=cmds.createNode('plusMinusAverage', n=j+a+'_NewScale2')
            cmds.connectAttr(jscale2+'.output'+a,  jscale2New+'.input1D[0]')
            cmds.setAttr(jscale2New+'.input1D[1]',1)

            cmds.connectAttr(jscale2New+'.output1D', rot2+'.scale'+a)

        jTr2= cmds.createNode('multiplyDivide', n=j+'_Transform2')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(interpUndiv2+'.outValue', jTr2+'.input1'+a)
            cmds.connectAttr('*2_CTL.translate'+a, jTr2+'.input2'+a)
            cmds.connectAttr(jTr2+'.output'+a, rot2+'.translate'+a)
 
         ## CONTROLER3
        #Condition for aheahd or behind CTL
        cdt3=cmds.createNode('condition', n=j+'_3relPos')
        cmds.connectAttr(j+'.UPostion', cdt3+'.firstTerm')
        cmds.connectAttr(name+'_Main_003_CTL.UPostion', cdt3+'.secondTerm')
        cmds.connectAttr('*3_CTL_falloffPos.output1D', cdt3+'.colorIfFalseR')
        cmds.connectAttr('*3_CTL_CtlMinFallof.output1D', cdt3+'.colorIfFalseG')

        cmds.connectAttr('*3_CTL_falloffPPos.output1D', cdt3+'.colorIfTrueR')
        cmds.connectAttr('*3_CTL_CtlPlusFallof.output1D', cdt3+'.colorIfTrueG')
        cmds.setAttr(cdt3 +'.operation', 2)

        jnt3Fpos=cmds.createNode( 'plusMinusAverage', n=j+'_3falloffPos')
        cmds.connectAttr(j+'.UPostion', j+'_3falloffPos.input1D[0]')
        cmds.connectAttr(cdt3+'.outColorR', j+'_3falloffPos.input1D[1]')
        cmds.setAttr(jnt3Fpos +'.operation', 2)

        rot3mult= cmds.createNode('multiplyDivide', n=j+'_Rot3Mult')
        cmds.connectAttr(jnt3Fpos+'.output1D', j+'_Rot3Mult.input1X')
        cmds.connectAttr(cdt3+'.outColorG', j+'_Rot3Mult.input2X')
        cmds.setAttr(rot3mult +'.operation', 2)

        falloffrot3 = cmds.createNode('multiplyDivide', n=j+'_FalloffRot3')
        cmds.connectAttr(rot3mult+'.outputX', j+'_FalloffRot3.input1X')
        cmds.connectAttr('*3_CTL_AffectedJointsMult1.outputX', j+'_FalloffRot3.input2X')

        clamp3 = cmds.createNode('clamp', n=j+'_clampMult3')
        cmds.connectAttr(falloffrot3+'.outputX', clamp3+'.inputR')
        cmds.setAttr(clamp3+'.maxR', 1)

        #to use for tr and scale, not the trunk rig
        clampUndiv3 = cmds.createNode('clamp', n=j+'_clampUndivMult3')
        cmds.connectAttr(rot3mult+'.outputX', clampUndiv3+'.inputR')
        cmds.setAttr(clampUndiv3+'.maxR', 1)

        interpUndiv3 = cmds.createNode('remapValue', n=j+'_InterpUndiv3')
        cmds.connectAttr(clampUndiv3+'.outputR', interpUndiv3+'.inputValue')
        cmds.setAttr(interpUndiv3+'.value[0].value_Interp', 2)


        jrot3= cmds.createNode('multiplyDivide', n=j+'_Rot3')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(clamp3+'.outputR', jrot3+'.input1'+a)
            cmds.connectAttr('*3_CTL.rotate'+a, jrot3+'.input2'+a)
            cmds.connectAttr(jrot3+'.output'+a, rot3+'.rotate'+a)

        jscale3= cmds.createNode('multiplyDivide', n=j+'_Scale3')
        
        ax='XYZ'
        for a in ax:
            jscale3Rel=cmds.createNode('plusMinusAverage', n=j+a+'_RelScale3')
            cmds.connectAttr('*3_CTL.scale'+a,  jscale3Rel+'.input1D[0]')
            cmds.setAttr(jscale3Rel+'.input1D[1]',1)
            cmds.setAttr(jscale3Rel +'.operation', 2)

            cmds.connectAttr(interpUndiv3+'.outValue', jscale3+'.input1'+a)
            cmds.connectAttr(jscale3Rel+'.output1D', jscale3+'.input2'+a)
            
            jscale3New=cmds.createNode('plusMinusAverage', n=j+a+'_NewScale3')
            cmds.connectAttr(jscale3+'.output'+a,  jscale3New+'.input1D[0]')
            cmds.setAttr(jscale3New+'.input1D[1]',1)

            cmds.connectAttr(jscale3New+'.output1D', rot3+'.scale'+a)

        jTr3= cmds.createNode('multiplyDivide', n=j+'_Transform3')
        ax='XYZ'
        for a in ax:
            cmds.connectAttr(interpUndiv3+'.outValue', jTr3+'.input1'+a)
            cmds.connectAttr('*3_CTL.translate'+a, jTr3+'.input2'+a)
            cmds.connectAttr(jTr3+'.output'+a, rot3+'.translate'+a)       

        P+=inc


    skinSel.append(name+'_Surf_NSURF')
    cmds.skinCluster(skinSel, dr=0.1, mi=1)




nbr_jnt=20
nbr_cv= nbr_jnt
name='Chain'
createRib(name, nbr_jnt)

uctl = [.25,.5,.75] 
x=1
mCtlSize=.3
for i in uctl:
    f=createFolicule(name+str(x).zfill(3), name+'_Surf_NSURF',i,.5)
    
    a = ctlSphere.ctlsphere(name+'_Main_{}_CTL'.format(str(x).zfill(3)), mCtlSize)
    ctl=a[0]
    ctlshape= a[1]
    
    cmds.addAttr(ctl,  ln="globaldivider_01", nn=" ", at="enum" , en="---------------:" , k=True)
    
    #add custom attribute to control the position of the follicle on the surface    
    cmds.addAttr(ctl, shortName='UPostion', longName='U_Position', at='float', minValue=0, maxValue=1)
    cmds.setAttr(ctl+'.UPostion', k=True)
    cmds.setAttr(ctl+'.UPostion', i)
    cmds.connectAttr(ctl+'.UPostion', f+'Shape'+'.parameterU')

    cmds.addAttr(ctl, shortName='Falloff', longName='Falloff', at='float', minValue=0, maxValue=1)
    cmds.setAttr(ctl+'.Falloff', k=True)
    cmds.setAttr(ctl+'.Falloff', .2)

    cmds.addAttr(ctl, shortName='AffectedJoints', longName='Affected_Joints', at='float', minValue=0, k=True)
    
    #neg falloff pos
    pmaFposN1=cmds.createNode( 'plusMinusAverage', n=ctl+'_falloffPos')
    cmds.connectAttr(ctl+'.UPostion', ctl+'_falloffPos.input1D[0]')
    cmds.connectAttr(ctl+'.Falloff', ctl+'_falloffPos.input1D[1]')
    cmds.setAttr(pmaFposN1 +'.operation', 2)

    #ctrl pos- neg falloff pos
    pmaCMF1=cmds.createNode( 'plusMinusAverage', n=ctl+'_CtlMinFallof')
    cmds.connectAttr(ctl+'.UPostion', ctl+'_CtlMinFallof.input1D[0]')
    cmds.connectAttr(pmaFposN1+'.output1D', ctl+'_CtlMinFallof.input1D[1]')
    cmds.setAttr(pmaCMF1 +'.operation', 2)

    #positiv falloff pos
    pmaFposP1=cmds.createNode( 'plusMinusAverage', n=ctl+'_falloffPPos')
    cmds.connectAttr(ctl+'.UPostion', ctl+'_falloffPPos.input1D[0]')
    cmds.connectAttr(ctl+'.Falloff', ctl+'_falloffPPos.input1D[1]')
    cmds.setAttr(pmaFposP1 +'.operation', 1)

    #ctrl pos- pos falloff pos
    pmaCPF1=cmds.createNode( 'plusMinusAverage', n=ctl+'_CtlPlusFallof')
    cmds.connectAttr(ctl+'.UPostion', ctl+'_CtlPlusFallof.input1D[0]')
    cmds.connectAttr(pmaFposP1+'.output1D', ctl+'_CtlPlusFallof.input1D[1]')
    cmds.setAttr(pmaCPF1 +'.operation', 2)

    #affected joints
    afjnt=cmds.createNode( 'multiplyDivide', n=ctl+'_AffectedJoints1')
    cmds.setAttr(afjnt+'.input1X', float(nbr_cv))
    cmds.connectAttr(ctl+'.Falloff' , afjnt+'.input2X')
    
    cmds.connectAttr(afjnt+'.outputX', ctl+'.AffectedJoints')

    afjntMult=cmds.createNode( 'multiplyDivide', n=ctl+'_AffectedJointsMult1')
    cmds.setAttr(afjntMult+'.input1X', 1)
    cmds.connectAttr(afjnt+'.outputX' , afjntMult+'.input2X')
    cmds.setAttr(afjntMult +'.operation', 2)


    orig.orig([ctl])
    driver = f
    driven = ctl.replace('CTL', 'GRP')
    print driver
    print driven
    cmds.delete(cmds.pointConstraint(driver, driven, maintainOffset = False))

    cmds.parentConstraint(driver, driven, mo=True)
    #for ax in 'XYZ':
    #    cmds.connectAttr('{}.{}'.format(driver, 'translate' + ax), '{}.{}'.format(driven, 'translate' + ax))
    #    cmds.connectAttr('{}.{}'.format(driver, 'rotate' + ax), '{}.{}'.format(driven, 'rotate' + ax))
    x+=1

createJoints(name, nbr_jnt)