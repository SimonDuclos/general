import maya.cmds as cmds
import os
from functools import partial

widgets={}

def chTab(*args):
	if cmds.window("chTab", exists=True):
		cmds.deleteUI("chTab")

	widgets["chWindow"]= cmds.window("chTab", title="Character Monitor", w=400, h=600, backgroundColor= [0.1,.2,.2])
	mainLayout=cmds.columnLayout(w=400,h=600)

	widgets["tabLayout"]=cmds.tabLayout(imw=5, imh=5)

	widgets[("Create_tab")]=cmds.columnLayout(w=400, h=600, parent=widgets[("tabLayout")])
	cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=2, columnWidth=[(1,180), (2,180)], columnSpacing=[(1,10), (2,10)])
	cmds.text('Import under a specific Namespace')
	cmds.textField('nstext')
	cmds.separator(h=10, style='none')
	cmds.setParent('..')
	cmds.rowColumnLayout('Refbutton', numberOfColumns=1, columnWidth=(1,200), columnAlign=(1,'center'), columnSpacing=(1,95))
	cmds.button('Create Reference', h=40, backgroundColor= [0.1,.4,.25],c=createRef)
	
	cmds.separator(h=10, style='none')
	cmds.setParent('..')
	cmds.rowColumnLayout("RefLayout", numberOfColumns=3, columnWidth=[(1,125), (2,125), (3,125)], columnSpacing=[(1,2), (2,5),(3,5)])
	cmds.button('JOSEE',h=30, backgroundColor= [0.4,.1,.25])
	cmds.button('POUSSE', h=30, backgroundColor= [0.4,.1,.25])
	cmds.button('DEER', h=30, backgroundColor= [0.4,.1,.25])
	cmds.tabLayout(widgets["tabLayout"], edit=True, tabLabel=(widgets[("Create_tab")], "Create Ref"))


	namespaces = cmds.namespaceInfo(listOnlyNamespaces=True)
	if namespaces ==[u'UI', u'shared']:
		cmds.warning('No Available Asset in Scene')
	else:
		findNamespaces()

	cmds.showWindow(widgets["chWindow"])

def createRef(*args):
	name = cmds.textField('nstext', q=True, tx=True)
	RefDir = cmds.fileDialog2(dialogStyle=2, fileMode =1) [0]
	if name!='':
		cmds.file(RefDir, r=True, namespace=name)
	else:
		filename=(os.path.split(RefDir)[1].split('.')[0])
		cmds.file(RefDir, r=True, namespace=filename)
	chTab()

def ctlSelect(v, *args):
	cmds.select(v+":*"+"CTL"+"*")

def masterSelect(v, *args):
	cmds.select(v+":*"+"Master*"+"CTL"+"*")

def findNamespaces(*args):
	cmds.namespace(setNamespace=":")
	namespaces = cmds.namespaceInfo(listOnlyNamespaces=True)
	namelist=[]
	for namespace in namespaces:
		if cmds.objExists(namespace+":*"):
			ns= namespace.split("_")
			if len(ns)>=3:
				if ns[2]== "RIG":
					name=ns[1]
					
					if name in namelist:
						templist=namelist
						for temp in templist:
							if temp!=name:
								templist.remove(temp)
						it = len(templist)
						name= name+str(it).zfill(3)
					namelist.append(ns[1])

					widgets[(namespace+"_tab")]=cmds.columnLayout(w=400, h=600, parent=widgets[("tabLayout")])
					
					cmds.separator(h=10, style='none')
					cmds.rowColumnLayout("ButtonsLayout", numberOfColumns=2, columnWidth=[(1,180), (2,180)], columnSpacing=[(1,10), (2,10)])
					widgets[(namespace)+"_Master_Button"]=cmds.button("Select Master", backgroundColor= [0.1,.4,.25], c=partial(masterSelect, namespace))
					widgets[(namespace)+"_CTLs_Button"]=cmds.button("Select all CTLs", backgroundColor= [0.1,.4,.25], c=partial(ctlSelect, namespace))


					cmds.setParent('..')
					cmds.separator(h=10, style='none')
					cmds.separator(h=10, style='none')
					cmds.text('Toggle Rig Visibility')
					widgets[(namespace)+"_attrEOM_RigVisibility"]= cmds.attrControlGrp(attribute=(namespace+':*'+'Master*'+'CTL.visibility'))
					cmds.text('Toggle Mesh Visibility')
					widgets[(namespace)+"_attrEOM_ModVisibility"]= cmds.attrControlGrp(attribute=(namespace+':*'+'MOD.visibility'))
					cmds.separator(h=10, style='none')

					cmds.text('Master Channels')
					widgets[(namespace)+"_attrFG_T"]= cmds.attrFieldGrp(attribute=(namespace+':*'+'Master*'+'CTL*'+'.translate'))
					widgets[(namespace)+"_attrFG_R"]= cmds.attrFieldGrp(attribute=(namespace+':*'+'Master*'+'CTL*'+'.rotate'))
					widgets[(namespace)+"_attrFG_S"]= cmds.attrFieldGrp(attribute=(namespace+':*'+'Master*'+'CTL*'+'.scale'))


					cmds.tabLayout(widgets["tabLayout"], edit=True, tabLabel=(widgets[(namespace+"_tab")], name))

