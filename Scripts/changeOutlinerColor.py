import maya.cmds as cmds

import maya.mel as mel
cmds.select(ado=True, hi=True)
sel = cmds.ls(sl=True , fl = True, showType = True, dagObjects = True)

for obj in sel:
    if 'R_' in (obj[2:]):
        cmds.setAttr (obj + '.outlinerColor' , 0,0,1)
        cmds.setAttr (obj + '.useOutlinerColor' , True)
    if 'L_' in (obj[2:]):
        cmds.setAttr (obj + '.outlinerColor' , 1,0,0)
        cmds.setAttr (obj + '.useOutlinerColor' , True)
