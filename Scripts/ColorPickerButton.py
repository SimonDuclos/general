import maya.cmds as cmds 
from functools import partial

def applyColor(name, color, *args):
    color=[cmds.floatField(name+'_R', q=True, v=1), cmds.floatField(name+'_G', q=True, v=1), cmds.floatField(name+'_B', q=True, v=1)]
    tempVal = cmds.colorEditor(rgb=color)
    tempColors = [float(x) for x in tempVal.split()]
    colorVal = tempColors[0:3]
    if tempColors[-1]:
         cmds.button(name, edit=True, bgc=colorVal)
         cmds.floatField(name+'_R', e=True, v=colorVal[0])
         cmds.floatField(name+'_G', e=True, v=colorVal[1])
         cmds.floatField(name+'_B', e=True, v=colorVal[2])



def colorPickerButton(name, color, *args):   
    cmds.rowColumnLayout(name+'ButtonLayout', numberOfColumns=6)
    cmds.separator(w=20, style='none')
    cmds.button(name, bgc=color, c=partial(applyColor,name), w=75)
    cmds.separator(w=20, style='none')
    cmds.floatField(name+'_R', v=color[0], w=60)
    cmds.floatField(name+'_G', v=color[1], w=60)
    cmds.floatField(name+'_B', v=color[2], w=60)
    cmds.setParent('..')


