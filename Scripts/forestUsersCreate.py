import maya.cmds as cmds 
import os
import os.path
from os import path
from functools import partial
import users 
import importlib


def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def createUser(userDir, *args):
    userName = cmds.textField('fullName', q=True, text=True)
    initials = cmds.textField('Initials', q=True, text=True)
    directory=userDir+'/'+userName+'_'+initials
    make_dir(directory)
    userVis()
 
def userVis(*args):
    reload(users)
    if cmds.rowColumnLayout( 'userListLayout', exists=True):
        cmds.deleteUI('userListLayout', 'exitSep', 'exitLayout')
    cmds.rowColumnLayout( 'userListLayout', numberOfColumns=2, columnWidth=[ (1,150), (2, 150)], parent='Layout')
    cmds.text(label = 'User Name')
    cmds.text(label = 'User Initials')
    cmds.separator( h=5, style='out')
    cmds.separator( h=5, style='out')
    for user in (users.users):
        cmds.separator( h=10, style='none')
        cmds.separator( h=10, style='none')
        cmds.text(label = user[0])
        cmds.text(label = user[1])
    cmds.setParent('..')
    cmds.separator('exitSep', h=10, style='none')
    cmds.rowColumnLayout( 'exitLayout', numberOfColumns=1, columnWidth=[ (1,300)], parent='Layout')
    cmds.button('close', label = 'Close Window', h=30, backgroundColor= [0.4,.1,.25], command = closeWindow)




def closeWindow(*args):
    # Closes UI
    cmds.deleteUI('ForestCreateUser')

def createUI(*args):
    pdir= cmds.workspace(q=True, rd=True) 
    projectdirs = [f for f in os.listdir(pdir) if os.path.isdir(os.path.join(pdir, f))]
    for dirs in projectdirs:
        if '3D' in dirs:
            usersDir = pdir+dirs+'/_Useful/Scripts/users'

    
    windowprop = 'ForestCreateUser'
    
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window(windowprop, t='Forest Create User',w=300, h=250, sizeable=True, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout('Layout')
    cmds.rowColumnLayout( numberOfColumns=1, columnWidth=[ (1,300) ], parent='Layout')
    cmds.text('Create User', al='center', font="boldLabelFont", h=40, w=100)
    cmds.rowColumnLayout(numberOfColumns=2, columnWidth=[(1, 150), (2, 150)], parent='Layout')
    cmds.text('  Full Name:', al='left', font="obliqueLabelFont")
    cmds.textField('fullName', text='fullName', width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')
    cmds.text('  Initials (AZ):', al='left', font="obliqueLabelFont")
    cmds.textField('Initials', text='AZ', width=150, editable=True, backgroundColor= [0.05,.15,.15])
    cmds.separator(h=15, style='none')
    cmds.separator(h=15, style='none')

    cmds.rowColumnLayout(numberOfColumns=1, columnWidth=[(1, 300)], parent='Layout')
    cmds.button(label = 'Create User', h=30, backgroundColor= [0.1,.4,.25], command = partial(createUser, usersDir))
    cmds.separator(h=15, style='none')
    userVis()
    cmds.showWindow(windowprop)
    cmds.window(window, edit=True, w=305, h=220) 



