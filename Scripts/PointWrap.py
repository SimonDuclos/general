import maya.cmds as cmds

def groupStructure(*args):
	if cmds.objExists('Renderables_GRP')==0:
		cmds.group(n='Renderables_GRP', em=True)
		cmds.group(n='Renderables_CH_GRP', p='Renderables_GRP', em=True)
		cmds.group(n='Renderables_PR_GRP', p='Renderables_GRP', em=True)



def pointWrap(*args):
	sel = cmds.ls(sl=True)
	for i in sel:
		createAnimationDup(i)


def createAnimationDup(grp, *args):
	dup = cmds.duplicate(grp, n=grp.replace('_GRP', '_GRD'))

	cmds.select(dup, hi=True)
	selection = cmds.ls(sl=True)
	for i in selection:
		if 'GRD' not in i:
			if '_GRP' not in i:
				cmds.rename(i, i[:-4]+'_RDR')
			else:
				cmds.rename(i, i[:-4]+'_GDR')

	cmds.select(grp, hi=True)
	
	selection = cmds.ls(sl=True)
	selection.remove(selection[0])
	for mesh in selection:
		if 'Shape' not in mesh:
			if '|' in mesh:
				meshname=mesh.split('|')[1:]
				if '_GRP' not in meshname:
					dup=(meshname[0].split(':')[1])[:-4]+'_RDR'
				else:
					dup=(meshname[0].split(':')[1])[:-4]+'_GDR'
			else:
				if '_GRP' not in mesh:
					dup=(mesh.split(':')[1])[:-4]+'_RDR'
				else:
					dup=(mesh.split(':')[1])[:-4]+'_GDR'
			try: 
				cmds.connectAttr(mesh+'.visibility', dup+'.visibility')
			except RuntimeError:
				cmds.warning(dup+' visibility already has an incomming connection, cannot link '+mesh)


	meshes=[]
	for i in selection:
		if '_MOD' in i:
			if 'Shape' not in i:        
				meshes.append(i)

	for mesh in meshes:
		if '|' in mesh:
			meshname=mesh.split('|')[1:]
			dup= (meshname[0].split(':')[1])[:-4]+'_RDR'
		else:
			dup= (mesh.split(':')[1])[:-4]+'_RDR'
		cmds.select(dup, mesh)
		cmds.CreateWrap()
		cmds.setAttr(mesh+'.castsShadows', 1)
		cmds.setAttr(mesh+'.receiveShadows', 1)
		cmds.setAttr(mesh+'.motionBlur', 1)
		cmds.setAttr(mesh+'.primaryVisibility', 1)
		cmds.setAttr(mesh+'.visibleInReflections', 1)
		cmds.setAttr(mesh+'.visibleInRefractions', 1)