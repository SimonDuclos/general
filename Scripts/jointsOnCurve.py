import maya.cmds as cmds

# array of selected objects
sel = cmds.ls( selection=True )

# name of the curve to convert
lastSelect = int(len(sel)) -1
curveSel = sel[lastSelect]



#find the number of CVs (Degree + Spans)
numCVs = int(cmds.getAttr (curveSel+'.degree'))+ (cmds.getAttr (curveSel+'.spans'))
#print numCVs

# selection must be clear
cmds.select(clear=True)

#For each CV in the Curve create a joint.

i=0
while 1:
    if not ( i < numCVs ):
        break
    cvPosition = (cmds.getAttr (curveSel+'.cv['+(str(i))+']'))
    if (i == 0):
    	cmds.joint(n=curveSel+'_jointRoot',p = cvPosition[0])
    if (i == (numCVs-1)):
    	cmds.joint(n=curveSel+'_jointEnd', p = cvPosition[0])
    if(i>0)and(i != numCVs-1):
    	cmds.joint(n=curveSel+'_joint_'+(str(i)),p = cvPosition[0])	
    i+=1
 
# reorient the joints    
i=1
while 1:
	if not (i < numCVs-1):
		break
	#find the name of the current Joint
	currentJoint = (curveSel+'_joint_'+(str(i)))
	cmds.joint(currentJoint,e=True,zso=True,oj="xyz")
	i+=1

cmds.createNode('pointOnCurveInfo')