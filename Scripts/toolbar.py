import os
from functools import partial
import maya.mel as mel
import maya.cmds as cmds
widgets={}

def UI():
	if cmds.dockControl("toolbarDock", exists=True):
		cmds.deleteUI("toolbarDock")

	widgets["window"]=cmds.window(w=100,h=700, title="Forest Toolbar", mnb=False, mxb=False, sizeable=True, backgroundColor= [0.1,.2,.2])
	widgets["scrollLayout"]=cmds.scrollLayout(hst=0, w=100)
	widgets["mainLayout"]=cmds.columnLayout(adj=True, parent=widgets["scrollLayout"])

	populateUI()

	widgets["docks"]=cmds.dockControl("toolbarDock", label="Forest Toolbar", area='left', content=widgets["window"])

def populateUI():

	path=cmds.internalVar(upd=True)

	path=path.strip('/prefs')+"/scripts/icons/"

	icons=os.listdir(path)


	categories=[]

	for icon in icons:
		categoryName=icon.partition("__")[0]
		categories.append(categoryName)

	categoryNames=list(set(categories))


	for category in categoryNames:
		widgets[(category+'_frameLayout')]=cmds.frameLayout(label=category,collapsable=True, parent=widgets["mainLayout"], backgroundColor= [0.1,.4,.25])
		widgets[(category+"_mainLayout")]=cmds.rowColumnLayout(nc=2)

	for icon in icons:
		category=icon.partition("__")[0]

		command=icon.partition("__")[2].partition(".")[0]

		widgets[(icon+"button")]=cmds.symbolButton(w=50,h=50, image=(path+icon), parent=widgets[(category+"_mainLayout")])	


def runMethod(method, *args):
	exec(method+"()")


