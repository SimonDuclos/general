import maya.cmds as cmds
from functools import partial

def createUI(*args):
    
    windowprop = 'PropAutoRig'
    
    if cmds.window( windowprop, exists = True):
        cmds.deleteUI( windowprop )
    
    window = cmds.window('PropAutoRig', t='Prop Autorig',w=150, h=250, sizeable=False, backgroundColor= [0.1,.2,.2])
    cmds.columnLayout()
    cmds.text('Rig axis')
    cmds.separator( height=7, style='out' )
    cmds.radioCollection('Axis')
    cmds.rowColumnLayout("radioLayout", numberOfColumns=3, columnWidth=[(1,50), (2,50), (3,50)])
    cmds.radioButton('X', label='X' )
    cmds.radioButton('Y', label='Y' )
    cmds.radioButton('Z', label='Z' )
    cmds.setParent("..")
    cmds.text('')
    cmds.text(label = 'Asset to rig')
    cmds.button(label = 'Run', backgroundColor= [0.4,.1,.25], command = proprigger)
    cmds.showWindow('PropAutoRig')
    cmds.window(window, edit=True, w=150, h=150) 
    
def proprigger(*args):
    axis=cmds.radioCollection('Axis', q=True, sl=True)
    prop = cmds.ls(sl=True)[0]
    if ':' in prop:
        name=prop.split(':')[1]
    namesplit = name.split('_')
    namelength= len(namesplit)
    name = name.replace(namesplit[0]+'_','')
    name = name.replace('_'+namesplit[namelength-1],'')


    minX = cmds.getAttr(prop + '.boundingBoxMinX')
    maxX = cmds.getAttr(prop + '.boundingBoxMaxX')
    X= maxX-minX
    tX=maxX-X/2

    minZ = cmds.getAttr(prop + '.boundingBoxMinZ')
    maxZ = cmds.getAttr(prop + '.boundingBoxMaxZ')
    Z=maxZ-minZ
    tZ=maxZ-Z/2

    minY = cmds.getAttr(prop + '.boundingBoxMinY')
    maxY = cmds.getAttr(prop + '.boundingBoxMaxY')
    Y=maxY-minY
    tY=maxY-Y/2

    size=((cmds.getAttr(prop + '.boundingBoxMax'+axis))-(cmds.getAttr(prop + '.boundingBoxMin'+axis)))/2
    midY=minY+(maxY-minY)/2
    midX=minX+(maxX-minX)/2
    midZ=minZ+(maxZ-minZ)/2


    ctlmain, creation_node = cmds.circle(n='R_'+ name + '_Main_CTL', r=size*1.1, nr=(0, 1, 0))
    ctloffset, creation_node =cmds.circle(n='R_'+name + '_Offset_CTL', r=size, nr=(0, 1, 0))

    ctlpropbase, creation_node = cmds.circle(n='R_' + name + '_Base_CTL', r=size, nr=(0, 1, 0))
    ctlpropmid = cmds.circle(n='R_' + name + '_Mid_CTL', r=size*1.25, nr=(0, 1, 0))[0]
    ctlpropup = cmds.circle(n='R_' + name + '_Up_CTL', r=size, nr=(0, 1, 0))[0]

    if axis=='Y':
        cmds.xform(ctlpropbase, t=[tX,minY,tZ], ws=True)
        cmds.xform(ctlpropmid, t=[tX,midY,tZ], ws=True)
        cmds.xform(ctlpropup, t=[tX,maxY,tZ], ws=True)

    if axis=='X':
        cmds.xform(ctlpropbase, t=[minX,tY,tZ], ws=True)
        cmds.xform(ctlpropmid, t=[midX,tY,tZ], ws=True)
        cmds.xform(ctlpropup, t=[maxX,ty,tZ], ws=True)    

    if axis=='Z':
        cmds.xform(ctlpropbase, t=[tX,tY,minZ], ws=True)
        cmds.xform(ctlpropmid, t=[tX,tY,midZ], ws=True)
        cmds.xform(ctlpropup, t=[tX,tY,maxZ], ws=True)
            
    jnt = cmds.createNode('joint', n='R_' + name + '_JNT')
    cmds.hide(jnt)
    grpmain = cmds.group(ctlmain, n='R_' + name + '_MAIN' + '_GRP')
    grpoffset = cmds.group(ctloffset, n='R_'+ name +'_Offset' + '_GRP')
    grppropbase = cmds.group(ctlpropbase, n='R_'+ name +'_Base' + '_GRP')
    grppropmid = cmds.group(ctlpropmid, n='R_' + name +'_Mid' + '_GRP')
    grppropup = cmds.group(ctlpropup, n='R_'+ name+ '_Up' + '_GRP')

    cmds.parent(grpoffset, ctlmain)
    cmds.parent(grppropbase, ctloffset)
    cmds.parent(grppropmid, ctlpropbase)
    cmds.parent(grppropup, ctlpropmid)

    cmds.parentConstraint(ctlpropup, jnt, mo=True)

    ctls = cmds.ls('*_CTL')
    cmds.makeIdentity(ctls, apply=True, t=1, r=1, s=1, n=0)

    system = cmds.group(jnt, n='SYSTEM')
    cmds.skinCluster(jnt, prop, mi=1) 

createUI() 