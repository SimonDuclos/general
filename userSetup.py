from maya import cmds

import os, sys

def mayaToolsInstall_UI():
	if cmds.window("mayaToolsInstall_UI", exists=True):
		cmds.deleteUI("mayaToolsInstall_UI")
	window = cmds.window("mayaToolsInstall_UI", title= "ForestTools Install", w=300, h=110, titleBarMenu=False, sizeable=False)

	mainLayout = cmds.columnLayout(w=300, h=110)
	formLayout = cmds.formLayout(w=300, h=110)

	text = cmds.text('Unable to locate tools, please browse.', w=300)

	cancelButton = cmds.button(label = 'Cancel', w=140, h=50, c=mayaToolsInstall_Cancel)
	browseButton = cmds.button(label = 'Browse', w=140, h=50, c=mayaToolsInstall_Browse)

	cmds.formLayout(formLayout, edit =True, af=[(text, 'left', 10), (text, 'top', 10)])
	cmds.formLayout(formLayout, edit =True, af=[(cancelButton, 'left', 5), (cancelButton, 'top', 50)])
	cmds.formLayout(formLayout, edit =True, af=[(browseButton, 'right', 5), (browseButton, 'top', 50)])

	cmds.showWindow(window)
	cmds.window(window, edit=True, w=300, h=300)


def mayaToolsInstall_Cancel(*args):
	cmds.deleteUI("mayaToolsInstall_UI")

def mayaToolsInstall_Browse(*args):
    toolDir = cmds.fileDialog2(dialogStyle=2, fileMode =3) [0]
    if toolDir.rpartition("/")[2] != "ForestTools":
        cmds.warning("Selected directory is not the ForestTools directory")
    else :
        cmds.deleteUI("mayaToolsInstall_UI")
        path = cmds.internalVar(upd = True)+ "ForestTools.txt"
        f=open(path, 'w',)
        f.write(toolDir)
        f.close()
        
        path = toolDir+"/General/Scripts"
        sys.path.append(path)
        
        import mayaSetup
        mayaSetup.setupTools()

def forestTools():

	path = cmds.internalVar(upd=True)+ "ForestTools.txt"

	if os.path.exists(path):
		f=open(path, 'r')
		toolDir= f.readline()
		path = toolDir+"/General/Scripts"

		if os.path.exists(path):
			if not path in sys.path:
				sys.path.append(path)

		import mayaSetup
		mayaSetup.setupTools()
		
	else:
		mayaToolsInstall_UI()

    


scriptJobNum = cmds.scriptJob(event = ["NewSceneOpened", forestTools])


